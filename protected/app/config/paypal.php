<?php
return array(
	
	'use_sandbox' => true,
	
    // set your paypal url sandbox
    'form_action_sandbox' => 'https://www.sandbox.paypal.com/cgi-bin/webscr', 
    'paypalurl_sandbox' => 'www.sandbox.paypal.com', 
    'default_email_sandbox' => 'endro.mono-facilitator@yahoo.co.id', 
    
    // set your paypal url 
    'form_action' => 'https://www.paypal.com/cgi-bin/webscr',
    'paypalurl' => 'www.paypal.com',
    'default_email' => 'jssom-bs@hotmail.com',
    
    // set your url    
    'cancel_return' => 'http://localhost/reseller-panel/user/balance?msg=trxn_failed',
    'success' => 'http://localhost/reseller-panel/user/balance/success',
       
);