<?php
return array(
	
	'use_sandbox' => false,
	
    // set your paypal url sandbox
    'form_action_sandbox' => 'https://www.sandbox.paypal.com/cgi-bin/webscr', 
    'paypalurl_sandbox' => 'www.sandbox.paypal.com', 
    'default_email_sandbox' => 'endro.mono-facilitator@yahoo.co.id', 
    
    // set your paypal url 
    'form_action' => 'https://www.paypal.com/cgi-bin/webscr',
    'paypalurl' => 'www.paypal.com',
    'default_email' => 'jssom-bs@hotmail.com',
       
);