<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug'           => true,

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url'             => 'http://localhost/smmproviders',

    'is_ssl' => false,

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone'        => 'UTC',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale'          => 'en',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key'             => '5wJuGTZRy9kDkr70BpXNa6pitbXKeyaV',

    'cipher'          => MCRYPT_RIJNDAEL_128,

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers'       => array(

        'Illuminate\Foundation\Providers\ArtisanServiceProvider',
        'Illuminate\Auth\AuthServiceProvider',
        'Illuminate\Cache\CacheServiceProvider',
        'Illuminate\Session\CommandsServiceProvider',
        'Illuminate\Foundation\Providers\ConsoleSupportServiceProvider',
        'Illuminate\Routing\ControllerServiceProvider',
        'Illuminate\Cookie\CookieServiceProvider',
        'Illuminate\Database\DatabaseServiceProvider',
        'Illuminate\Encryption\EncryptionServiceProvider',
        'Illuminate\Filesystem\FilesystemServiceProvider',
        'Illuminate\Hashing\HashServiceProvider',
        'Illuminate\Html\HtmlServiceProvider',
        'Illuminate\Log\LogServiceProvider',
        'Illuminate\Mail\MailServiceProvider',
        'Illuminate\Database\MigrationServiceProvider',
        'Illuminate\Pagination\PaginationServiceProvider',
        'Illuminate\Queue\QueueServiceProvider',
        'Illuminate\Redis\RedisServiceProvider',
        'Illuminate\Remote\RemoteServiceProvider',
        'Illuminate\Auth\Reminders\ReminderServiceProvider',
        'Illuminate\Database\SeedServiceProvider',
        'Illuminate\Session\SessionServiceProvider',
        'Illuminate\Translation\TranslationServiceProvider',
        'Illuminate\Validation\ValidationServiceProvider',
        'Illuminate\View\ViewServiceProvider',
        'Illuminate\Workbench\WorkbenchServiceProvider',
        'Way\Generators\GeneratorsServiceProvider',
        'Cartalyst\Sentry\SentryServiceProvider',
        'Authority\Repo\RepoServiceProvider',
        'Authority\Service\Form\FormServiceProvider',
        'Jenssegers\Date\DateServiceProvider',
        'Ignited\LaravelOmnipay\LaravelOmnipayServiceProvider',

    ),

    /*
    |--------------------------------------------------------------------------
    | Service Provider Manifest
    |--------------------------------------------------------------------------
    |
    | The service provider manifest is used by Laravel to lazy load service
    | providers which are not needed for each request, as well to keep a
    | list of all of the services. Here, you may set its storage spot.
    |
    */

    'manifest'        => storage_path() . '/meta',

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases'         => array(

        'App'               => 'Illuminate\Support\Facades\App',
        'Artisan'           => 'Illuminate\Support\Facades\Artisan',
        'Auth'              => 'Illuminate\Support\Facades\Auth',
        'Blade'             => 'Illuminate\Support\Facades\Blade',
        'Cache'             => 'Illuminate\Support\Facades\Cache',
        'ClassLoader'       => 'Illuminate\Support\ClassLoader',
        'Config'            => 'Illuminate\Support\Facades\Config',
        'Controller'        => 'Illuminate\Routing\Controller',
        'Cookie'            => 'Illuminate\Support\Facades\Cookie',
        'Crypt'             => 'Illuminate\Support\Facades\Crypt',
        'DB'                => 'Illuminate\Support\Facades\DB',
        'Eloquent'          => 'Illuminate\Database\Eloquent\Model',
        'Event'             => 'Illuminate\Support\Facades\Event',
        'File'              => 'Illuminate\Support\Facades\File',
        'Form'              => 'Illuminate\Support\Facades\Form',
        'Hash'              => 'Illuminate\Support\Facades\Hash',
        'HTML'              => 'Illuminate\Support\Facades\HTML',
        'Input'             => 'Illuminate\Support\Facades\Input',
        'Lang'              => 'Illuminate\Support\Facades\Lang',
        'Log'               => 'Illuminate\Support\Facades\Log',
        'Mail'              => 'Illuminate\Support\Facades\Mail',
        'Paginator'         => 'Illuminate\Support\Facades\Paginator',
        'Password'          => 'Illuminate\Support\Facades\Password',
        'Queue'             => 'Illuminate\Support\Facades\Queue',
        'Redirect'          => 'Illuminate\Support\Facades\Redirect',
        'Redis'             => 'Illuminate\Support\Facades\Redis',
        'Request'           => 'Illuminate\Support\Facades\Request',
        'Response'          => 'Illuminate\Support\Facades\Response',
        'Route'             => 'Illuminate\Support\Facades\Route',
        'Schema'            => 'Illuminate\Support\Facades\Schema',
        'Seeder'            => 'Illuminate\Database\Seeder',
        'Session'           => 'Illuminate\Support\Facades\Session',
        'SoftDeletingTrait' => 'Illuminate\Database\Eloquent\SoftDeletingTrait',
        'SSH'               => 'Illuminate\Support\Facades\SSH',
        'Str'               => 'Illuminate\Support\Str',
        'URL'               => 'Illuminate\Support\Facades\URL',
        'Validator'         => 'Illuminate\Support\Facades\Validator',
        'View'              => 'Illuminate\Support\Facades\View',
        'Sentry'            => 'Cartalyst\Sentry\Facades\Laravel\Sentry',
        'Date'              => 'Jenssegers\Date\Date',
        'Omnipay'           => 'Ignited\LaravelOmnipay\Facades\OmnipayFacade',

    ),

    'ipanel_add'      => array(
        'Middle East Instagram Likes'      => 'https://api-ig69.social-engine.pro/api/add.php',
        'Middle East Instagram Comments'   => 'https://api-ig69.social-engine.pro/comments/add.php',
        'Middle East Instagram Followers'  => 'https://api-ig69.social-engine.pro/followers/index.php',
        'Middle East Instagram Auto Likes' => 'https://api-ig69.social-engine.pro/api/index.php',
        'Instagram High Quality Likes'     => 'http://ipanel4.auto-gram.com/api/add.php',
        'Instagram High Quality Comments'  => 'http://ipanel4.auto-gram.com/comments/add.php',
        'Instagram High Quality Followers' => 'http://ipanel4.auto-gram.com/followers/index.php',
        'Instagram Mentions'               => 'http://ipanel4.auto-gram.com/mentions/add.php',
        'Instagram Mentions 69'            => 'https://api-ig69.social-engine.pro/mentions/add.php',
        'Instagram Mentions Custom List'   => 'http://ipanel4.auto-gram.com/mentions/add.php',
        'Instagram Views'                  => 'https://api-ig69.social-engine.pro/video_views/add.php',
        'Instagram User Mentions'          => 'https://api-ig69.social-engine.pro/user_mentions/add.php',
        'Auto Likes'               		   => 'https://api-ig69.social-engine.pro/api/index.php',
		'Auto Views'               		   => 'https://api-ig69.social-engine.pro/video_views/index.php',
    ),
    'ipanel_check'    => array(
        'Middle East Instagram Likes'      => 'https://api-ig69.social-engine.pro/api/check.php',
        'Middle East Instagram Comments'   => 'https://api-ig69.social-engine.pro/comments/check.php',
        'Middle East Instagram Followers'  => 'https://api-ig69.social-engine.pro/followers/check.php',
        'Middle East Instagram Auto Likes' => 'https://api-ig69.social-engine.pro/api/like.php',
        'Instagram Mentions'               => 'http://ipanel4.auto-gram.com/mentions/check.php',
        'Instagram Mentions Custom List'   => 'http://ipanel4.auto-gram.com/mentions/check.php',
        'Instagram Mentions 69'            => 'https://api-ig69.social-engine.pro/mentions/check.php',
        'Instagram High Quality Likes'     => 'http://ipanel4.auto-gram.com/api/check.php',
        'Instagram High Quality Comments'  => 'http://ipanel4.auto-gram.com/comments/check.php',
        'Instagram High Quality Followers' => 'http://ipanel4.auto-gram.com/followers/check.php',
        'Instagram Views'                  => 'https://api-ig69.social-engine.pro/video_views/check.php',
        'Instagram User Mentions'          => 'https://api-ig69.social-engine.pro/user_mentions/check.php',
        'Auto Likes'     				   => 'https://api-ig69.social-engine.pro/test-api/check.php',
    ),
    'ipanel_update'   => array(
        'Middle East Instagram Likes'      => '',
        'Middle East Instagram Comments'   => '',
        'Middle East Instagram Followers'  => 'https://api-ig69.social-engine.pro/followers/update.php',
        'Instagram High Quality Followers' => 'http://ipanel4.auto-gram.com/followers/update.php',
        'Auto Likes'					   => 'https://api-ig69.social-engine.pro/api/index.php',
        'Auto Views'					   => 'https://api-ig69.social-engine.pro/video_views/index.php',
    ),

    'api_access'      => array(
        'abcdefg1_string' => 1,
        'abcdefg2_string' => 2,
        'abcdefg3_string' => 3,
        'abcdefg4_string' => 4,
        'abcdefg5_string' => 5,
    ),

    'api_url'         => 'http://localhost/smmproviders/api',

    'private_key'     => 'j#FD3ZX3OLQFGZJjFZR#'
);
