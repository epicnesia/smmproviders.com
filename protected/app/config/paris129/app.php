<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug'      => false,

    'url'        => 'http://smmproviders.com',

    'timezone'   => 'UTC',

    'api_access' => array(
        'We0zkceqUXWhV4FbIBVMmtf5vBI5I5oH' => 22,
    ),


    'api_url'          => 'http://smmproviders.com/api',

);
