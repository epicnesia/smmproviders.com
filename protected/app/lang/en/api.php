<?php

return array(

        /*
        |--------------------------------------------------------------------------
        | API Language Lines
        |--------------------------------------------------------------------------
        |
        | The following language lines are used by the paginator library to build
        | the simple pagination links. You are free to change them to anything
        | you want to customize your views to better match your application.
        |
        */
      
      'yourApiKey' => "Your API Key",
      
      'key' => "API Key",
      
      'generate_key' => "Generate Key",
      
      'copy_key' => "Copy Key",
      
      'systemError' => "System Error, please try again later!",
      
      'generateKeySuccess' => "API Key generated successfully!",
      
      'notAllowedUser' => "You are not allowed to do this action!"
      
      );
