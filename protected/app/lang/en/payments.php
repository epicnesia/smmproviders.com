<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| payments Repository Messages
	|--------------------------------------------------------------------------
	*/

	"transaction_id" 	=> "Transaction id",
	
	"balance" 	=> "Balance",
	
	"amount" 	=> "Amount",
	"details" 	=> "Details",
	"date" 	=> "Date",
	"add" 	=> "Add",
	
	"click_complete" 	=> "Please click for Completing Payment",
	
        "addBalance" => "Add Balance",
        "editBalance" => "Edit Balance",
        "balanceAdderHistory" => "Balance Adder History",
        "id"=> "ID",
        "username"=> "Username",
        "balanceAmount"=> "Amount",
        "created"=> "Created",
        "updated"=> "Updated",
        "action"=> "Action",
        "user_list"=> "User List",
        "history"=> "History"
	

);