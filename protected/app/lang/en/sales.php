<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| sales Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used by the paginator library to build
	| the simple pagination links. You are free to change them to anything
	| you want to customize your views to better match your application.
	|
	*/
	
	'nodatadeleted' => 'No sales removed',
	
	'datadeleted' => 'Sales have been removed',
	
	'therisnotdel' => 'There are sales that are not removed',
	
	'newsalesaved' => 'A new sale has been saved',
	
	'onedatadeleted' => 'An sale has been removed',
	
	'nodata' => 'No sales',
	
	'nodatachanged' => 'No sales were changed',
	
	'newsalenotsaved' => 'A new sale is not saved',
	
	'onedatachanged' => 'An sale has been successfully changed',
	
	'onedatafailchanged' => 'An sale fails modified',
	
	'accept' => 'Accept',

);
