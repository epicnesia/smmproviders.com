<?php

return array(
	
	/*
	|--------------------------------------------------------------------------
	| Language strings for views
	|--------------------------------------------------------------------------
	*/
	
	'flashsuccess'    => 'Success',
	
	'flasherror'      => 'Error',
	
	'flashwarning'    => 'Warning',
	
	'flashinfo'       => 'FYI',
	
	'register'        => 'Register',
	
	'login'           => "Login",
	
	'logout'          => "Logout",
	
	'home'            => "Home",
	
	'users'           => "Users",
	
	'user'            => "User",
	
	'groups'          => "Groups",
	
	'helloworld'      => "Hello World!",
	
	'description'     => "This is an example of <a href=\"https://github.com/laravel/laravel\">Laravel 4.2</a> running with <a href=\"https://github.com/cartalyst/sentry\">Sentry 2.1</a> and <a href=\"http://getbootstrap.com/\">Bootstrap 3.2</a>.",
	
	'loginstatus'     => "You are currently logged in.",
	
	'sessiondata'     => "Session Data",
	
	'currentusers'    => "Current Users",
	
	'options'         => 'Options',
	
	'status'          => "Status",
	
	'active'          => "Active",
	
	'notactive'       => "Not Active",
	
	'suspended'       => "Suspended",
	
	'banned'          => "Banned",
	
	'actionedit'      => "Edit",
	
	'actionupdate'    => "Update",
	
	'actionsuspend'   => "Suspend",
	
	'actionunsuspend' => "Un-Suspend",
	
	'actionban'       => "Ban",
	
	'actionunban'     => "Un-Ban",
	
	'actiondelete'    => "Delete",
	
	'actionEnable'    => "Enable",
	
	'actionDisable'    => "Disable",
	
	'name'            => "Name",
	
	//'listwith'     => "",
	
	'usermanagement'                => "smmproviders.com",
	
	'reallylogout'                => "Are you sure to logout?",
	
	'profile'                => "Profile",
	
	'created'                => "Created",
	
	'modified'                => "Modified",
	
	'listusers'                => "Users List",
	
	'viewusers'                => "View User",
	
	'createuser'                => "Create User",
	
	'listof'                => "List of",
	
	'editgroup'                => "Edit Group",
	
	'creategroup'                => "Create Group",
	
	'wellcometo'                => "Welcome to",
	
	'manager'                => "Manager",
	
	'accounting'                => "Accounting",
	
	'data'                => "Data",
	
	'instagram'                => "Instagram",
	
	'tools'                => "Tools",
	
	'whatsapp'                => "Whatsapp",
	
	'showgroup'                => "Show Group",
	
	'edit'                => "Edit",
	
	'super'                => "Super",
	
	'manageusers'                => "Manage Users",
	
	'managepermisions'                => "Manage Permisions",
	
	'actionsave'      => "Save",
	
	'check'      => "Check",
	
	'permissions'      => "Permissions",
	
	'createpermission'      => "Create Permission",
	
	'wellcometo'      => "Welcome to",
	
	'orders'      => "Orders",
	
	'supports'      => "Supports",
	
	'createorder'                => "Create Order",
	
	'create'                => "Create",
	
	'services' => 'Services',
	
	'customprice' => 'Custom Price',
	
	'order'      => "Order",
	
	'sureto'      => "Are you sure you want to",
	
	'nodatadeleted' => 'No data is deleted',
	
	'datadeleted' => 'The data has been deleted',
	
	'therisnotdel' => 'There is data is not deleted',
	
	'accountanttool'                => "Accountant Tool",
	
	'workertool'                => "Worker Tool",

	'setting'      => "Setting",

	'order'      => "Order",

	'payments'      => "Payments",

	'stat'      => "Statistics",
        
        'signup' => "Sign Up",
        
		'history' => 'History',
	
	'balance' => 'Balance',
	
	'faq' => 'FAQ',
	
	'ticket' => 'Ticket',
	
	'page' => 'Page',

    'balanceAdder' => 'Balance Adder',

    'apiKeyGen' => 'Api Key Generator',
	
    'apikey' => 'API Help',
	
    'changePassword' => 'Change Password',
	
    'Auto Likes' => 'Auto Likes',
	
    'Auto Views' => 'Auto Views',
	
    'Service List' => 'Service List',
	
    'Search' => 'Search',
);