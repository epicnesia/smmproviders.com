<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Orders Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used by the paginator library to build
	| the simple pagination links. You are free to change them to anything
	| you want to customize your views to better match your application.
	|
	*/

	'pleaseselect' => 'Please Select',

	'check'     => 'Check',
	
	'service'     => 'Service',
	
	'type'     => 'Type',
	
	'subtype'     => 'Subtype',
	
	'checked'     => 'Checked',
		
	'edit'     => 'Edit',
	
	'clientname'     => 'Client Name',
	
	'phonenumber'     => 'Phone Number',
	
	'sellername'     => 'Seller Name',
	
	'price'     => 'Price',
	
	'nodatadeleted' => 'No orders removed',
	
	'datadeleted' => 'Orders have been removed',
	
	'therisnotdel' => 'There are orders that are not removed',
	
	'newordersaved' => 'A new order has been saved',
	
	'onedatadeleted' => 'An order has been removed',
	
	'nodata' => 'No orders',
	
	'nodatachanged' => 'No orders were changed',
	
	'newordernotsaved' => 'A new order is not saved',
	
	'onedatachanged' => 'An order has been successfully changed',
	
	'onedatafailchanged' => 'An order fails modified',
	
	'status' => 'Status',
	
	'accept' => 'Accept',
	
	'date' => 'Date',

	'noresultapi' => 'Order Failed to Processed, Please try again later or contact our support',
	
	'neworder' => 'New Order',
	
	'link' => 'Link',
	
	'quantity' => 'Quantity',
	
	'Re' => 'Re',

	'charge' => 'Charge',

	'startcount' => 'Start Count',

	'remains' => 'Remains',
        
    'title' => 'Title',
        
	'content' => 'Content',
		
	'value' => 'Value',
		
	'mUsernames' => 'Usernames',

	'mHashtags' => 'Hashtags',

	'custom_list' => 'Custom List',
        
	'custom_listFile' => 'Custom List File',
        
	'created' => 'Created',
        
	'expired' => 'Expired',
        
	'7Days' => '7 Days',
        
	'30Days' => '30 Days',
        
	'60Days' => '60 Days',
        
	'90Days' => '90 Days',
        
	'service' => 'Service',
        
	'time' => 'Time',
        
	'Total Orders' => 'Total Orders',
        
	'Orders Completed' => 'Orders Completed',
        
	'Orders Processing' => 'Orders Processing',
        
	'Orders Pending' => 'Orders Pending',
        
	'Extend Days' => 'Extend Days',
        
	'plan' => 'Plan',
        
	'username' => 'Username',

);
