<?php

return array(
        
        /*
        |--------------------------------------------------------------------------
        | Language strings for views
        |--------------------------------------------------------------------------
        */
      
        'addService' => 'Add Service',
        
        'addServiceTypes' => 'Add Service Types',
        
        'addServiceSubtypes' => 'Add Service Subtypes',
        
        'createService' => 'Create Service',
        
        'createServiceTypes' => 'Create Service Types',
        
        'createServiceSubtypes' => 'Create Service Subtypes',
        
        'editService' => 'Edit Service',
        
        'editServiceTypes' => 'Edit Service Types',
        
        'editServiceSubtypes' => 'Edit Service Subtypes',
        
        'services' => 'Services',
        
        'servicesList' => 'List Of Services',
        
        'serviceName' => 'Service Name',
        
        'serviceTypes' => 'Service Types',
        
        'serviceSubtypes' => 'Service Subtypes',
        
        'serviceTypesName' => 'Service Types Name',
        
        'serviceSubtypesName' => 'Service Subtypes Name',
        
        'serviceTypesList' => 'Service Types',
        
        'serviceSubtypesList' => 'Service Subtypes',
        
        'serviceSubtypesPrice' => 'Price',
        
        'serviceSubtypesPrice/1000' => 'Price / 1000',
        
        'serviceSubtypesCount' => 'Count',
        
        'serviceStatus' => 'Service Status',
        
        'backToParent' => 'Back To Parent',
        
        'customPriceList' => 'Custom Price List',
        
        'addCustomPrice'=> 'Add Custom Price',
        
        'editCustomPrice' => 'Edit Custom Price',
        
        'customPriceUsers'=> 'User',
        
        'normalPrice'=> 'Normal Price',
        
        'customPrice'=> 'Custom Price',
        
        'userName'=> 'Username',
        
        'createCustomPrice'=> 'Create Custom Price',
        
        'editCustomPriceTitle' => 'Edit Custom Price For ',
        
        'serviceSubtypesAutoOrder' => 'Auto Order',
        
        'serviceSubtypesQuestions' => 'Questions',
        
        'serviceSubtypesAnswers' => 'Answer to this question',
        
        'serviceMaxAmount' => 'Max Amount',
        
        'price_7_days' => '7 Days Price',
        
        'price_30_days' => '30 Days Price',
        
        'price_60_days' => '60 Days Price',
        
        'price_90_days' => '90 Days Price',
        
        'cost_7_days' => '7 Days Cost',
        
        'cost_30_days' => '30 Days Cost',
        
        'cost_60_days' => '60 Days Cost',
        
        'cost_90_days' => '90 Days Cost',
        
        '7_days' => '7 Days',
        
        '30_days' => '30 Days',
        
        '60_days' => '60 Days',
        
        '90_days' => '90 Days',
        
        'Regular Service' => 'Regular Service',
        
        'Subscription Service' => 'Subscription Service',
        
        'Service Cost' => 'Service Cost',
        
        'Name' => 'Name',
        
        'Count' => 'Count',
        
        'Price' => 'Price',
        
        'Cost' => 'Cost',
        
        'Option' => 'Option',
        
        'Plan' => 'Plan',
        
        'Add Plan' => 'Add Plan',
        
        'Edit Plan' => 'Edit Plan',
        
        'Auto Likes Plans' => 'Auto Likes Plans',
        
        'Auto Views Plans' => 'Auto Views Plans',

);  