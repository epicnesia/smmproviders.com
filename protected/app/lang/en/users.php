<?php

return array(

	/*
	 |--------------------------------------------------------------------------
	 | User Repositiory Messages
	 |--------------------------------------------------------------------------
	 */

	'created' => "Your account has been created. Please Login with your username and password",

	'loginreq' => "Login field required.",

	'exists' => "User already exists.",

	'notfound' => "User not found",

	'noaccess' => "You are not allowed to do that.",

	'updated' => "Profile updated",

	'notupdated' => "Unable to update profile",

	'activated' => "Activation complete. <a href=':url' class='alert-link'>You may now login</a>",

	'notactivated' => "Activation could not be completed.",

	'alreadyactive' => "That account has already been activated.",

	'emailconfirm' => "Check your email for the confirmation link.",

	'emailinfo' => "Check your email for instructions.",

	'emailpassword' => "Your password has been changed. Check your email for the new password.",

	'problem' => "There was a problem. Please contact the system administrator.",

	'passwordchg' => "Your password has been changed.",

	'passwordprob' => "Your password could not be changed.",

	'oldpassword' => "You did not provide the correct original password.",

	'suspended' => "User has been suspended for :minutes minutes.",

	'unsuspended' => "Suspension removed.",

	'banned' => "User has been banned.",

	'unbanned' => "User has been unbanned.",

	'remember' => "Remember",

	'forgot' => "Forgot your password",

	'forgotupword' => "Please type your e-mail address",

	'resendpword' => "Reset Password",

	'email' => "E-mail",

	'pword' => "Password",

	'fname' => "First Name",

	'lname' => "Last Name",

	'group_membership' => "Groups",

	'change_password' => "Change Password",

	'oldpassword_lbl' => "Old Password",

	'newpassword_lbl' => "New Password",

	'newcompassword_lbl' => "New Confirm Password",

	'create' => "Create User",

	'active' => "Activated",

	'users' => "Users",

	'ploginf' => "Please Login First",

	'manager' => "Manager",

	'workers' => "Workers",

	'marketer' => "Marketer",

	'super' => "Super",

	'accountan' => "Accountan",

	'user' => "User",

	'compassword' => "Confirm Password",

	'password' => "Password",

	'check' => "Check",

	'checked' => "Checked",

	'username' => "Username",

	'country' => "Country",

	'id' => "Id",

	'balance' => "Balance",

	'spent' => "Spent",

	'registrationdate' => "Registration Date",

	'lastlogin' => "Last login",

        'rates' => "Rates",

        'service_name' => "Service Name",

        'normal_price' => "Normal Price",

        'custom_price' => "Custom Price",
        
	'adduser' => "Add User",
	
	'social_api' => 'Social Bulk Panel API Key',
	
	'change_api_key' => "Change API key",
	
	'youtube_api' => "YouTube sevice API Key",
	
	'apichg' => 'API has been changed',
	
	'apinochg' => 'APIs can not be changed',
	
	'change_paypal_detail' => "Change Paypal Detail",
	
	'paypall' => "Paypal",
	
	'signature' => "Signature",
	
	'paypalchg' => 'Paypal Email changed',
	
	'paypalnochg' => 'Paypal Email unchanged',
	
	'7Days_price' => '7 Days Price',
	
	'30Days_price' => '30 Days Price',
	
	'60Days_price' => '60 Days Price',
	
	'90Days_price' => '90 Days Price',
	
	'skype_username' => 'Skype Username',
	
	'Account Overview' => 'Account Overview',
	
	
	
);
