<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Home
//Route::get('/', 'HomeController@showWelcome');
//Route::get('/main', 'HomeController@showWelcome');
//Route::post('/main', 'HomeController@showWelcome');
//
//// Session Routes
//Route::get('login', array('as' => 'login', 'uses' => 'SessionController@create'));
//Route::post('logout', array('as' => 'logout', 'uses' => 'SessionController@destroy'));
//Route::get('logout', array('as' => 'logout', 'uses' => 'SessionController@destroy'));
//Route::resource('sessions', 'SessionController', array('only' => array('create', 'store', 'destroy')));
//
//Route::post('register', 'UserController@register');
//
//// Order
//Route::resource('/order', 'OrderController');
//Route::get('/order/{id}/{status}', 'OrderController@changeStatus');
//Route::post('/order/setStartCount', 'OrderController@setStartCount');
//Route::post('/order/checkServiceType', 'OrderController@checkServiceType');
//Route::post('/order/getCharge', 'OrderController@getCharge');
//Route::post('/order/getPrice', 'OrderController@getPrice');
//
////api
//Route::group(array('prefix' => 'api'), function()
//{
//    Route::get('/', 'ApiController@index');
//    Route::post('/neworder', 'ApiOrderController@store');
//    Route::get('/order/{id}', 'ApiOrderController@show')-> where('id', '[0-9]+');
//
//    Route::get('/key', 'ApiController@showapi');
//    Route::get('/example', 'ApiController@showexample');
//    Route::get('/orderstatus', 'ApiOrderController@status');
//
//    Route::Resource('/keyGenerator', 'ApiKeyGeneratorsController');
//    Route::post('/generateKey', 'ApiKeyGeneratorsController@generateKey');
//    Route::get('/generateKeyFor/{id}', 'ApiKeyGeneratorsController@generateKeyFor')-> where('id', '[0-9]+');
//    Route::get('/changeApiAccess/{id}/{status}', 'ApiKeyGeneratorsController@changeApiAccess')-> where('id', '[0-9]+')-> where('status', '[0-9]+');
//});
//
//// ===============================================
//// ADMIN  =================================
//// ===============================================
//Route::group(array('prefix' => 'admin'), function()
//{
//	// main page
//	Route::get('/', 'StatController@index');
//
//	// users
//	Route::resource('/users', 'UserController');
//
//	Route::get('/setting', 'UserController@setting');
//
//	Route::resource('/payments', 'PaymentController');
//	Route::post('/payments', 'PaymentController@index');
//	Route::resource('/stat', 'StatController');
//
//	Route::post('/users/{id}/ubah', 'UserController@ubah')-> where('id', '[0-9]+');
//	Route::post('/users/{id}/perbaharui', 'UserController@perbaharui');
//	Route::post('/users/{id}/ubahApi', 'UserController@ubahApi')-> where('id', '[0-9]+');
//        Route::post('/users/{id}/ubahPaypalDetail', 'UserController@ubahPaypalDetail')-> where('id', '[0-9]+');
//        Route::get('/users/changeStatusUser/{id}/{status}', 'UserController@changeStatusUser')-> where('id', '[0-9]+')-> where('status', '[0-9]+');
//	Route::get('/order/{id}/cancelOrder', 'OrderController@cancelOrder')-> where('id', '[0-9]+');
//	Route::post('/order/{id}/cancelOrder', 'OrderController@cancelOrder')-> where('id', '[0-9]+');
//
//
//        Route::get('/balanceAdder', 'PaymentController@balanceAdder');
//        Route::get('/balanceAdder/history', 'PaymentController@balanceHistory');
//        Route::get('/balanceAdder/{id}/addBalance', 'PaymentController@addBalance')-> where('id', '[0-9]+');
//        Route::post('/balanceAdder/storeBalance', 'PaymentController@storeBalance');
//        Route::get('/balanceAdder/{id}/editBalance', 'PaymentController@editBalance')-> where('id', '[0-9]+');
//        Route::delete('/balanceAdder/{id}/cancelBalance', 'PaymentController@cancelBalance')-> where('id', '[0-9]+');
//        Route::post('/balanceAdder/updateBalance', 'PaymentController@updateBalance');
//
//});
//
//// ./admin
//
//// ===============================================
//// users  =================================
//// ===============================================
//Route::group(array('prefix' => 'user'), function()
//{
//	// main page
//	Route::get('/', 'StatController@userStat');
//
//	// users
//	Route::get('/changePassword', 'UserController@changePassword');
//	Route::get('/balance', 'UserController@balance');
//	Route::get('/order/history', 'OrderController@history');
//	Route::get('/order/show/{id}', 'OrderController@historyShow');
////	Route::post('/balance/pay', 'PaymentController@storePay');
//	Route::post('/balance/pay', 'PaymentController@buyBalance');
//	Route::get('/balance/success', 'PaymentController@successPay');
//	Route::get('/balance/added', 'PaymentController@balanceAdded');
//	Route::post('/balance/success', 'PaymentController@successPay');
//	Route::any('/balance/success', 'PaymentController@successPay');
//});
//// ./user
//
//// Ticket
//Route::resource('/ticket', 'TicketController');
//Route::post('/ticket/{id}/reply', 'TicketController@reply')-> where('id', '[0-9]+');
//
//// faq
//Route::resource('/page', 'PageController');
//
//// Rates Routes
//Route::resource('admin/rates', 'RateController');
//
//
//// Services Routes
//Route::resource('/admin/services', 'ServicesController');
//Route::post('/admin/services/deleteList/', 'ServicesController@deleteList');
//Route::post('/admin/services/{id}/destroy/', 'ServicesController@destroy')->where('id', '[0-9]+');
//Route::get('/admin/services/changeStatus/{id}', 'ServicesController@changeStatus')-> where('id', '[0-9]+');
//Route::get('/user/service_list', 'ServicesController@serviceList');
//
//// Auto Likes & Auto Views
//Route::get('/auto_likes', 'SubscriptionController@auto_likes');
//Route::get('/auto_likes/active', 'SubscriptionController@activeAutoLikes');
//Route::get('/auto_likes/expired', 'SubscriptionController@expiredAutoLikes');
//Route::get('/auto_views', 'SubscriptionController@auto_views');
//Route::get('/auto_views/active', 'SubscriptionController@activeAutoViews');
//Route::get('/auto_views/expired', 'SubscriptionController@expiredAutoViews');
//Route::post('/subscription/update_setting', 'SubscriptionController@updateSetting');
//Route::get('/subscription/{id}/add_plan', 'SubscriptionController@addPlan')-> where('id', '[0-9]+');
//Route::get('/subscription/{id}/edit_plan', 'SubscriptionController@editPlan')-> where('id', '[0-9]+');
//Route::post('/subscription/{id}/update_plan', 'SubscriptionController@updatePlan')-> where('id', '[0-9]+');
//Route::post('/subscription/store_plan', 'SubscriptionController@storePlan');
//Route::post('/subscription/deleteList/', 'SubscriptionController@deleteList');
//Route::post('/subscription/get_plan_price/', 'SubscriptionController@getPlanPrices');
//Route::get('/subscription/changeStatus/{id}', 'SubscriptionController@changeStatus')-> where('id', '[0-9]+');
//Route::resource('/subscription', 'SubscriptionController');



Route::get('/', function(){
    return View::make('maintenance');
//    return 'https://s30.postimg.org/5ahac8981/vinyl-decal-sticker-1338.jpg';
});
