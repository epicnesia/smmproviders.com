<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class OrdersCheck extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'orders:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Order Checker';

    /**
     * OrdersCheck constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

//    public static $_SERVICES_TO_CHECK
//        = [
//            'Middle East Instagram Likes',
//            'Middle East Instagram Followers',
//            'Middle East Instagram Comments',
//            'Instagram Views',
//        ];

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {

        try {

            $value = \Illuminate\Support\Facades\Cache::get('orders_check');
            if ($value) {
                var_dump($value);
                throw new Exception('Already Running');
            }
            \Illuminate\Support\Facades\Cache::forever('orders_check', date('l jS \of F Y h:i:s A'));


            $ordersQuery  = Order::whereNotIn(
                'status',
                array(
                    2,
                    3,
                )
            )->orderBy('status', 'asc')->orderBy('id', 'asc');
            $ordersSelect = $ordersQuery;

            $curl = new \Reseller\Api\Curl();
            $curl->option(CURLOPT_TIMEOUT, 10000);

            $ordersSelect->chunk(
                100,
                function ($orders) use ($curl) {
                    foreach ($orders as $order) {

                        try {
                            $order = Order::find($order->id);

                            if ($order->status == 2 || $order->status == 3) {
                                throw new Exception('Order Done / Refunded');
                            }

                            $serviceTypeId = $order->service_type_id;
                            $service       = Services::find($serviceTypeId);

                            if (!$service) {
                                throw new PanelServiceNotFoundException($order, $serviceTypeId, 'PanelServiceNotFoundException');
                            }
                            if ($service->status != 1) {
                                throw new PanelServiceDisabledException($order, $service, 'PanelServiceDisabledException');
                            }

                            $urls = \Illuminate\Support\Facades\Config::get('app.ipanel_check');

                            if (!in_array($service->name, array_keys($urls))) {
                                throw new Exception($service->name.' not exist on '.json_encode(array_keys($urls)));
                            }

                            $this->info('Checking order id #'.$order->id);

                            $ids = unserialize($order->api_id);
                            if (!$ids) {
                                throw new Exception('$ids = false');
                            }

                            if ($service->name === 'Middle East Instagram Comments') {
                                try {
                                    $this->checkIpanel69CommentOrders($ids, $urls, $order, $service, $curl);
                                } catch (Exception $e) {
                                    throw new Exception('Exception @checkIpanel69CommentOrders '.$e->getMessage(), 0, $e);
                                }

                            } elseif ($service->name === 'Middle East Instagram Likes') {
                                try {
                                    $this->checkIpanel69SingleLikesOrders($ids, $urls, $order, $service, $curl);
                                } catch (Exception $e) {
                                    throw new Exception('Exception @checkIpanel69SingleLikesOrders '.$e->getMessage(), 0, $e);
                                }
                            } elseif ($service->name === 'Middle East Instagram Followers') {
                                try {
                                    $this->checkIpanel69FollowersOrders($ids, $urls, $order, $service, $curl);
                                } catch (Exception $e) {
                                    throw new Exception('Exception @checkIpanel69FollowersOrders '.$e->getMessage(), 0, $e);
                                }
                            } elseif ($service->name === 'Instagram Views') {
                                try {
                                    $this->checkIpanel69SingleViewsOrders($ids, $urls, $order, $service, $curl);
                                } catch (Exception $e) {
                                    throw new Exception('Exception @checkIpanel69SingleViewsOrders '.$e->getMessage(), 0, $e);
                                }
                            }


                        } catch (PanelServiceDisabledException $e) {
                            $this->error($e->getMessage());
                            $this->error($e->getTraceAsString());
                            $this->serviceDisabled($e->order, $e->service);
                        } catch (PanelServiceNotFoundException $e) {
                            $this->error($e->getMessage());
                            $this->error($e->getTraceAsString());
                            $this->serviceNotFound($e->order, $e->serviceTypeId);
                        } catch (Exception $e) {
                            $this->error($e->getMessage());
                            $this->error($e->getTraceAsString());
                        }


                    }

                }
            );


        } catch (Exception $e) {

            $this->error($e->getMessage());
            $this->error($e->getTraceAsString());
        }

        return true;

    }

    private function checkOrderType($service)
    {
        $type = '';

        if (stripos($service->name, 'Middle East Instagram Likes') !== false
            || stripos($service->name, 'Instagram High Quality Likes') !== false
        ) {
            $type = 'like';
        } elseif (stripos($service->name, 'Middle East Instagram Comments') !== false
                  || stripos($service->name, 'Instagram High Quality Comments') !== false
        ) {
            $type = 'comment';
        } elseif (stripos($service->name, 'Middle East Instagram Followers') !== false
                  || stripos($service->name, 'Instagram High Quality Followers') !== false
        ) {
            $type = 'follower';
        } elseif (stripos($service->name, 'Instagram Views') !== false
                  || stripos($service->name, 'Instagram Views') !== false
        ) {
            $type = 'views';
        }

        return $type;
    }

    /**
     * @param                    $ids
     * @param                    $urls
     * @param                    $order
     * @param Services           $service
     * @param \Reseller\Api\Curl $curl
     *
     * @throws Exception
     */
    private function checkIpanel69CommentOrders($ids, $urls, $order, Services $service, \Reseller\Api\Curl $curl)
    {

        //initialize current vars
        $startCount = $order->start_count;
        $desc       = $order->desc;


        $remains  = count($ids);
        $numOrder = $remains;
        $done     = 0;
        $procing  = 0;
        $deleted  = 0;

        $urlCheck = $urls[$service->name];
        foreach ($ids as $id) {
            $http_get = $curl->simple_post($urlCheck, ['id' => $id]);

            if (!$http_get) {
                throw new Exception($curl->error_string);
            }

            $result = json_decode($http_get, true);
            //                                        var_dump($result);
            if ($result['result'] == 'success') {
                if (isset($result['data']['comment_date']) && $result['data']['comment_date']) {
                    $done++;
                } else {
                    $procing++;
                }
            } elseif ($result['result'] == 'error') {
                if (isset($result['msg']) && $result['msg'] == 'not found or deleted') {
                    $deleted++;
                }
            }

        }

        //done
        if ($done >= $numOrder) {
            $status  = 2;
            $remains = 0;
        } else {
            if ($procing) {
                $status  = 1;
                $remains = $procing;
            } else {

                if ($deleted > $numOrder) {
                    $deleted = $numOrder;
                }

                $this->info('$done : '.$done);
                $this->info('$procing : '.$procing);
                $this->info('$remains : '.$remains);
                $this->info('$deleted : '.$deleted);

                $remains = $deleted;
                $status  = 3;
                $desc    = 'Order Refunded Because Instagram Media Probably Deleted OR Comments Not Allowed, Remains : '.$remains;

                $this->updateUserFinance($order->seller_id, $service, $remains);


            }
        }

        $this->updateOrder($order, $startCount, $remains, $status, $desc);

    }

    /**
     * @param                    $ids
     * @param                    $urls
     * @param Order              $order
     * @param Services           $service
     * @param \Reseller\Api\Curl $curl
     *
     * @throws ApiExternalErrorException
     * @throws Exception
     */
    private function checkIpanel69SingleLikesOrders($ids, $urls, Order $order, Services $service, \Reseller\Api\Curl $curl)
    {
        //initialize current vars
        $startCount = $order->start_count;
        $desc       = $order->desc;
        $status     = $order->status;
        $remains    = $order->remains;


        $urlCheck = $urls[$service->name];
        if (!isset($ids[0]) || !$ids[0]) {
            throw new Exception('$ids[0] = false');
        }
        $id = $ids[0];

        $http_get = $curl->simple_post($urlCheck, ['id' => $id]);
        if (!$http_get) {
            throw new Exception($curl->error_string);
        }

        try {

            $result = json_decode($http_get, true);

            if ($result['result'] === 'error') {
                throw new ApiExternalErrorException($result, $order, $service, $result['error']);
            }

            $startCount = (isset($result['data']['initial_likes']) ? $result['data']['initial_likes'] : $startCount);
            $remains    = isset($result['data']['pending']) ? $result['data']['pending'] : $remains;
            $status     = isset($result['data']['pending']) && $result['data']['pending'] == 0 ? 2 : (isset($result['data']['id']) ? 1 : $status);


            if (isset($result['data']['status']) && $result['data']['status'] == 2) {
                $remains = 0;
                $status  = 2;
            } else {
                if (isset($result['data']['status'])) {
                    //private
                    if ($result['data']['status'] == 5 && $order->status != 3) {
                        $remains = $result['data']['pending'];
                        $desc    = 'Order Refunded Because Instagram Picture is Private, Remains : '.$remains;
                        $status  = 3;
                        if ($remains > 0) {
                            $this->updateUserFinance($order->seller_id, $service, $remains);
                        }
                        //deleted
                    } elseif ($result['data']['status'] == 3 && $order->status != 3) {
                        $remains = $result['data']['pending'];
                        if ($remains <= 0) {
                            //success
                            $remains = 0;
                            $status  = 2;

                        } else {
                            //deleted
                            $desc   = 'Order Refunded Because Instagram Picture is Deleted, Remains : '.$remains;
                            $status = 3;
                            //get rates
                            $this->updateUserFinance($order->seller_id, $service, $remains);
                        }
                    }
                }
            }

            $this->updateOrder($order, $startCount, $remains, $status, $desc);

        } catch (ApiExternalErrorException $e) {
            $apiData = $e->apiData;
            if ($apiData['error'] === 'likes order not found') {

                $order   = $e->order;
                $service = $e->service;

                $status = 3;
                if ($order->remains) {
                    $remains = $order->remains;
                } else {
                    $remains = $order->quantity;
                }
                $desc = 'Order refunded, Because likes order not exist on system, please try to reorder';

                $this->updateUserFinance($order->seller_id, $service, $remains);
                $this->updateOrder($order, $order->start_count, $remains, $status, $desc);
            } else {
                $this->error($e->apiData);
                throw $e;
            }

        } catch (Exception $e) {
            throw $e;
        }

    }

    private function checkIpanel69FollowersOrders($ids, $urls, Order $order, Services $service, \Reseller\Api\Curl $curl)
    {
        //initialize current vars
        $startCount = $order->start_count;
        $desc       = $order->desc;
        $status     = $order->status;
        $remains    = $order->remains;


        $urlCheck = $urls[$service->name];
        if (!isset($ids[0]) || !$ids[0]) {
            throw new Exception('$ids[0] = false');
        }
        $id = $ids[0];

        $http_get = $curl->simple_post($urlCheck, ['id' => $id]);
        if (!$http_get) {
            throw new Exception($curl->error_string);
        }

        try {

            $result = json_decode($http_get, true);

            if ($result['result'] === 'error') {
                throw new ApiExternalErrorException($result, $order, $service, $result['error']);
            }


            $isPrivate = false;
            $is404     = false;
            $tooLong   = false;
            if ($result['data']['status'] == 92) {
                $isPrivate = true;
            } elseif ($result['data']['status'] == 91) {
                $is404 = true;
            } elseif ($result['data']['status'] == 93) {
                $tooLong = true;
            }

            $startCount = (isset($result['data']['initial_followers']) ? $result['data']['initial_followers'] : $startCount);
            $remains    = (isset($result['data']['pending_followers']) ? $result['data']['pending_followers'] : $remains);
            $status     = ($result['data']['status'] === 1 ? 2 : $status);
            if ($remains <= 0) {
                $remains = 0;
                $status  = 2;
            } else {
                if ($isPrivate && $order->status != 3) {
                    if ($remains > 0) {
                        $desc   = 'Order Refunded Because Instagram User is Private, Remains : '.$remains;
                        $status = 3;
                        $this->updateUserFinance($order->seller_id, $service, $remains);
                    }


                } elseif ($is404 && $order->status != 3) {
                    if ($remains > 0) {
                        $desc   = 'Order Refunded Because Instagram User Not Found (Maybe Banned), Remains : '.$remains;
                        $status = 3;
                        $this->updateUserFinance($order->seller_id, $service, $remains);
                    }


                } elseif ($tooLong && $order->status != 3) {
                    if ($remains > 0) {
                        //refund
                        $desc   = 'Order Refunded because taking too long, Remains : '.$remains;
                        $status = 3;
                        $this->updateUserFinance($order->seller_id, $service, $remains);
                    }

                }
            }


            $this->updateOrder($order, $startCount, $remains, $status, $desc);

        } catch (ApiExternalErrorException $e) {
            $apiData = $e->apiData;
            if ($apiData['error'] === 'followers order not found') {

                $order   = $e->order;
                $service = $e->service;

                $status = 3;
                if ($order->remains) {
                    $remains = $order->remains;
                } else {
                    $remains = $order->quantity;
                }
                $desc = 'Order refunded, Because followers order not exist on system, please try to reorder';

                $this->updateUserFinance($order->seller_id, $service, $remains);
                $this->updateOrder($order, $order->start_count, $remains, $status, $desc);
            } else {
                $this->error($e->apiData);
                throw $e;
            }

        } catch (Exception $e) {
            throw $e;
        }

    }

    private function checkIpanel69SingleViewsOrders($ids, $urls, Order $order, Services $service, \Reseller\Api\Curl $curl)
    {
        //initialize current vars
        $startCount = $order->start_count;
        $desc       = $order->desc;
        $status     = $order->status;
        $remains    = $order->remains;


        $urlCheck = $urls[$service->name];
        if (!isset($ids[0]) || !$ids[0]) {
            throw new Exception('$ids[0] = false');
        }
        $id = $ids[0];

        $http_get = $curl->simple_post($urlCheck, ['id' => $id]);
        if (!$http_get) {
            throw new Exception($curl->error_string);
        }

        try {

            $result = json_decode($http_get, true);

            if ($result['result'] === 'error') {
                throw new ApiExternalErrorException($result, $order, $service, $result['error']);
            }


            $startCount = isset($result['data']['initial_views']) ? $result['data']['initial_views'] : $startCount;
            $remains    = isset($result['data']['pending']) ? $result['data']['pending'] : $remains;
            $status     = isset($result['data']['pending']) && $result['data']['pending'] == 0 ? 2 : (isset($result['data']['id']) ? 1 : $status);

            if (isset($result['data']['status']) && $result['data']['status'] == 2) {
                $remains = 0;
                $status  = 2;
            } else {
                if (isset($result['data']['status'])) {
                    //private
                    if ($result['data']['status'] == 5 && $order->status != 3) {
                        $remains = $result['data']['pending'];
                        if ($remains <= 0) {
                            //success
                            $remains = 0;
                            $status  = 2;
                        } else {
                            $desc   = 'Order Refunded Because Instagram Video is Private, Remains : '.$remains;
                            $status = 3;
                            $this->updateUserFinance($order->seller_id, $service, $remains);
                        }


                    } else {
                        if ($result['data']['status'] == 3 && $order->status != 3) {
                            $remains = $result['data']['pending'];

                            if ($remains <= 0) {
                                //success
                                $remains = 0;
                                $status  = 2;
                            } else {
                                $desc   = 'Order Refunded Because Instagram Video is Deleted, Remains : '.$remains;
                                $status = 3;
                                $this->updateUserFinance($order->seller_id, $service, $remains);
                            }

                        }
                    }
                }
            }


            $this->updateOrder($order, $startCount, $remains, $status, $desc);
        } catch (ApiExternalErrorException $e) {
            $apiData = $e->apiData;
            if ($apiData['error'] === 'views order not found') {

                $order   = $e->order;
                $service = $e->service;

                $status = 3;
                if ($order->remains) {
                    $remains = $order->remains;
                } else {
                    $remains = $order->quantity;
                }
                $desc = 'Order refunded, Because views order not exist on system, please try to reorder';

                $this->updateUserFinance($order->seller_id, $service, $remains);
                $this->updateOrder($order, $order->start_count, $remains, $status, $desc);
            } else {
                $this->error($e->apiData);
                throw $e;
            }

        } catch (Exception $e) {
            throw $e;
        }

    }

    /**
     * @param Order $order
     * @param       $startCount
     * @param       $remains
     * @param       $status
     * @param       $desc
     */
    public function updateOrder(Order $order, $startCount, $remains, $status, $desc)
    {
        $order->start_count = $startCount;
        $order->remains     = $remains;
        $order->status      = $status;
        $order->desc        = $desc;
        $order->save();
    }

    /**
     * @param          $userId
     * @param Services $service
     * @param          $remains
     *
     * @throws Exception
     */
    private function updateUserFinance($userId, Services $service, $remains)
    {
        $rate     = Rate::where('user_id', '=', $userId)
                        ->where('service_id', '=', $service->id)->first();
        $price    = is_null($rate) ? $service->price : $rate->rates;
        $Corder   = 1000 / $remains;
        $toRefund = $price / $Corder;


        $user = User::find($userId);
        if (!$user) {
            throw new Exception('User Not Found @updateUserFinance');
        }
        if ($user) {
            $user->balance += $toRefund;
            $spent         = $user->spent - $toRefund;
            if ($spent > 0) {
                $user->spent = $spent;
            }
            $user->save();
        }
    }

    private function serviceDisabled($order, $service)
    {
        $this->error('Service Disabled '.$service->name);
        $this->error(json_encode($order));
        $status = 3;
        $desc   = 'Order Refunded Because Service Deleted / Disabled on System, Refund All Remains : '.$order->quantity;

        $toRefund = $order->charge;

        $user = User::find($order->seller_id);
        if ($user) {
            $user->balance += $toRefund;
            $spent         = $user->spent - $toRefund;
            if ($spent > 0) {
                $user->spent = $spent;
            }
            $user->save();
        }

        $order->remains = $order->quantity;
        $order->status  = $status;
        $order->desc    = $desc;
        $order->save();
    }

    private function serviceNotFound($order, $serviceTypeId)
    {
        $this->error('Service Not Found '.$serviceTypeId);
        $this->error(json_encode($order));

        $status = 3;
        $desc   = 'Order Refunded Because Service Deleted / Disabled on System, Refund All Remains : '.$order->quantity;

        $toRefund = $order->charge;

        $user = User::find($order->seller_id);
        if ($user) {
            $user->balance += $toRefund;
            $spent         = $user->spent - $toRefund;
            if ($spent > 0) {
                $user->spent = $spent;
            }
            $user->save();
        }

        $order->remains = $order->quantity;
        $order->status  = $status;
        $order->desc    = $desc;
        $order->save();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array(
                'max_id',
                InputArgument::OPTIONAL,
                'max_id',
            ),
            array(
                'delimiter',
                InputArgument::OPTIONAL,
                'Delimiter',
            ),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array(
                'example',
                null,
                InputOption::VALUE_OPTIONAL,
                'An example option.',
                null,
            ),
        );
    }

    private static function xg_referrer($url, $referrer)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt(
            $ch,
            CURLOPT_USERAGENT,
            'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36'
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_REFERER, $referrer);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_COOKIEFILE, '/tmp/gg.out');
        curl_setopt($ch, CURLOPT_COOKIEJAR, '/tmp/gg.out');
        $result['result'] = curl_exec($ch);

//        $err            = curl_error($ch);
        $result['info'] = curl_getinfo($ch);
        curl_close($ch);

        return $result;
    }

    private static function filter_text($start, $end, $str_page)
    {
        $pos = strpos($str_page, $start);
        if ($pos != false) {
            $pos         = $pos + strlen($start);
            $field_value = substr($str_page, $pos);
            $pos         = strpos($field_value, $end);
            $field_value = substr($field_value, 0, $pos);
            $field_value = trim($field_value);
        } else {
            $field_value = "";
        }

        return $field_value;
    }

    public function __destruct()
    {
        \Illuminate\Support\Facades\Cache::forget('orders_check');
    }
}

class PanelServiceNotFoundException extends Exception
{
    public $order;
    public $serviceTypeId;

    public function __construct($order, $serviceTypeId, $message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->order         = $order;
        $this->serviceTypeId = $serviceTypeId;
    }
}

class PanelServiceDisabledException extends Exception
{
    public $order;
    public $service;

    public function __construct($order, $service, $message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->order   = $order;
        $this->service = $service;
    }
}

class ApiExternalErrorException extends Exception
{
    public $apiData = [];
    public $order;
    public $service;


    public function __construct($apiData, Order $order, Services $service, $message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->apiData = $apiData;
        $this->order   = $order;
        $this->service = $service;
    }
}
