<?php

class Ticket extends \Eloquent {
	protected $table = 'tickets';
	protected $fillable = array('title', 'content', 'user_id', 'status','reply_to', 'reply_from','is_read');
}