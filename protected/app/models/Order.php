<?php

/**
 * @property  int start_count
 * @property  int remains
 * @property  int status
 * @property  string desc
 * @property  int seller_id
 * @property  int quantity
 */
class Order extends \Eloquent {
	
	protected $table = 'orders';
	
	public function one_service() {
		return $this -> hasOne('Services', 'id', 'service_type_id');
	}
	
	public function one_user() {
		return $this -> hasOne('User', 'id', 'seller_id');
	}
        
    public function comments()
    {
		return $this->hasOne('Answer','order_id','id');
    }
	
	public function service() {
		return $this->belongsTo('Services', 'service_type_id');
	}
	
	public function plan() {
		return $this->belongsTo('ServicePlans', 'service_plan_id');
	}
	
	public function seller() {
		return $this->belongsTo('User', 'seller_id');
	}
}