<?php

class ApiKeyGenerator extends \Eloquent {
        protected $table = 'api', $fillable = array('user_id', 'api_key', 'api_enable');

        public function user() {
                return $this -> belongsTo('User', 'user_id');
        }

}
