<?php

class Page extends \Eloquent {
	protected $table = 'pages';
	protected $fillable = array('name','title', 'content');
}