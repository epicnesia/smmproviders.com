<?php

class ServicePlans extends \Eloquent {
	
	protected $table = 'service_plans',
			  $fillable = array('service_id', 'name', 'count', 'price_7_days', 'price_30_days', 'price_60_days', 'price_90_days',
			  					'cost_7_days', 'cost_30_days', 'cost_60_days', 'cost_90_days', 'status');
							
	
	public function service() {
		return $this->belongsTo('Services', 'service_id');
	}
	
	public function rate() {
		return $this->hasOne('ServicePlanRates', 'plan_id');
	}
	
	public static function boot()
    {
    	
		parent::boot();
		
		static::deleted(function($data)
		{
			$data->rate()->delete();
		});
		
	}   
	
}