<?php


/**
 * @property  string name
 * @property  int id
 * @property  float price
 */
class Services extends \Eloquent {

	protected $table = 'services',
			  $fillable = array('name', 'price', 'max_amount', 'service_cost', 'status', 'is_subscription');
	
	public function plans() {
		return $this->hasMany('ServicePlans', 'service_id');
	}
}