<?php

class Payment extends \Eloquent {
	protected $table = 'payments', $fillable = array('transaction_id', 'user_id', 'balance', 'amount', 'details');
	
	public function one_user() {
		return $this -> hasOne('User','id', 'user_id');
	}
}