<?php

class ServicePlanRates extends \Eloquent {
	protected $table = 'service_plan_rates',
			  $fillable = array('user_id', 
			  					'plan_id',
								'rate_7_days',
								'rate_30_days',
								'rate_60_days',
								'rate_90_days',
								);
								
	public function plan() {
		return $this->belongsTo('ServicePlans', 'plan_id');
	}
	
}