<?php

class BalanceAdder extends \Eloquent {
	protected $table='balance_history', $fillable = array('user_id', 'balance', 'amount', 'by');
	
	public function one_user() {
		return $this -> hasOne('User','id', 'user_id');
	}
}