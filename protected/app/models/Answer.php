<?php

class Answer extends \Eloquent {
	protected $table = 'answers', $fillable = array('order_id', 'question', 'answers');
        
        public function order()
        {
                return $this -> belongsTo('Order', 'order_id');
        }
}