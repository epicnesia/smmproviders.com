<?php

class TicketUnread extends \Eloquent {
	protected $table = 'tickets_unread';
    protected $fillable = array('ticket_id', 'user_id');
}