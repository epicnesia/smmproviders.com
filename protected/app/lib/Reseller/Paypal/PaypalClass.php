<?php

namespace Reseller\Paypal;

use Illuminate\Support\Facades\Config;

class PaypalClass
{	
	var $logfile='ipnlog.txt';
	var $form=array();
	var $log=0;	
	var $form_action='';	
	var $paypalurl='';
	var $type='payment';
	var $posted_data=array();
	var $action='';
	var $error='';
	var $ipn='';
	var $price=0;
	var $payment_success=0;
	var $ignore_type=array();

	public function __construct($price_item=0) {
		if(Config::get('paypal.use_sandbox')){
			$this->form_action = Config::get('paypal.form_action_sandbox'); 
			$this->paypalurl = Config::get('paypal.paypalurl_sandbox');
		}else{
			$this->form_action = Config::get('paypal.form_action'); 
			$this->paypalurl = Config::get('paypal.paypalurl');
		}
		$this->price=$price_item;
	}
	
	function Paypal($price_item=0)
	{		
		$this->price=$price_item;
	}

	function validate_ipn()
	{
		if(!empty($_POST))
		{
			$postvars='';
			$this->price=0;

			foreach($_POST as $key=>$value)
			{
				$postvars.=$key. '=' . urlencode($value) . '&';
				$this->posted_data[$key]=$value;
			}


            $req = 'cmd=_notify-validate';
            if(function_exists('get_magic_quotes_gpc')) {
                $get_magic_quotes_exists = true;
            }
            foreach ($_POST as $key => $value) {
                if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                    $value = urlencode(stripslashes($value));
                } else {
                    $value = urlencode($value);
                }
                $req .= "&$key=$value";
            }

            $ch = curl_init('https://' . $this->paypalurl . '/cgi-bin/webscr');
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

// In wamp-like environments that do not come bundled with root authority certificates,
// please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set
// the directory path of the certificate as shown below:
// curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
            if( !($res = curl_exec($ch)) ) {
                // error_log("Got " . curl_error($ch) . " when processing IPN data");
                curl_close($ch);
                exit;
            }
            curl_close($ch);

            $str = $res;
            file_put_contents('/tmp/logpaypal', print_r($str, true), FILE_APPEND);

			if(strcmp(trim($str), "VERIFIED") == 0)
			{
				if($this->log)
				$this->log_results(1);
					
				if(preg_match('/subscr/',$this->posted_data['txn_type']))
				{
					$this->type='subscription';

					if(in_array($this->posted_data['txn_type'],$this->ignore_type))
					return 0;
					
					if($this->posted_data['txn_type']=='subscr_payment')
					{
						if($this->posted_data['payment_status']=='Completed')
						{
							$this->price=$this->posted_data['mc_amount3'];
							$this->payment_success=1;
						}
					}

				}
				else
				{
					if($this->posted_data['payment_status']=='Completed')
					{
						$this->type='payment';
						$this->price=$this->posted_data['mc_gross'];
						$this->payment_success=1;
					}
				}

				return 1;
			}
			else
			{
				if($this->log)
				$this->log_results(0);

				$this->error='IPN verification failed.';
				return 0;
			}
		}
		else
		return 0;
	}

	function add($name,$value)
	{
		$this->form[$name]=$value;
	}

	function remove($name)
	{
		unset($this->form[$name]);
	}

	function enable_recurring()
	{
		$this->type='subscription';
		$this->add('src','1');
		$this->add('sra','1');
		$this->add('cmd','_xclick-subscriptions');
		$this->remove('amount');
		$this->add('no_note',1);
		$this->add('no_shipping',1);
		$this->add('currency_code','USD');
		$this->add('a3',$this->price);
		$this->add('notify_url',$this->ipn);

	}

	function recurring_year($num)
	{
		$this->enable_recurring();
		$this->add('t3','Y');
		$this->add('p3',$num);
	}

	function recurring_month($num)
	{
		$this->enable_recurring();
		$this->add('t3','M');
		$this->add('p3',$num);
	}

	function recurring_day($num)
	{
		$this->enable_recurring();
		$this->add('t3','D');
		$this->add('p3',$num);
	}


	function enable_payment()
	{
		$this->type='payment';
		$this->remove('t3');
		$this->remove('p3');
		$this->remove('a3');
		$this->remove('src');
		$this->remove('sra');
		$this->add('amount',$this->price);
		$this->add('cmd','_xclick');
		$this->add('rm',2);
		$this->add('no_note',1);
		$this->add('no_shipping',1);
		$this->add('currency_code','USD');
		$this->add('notify_url',$this->ipn);
	}
	function output_form()
	{

		echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'
		. '<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Redirecting to PayPal...</title></head>'
		.'<body onload="document.f.submit();"><h3>Redirecting to PayPal...</h3>'
		. '<form name="f" action="'.$this->form_action.'" method="post">';

		foreach($this->form as $k=>$v)
		{
			echo '<input type="hidden" name="' . $k . '" value="' . $v . '" />';
		}

		echo '<input type="submit" value="Click here if you are not redirected within 10 seconds" /></form></body></html>';


	}

	function reset_form()
	{
		$this->form=array();
	}

	function log_results($var)
	{
		$fp=@ fopen($this->logfile,'a');
		$data=date('m/d/Y g:i A');
		if($var==1)
		{
			$str="\nIPN PAYPAL TRANSACTION ID: " . $this->posted_data['txn_id'] ."\n";
			$str.="SUCCESS\n";
			$str.="DATE: ". $data . "\n";
			$str.="PAYER EMAIL: " . $this->posted_data['payer_email']. "\n";
			$str.="NAME: " . $this->posted_data['last_name']." ".$this->posted_data['first_name']. "\n";
			$str.="LINK ID: ". $this->posted_data['item_number']. "\n";
			$str.="QUANTITY: ". $this->posted_data['quantity']. "\n";
			$str.="TOTAL: "   . $this->posted_data['mc_gross']. "\n\n\n";
		}
		else
		{
			$str="\nIPN PAYPAL TRANSACTION ID:\n";
			$str.="INVALID\n";
			$str.="REMOTE IP: " . $_SERVER['REMOTE_ADDR'] . "\n";
			$str.="ERROR: ". $this->posted_data['error'] . "\n";
			$str.="DATE: ". $data . "\n";
			$str.="PAYER EMAIL: " . $this->posted_data['payer_email']. "\n";
			$str.="NAME: " . $this->posted_data['last_name']." ".$this->posted_data['first_name']. "\n";
			$str.="LINK ID: ". $this->posted_data['item_number']. "\n";
			$str.="QUANTITY: ". $this->posted_data['quantity']. "\n";
			$str.="TOTAL: "   . $this->posted_data['mc_gross']. "\n\n\n";
		}
		if($fp)
		@ fputs($fp,$str);

		@ fclose($fp);
	}

	function headers_nocache()
	{
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: pre-check=0, post-check=0, max-age=0');
		header('Pragma: no-cache');
	}


}
