<?php

class SentryUserSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        Sentry::getUserProvider()->create(
            array(
                'email'     => 'su_admin@fg.com',
                'username'  => 'su_admin',
                'password'  => '123456',    
                'password_nohash'  => '123456',            
                'activated' => 1,
            )
        );

        Sentry::getUserProvider()->create(
            array(
                'email'     => 'admin@fg.com',
                'username'  => 'admin',
                'password'  => '123456',    
                'password_nohash'  => '123456',            
                'activated' => 1,
            )
        );

        Sentry::getUserProvider()->create(
            array(
                'email'     => 'marketer@fg.com',
                'username'  => 'marketer',
                'password'  => '123456',
                'password_nohash'  => '123456',
                'activated' => 1,
            )
        );

        Sentry::getUserProvider()->create(
            array(
                'email'     => 'worker@fg.com',
                'username'  => 'worker',
                'password'  => '123456',
                'password_nohash'  => '123456',
                'activated' => 1,
            )
        );

        Sentry::getUserProvider()->create(
            array(
                'email'     => 'accountant@fg.com',
                'username'  => 'accountant',
                'password'  => '123456',
                'password_nohash'  => '123456',
                'activated' => 1,
            )
        );
		
		
    }

}