<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class StatusTableSeeder extends Seeder {

	public function run()
	{
		DB::table('status')->delete();

		DB::table('status')->insert(array (
			0 => 
			array (
				'id' => 0,
				'name' => 'Pending',				
			),
			1 => 
			array (
				'id' => 1,
				'name' => 'Processing',				
			),
			2 => 
			array (
				'id' => 2,
				'name' => 'Completed',				
			),
            3 =>
                array (
                    'id' => 3,
                    'name' => 'Refunded',
                )
		));
		
	}

}