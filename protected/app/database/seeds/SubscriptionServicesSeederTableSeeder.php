<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class SubscriptionServicesSeederTableSeeder extends Seeder {

	public function run()
	{
		\DB::table('services')->insert([
			[
				'name'				=>	'Auto Likes',
				'price'				=> 	'10',
				'created_at' 		=> date('Y-m-d H:i:s'),
				'updated_at' 		=> date('Y-m-d H:i:s'),
                'status' 			=> '1',
                'is_subscription' 	=> '1'
			],
			[
				'name'				=>	'Auto Views',
				'price'				=> 	'10',
				'created_at' 		=> date('Y-m-d H:i:s'),
				'updated_at' 		=> date('Y-m-d H:i:s'),
                'status' 			=> '1',
                'is_subscription' 	=> '1'
			],
		]);
	}

}