<?php

class SentryGroupSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('groups')->delete();

		Sentry::getGroupProvider()->create(array(
                'name'        => 'Super Admins',
                'views'        => 'admin_v.group_v.show',               
                'permissions' => array(
                    'su_admin' => 1,
                    'admin' => 1,
                    'users' => 1,
                )));
                
                Sentry::getGroupProvider()->create(array(
                'name'        => 'Admins',
                'views'        => 'admin_v.group_v.show',               
                'permissions' => array(
                    'admin' => 1,
                    'users' => 1,
                )));
			
		Sentry::getGroupProvider()->create(array(
	        'name'        => 'Super Marketer',
	        'views'        => 'users_v.super_marketer',	
	        'permissions' => array(
	            'admin' => 0,
	            'users' => 1,
	        )));
			
		Sentry::getGroupProvider()->create(array(
	        'name'        => 'Marketer',
	        'views'        => 'users_v.marketer',	
	        'permissions' => array(
	            'admin' => 0,
	            'users' => 1,
	        )));
			
		Sentry::getGroupProvider()->create(array(
	        'name'        => 'Worker',
	        'views'        => 'users_v.workers',	
	        'permissions' => array(
	            'admin' => 0,
	            'users' => 1,
	        )));
			
		Sentry::getGroupProvider()->create(array(
	        'name'        => 'Accountant',
	        'views'        => 'users_v.accountan',	
	        'permissions' => array(
	            'admin' => 0,
	            'users' => 1,
	        )));
			
	}

}