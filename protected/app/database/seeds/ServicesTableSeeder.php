<?php

class ServicesTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('services')->truncate();
        
		\DB::table('services')->insert(array (
			0 => 
			array (
				'id' => 1,
                                'name' => 'Middle East Instagram Likes',
                                'price' => '10',
				'created_at' => '2014-12-10 08:25:13',
				'updated_at' => '2014-12-10 08:25:13',
                                'status' => '1'
			),
                        1 => 
                        array (
                                'id' => 2,
                                'name' => 'Middle East Instagram Followers',
                                'price' => '10',
                                'created_at' => '2014-12-10 08:26:17',
                                'updated_at' => '2014-12-10 08:26:17',
                                'status' => '1'
                        ),
                        2 => 
                        array (
                                'id' => 3,
                                'name' => 'Middle East Instagram Comments',
                                'price' => '10',
                                'created_at' => '2014-12-10 08:27:17',
                                'updated_at' => '2014-12-10 08:27:17',
                                'status' => '1'
                        )
		));
	}

}
