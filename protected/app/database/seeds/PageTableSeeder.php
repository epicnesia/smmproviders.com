<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class PageTableSeeder extends Seeder {

	public function run()
	{
		DB::table('pages')->delete();

		DB::table('pages')->insert(array (
			0 => 
			array (
				'id' => 0,
				'name' => 'faq',
				'title' => 'FAQ',
				'content' => '<p style="font-size:20px">
							Maximal order for one page:
							<br />
							How to place new order:
							<br />
							How to fill the field Link depending on the type of service:
							<br />
							What is Partially completed status:
							</p>',				
			)
		));
	}

}