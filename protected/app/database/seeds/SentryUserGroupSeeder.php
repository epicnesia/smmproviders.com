<?php

class SentryUserGroupSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('users_groups')->delete();


		$su_admin = Sentry::getUserProvider()->findByLogin('su_admin@fg.com');
                $admin = Sentry::getUserProvider()->findByLogin('admin@fg.com');
                $marketer = Sentry::getUserProvider()->findByLogin('marketer@fg.com');
                $worker = Sentry::getUserProvider()->findByLogin('worker@fg.com');
                $accountant = Sentry::getUserProvider()->findByLogin('accountant@fg.com');
        
                $suAdminGroup = Sentry::getGroupProvider()->findByName('Super Admins');
                $adminGroup = Sentry::getGroupProvider()->findByName('Admins');
                $marketerGroup = Sentry::getGroupProvider()->findByName('Marketer');
                $workerGroup = Sentry::getGroupProvider()->findByName('Worker');
                $accountantGroup = Sentry::getGroupProvider()->findByName('Accountant');
        
                $su_admin->addGroup($suAdminGroup);
                $admin->addGroup($adminGroup);
                $marketer->addGroup($marketerGroup);
                $worker->addGroup($workerGroup);
                $accountant->addGroup($accountantGroup);
	}

}