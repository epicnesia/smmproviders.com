<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class RemoveAllPricesAndCostsFromServices extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('services', function(Blueprint $table)
		{
			$table->dropColumn('price_7_days');
			$table->dropColumn('price_30_days');
			$table->dropColumn('price_60_days');
			$table->dropColumn('price_90_days');
			$table->dropColumn('cost_7_days');
			$table->dropColumn('cost_30_days');
			$table->dropColumn('cost_60_days');
			$table->dropColumn('cost_90_days');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('services', function(Blueprint $table)
		{
			//
		});
	}

}
