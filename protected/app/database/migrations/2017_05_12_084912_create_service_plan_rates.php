<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateServicePlanRates extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('service_plan_rates', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned()->nullable();
			$table->integer('plan_id')->unsigned()->nullable();
			$table->decimal('rate_7_days', 10, 2)->default(0);
			$table->decimal('rate_30_days', 10, 2)->default(0);
			$table->decimal('rate_60_days', 10, 2)->default(0);
			$table->decimal('rate_90_days', 10, 2)->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('service_plan_rates');
	}

}
