<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddPricesToServicesAndRates extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('services', function(Blueprint $table)
		{
			$table->decimal('price_7_days', 10, 2)->default(0);
			$table->decimal('price_30_days', 10, 2)->default(0);
			$table->decimal('price_60_days', 10, 2)->default(0);
			$table->decimal('price_90_days', 10, 2)->default(0);
		});
		
		Schema::table('rates', function(Blueprint $table)
		{
			$table->decimal('price_7_days', 10, 2)->default(0);
			$table->decimal('price_30_days', 10, 2)->default(0);
			$table->decimal('price_60_days', 10, 2)->default(0);
			$table->decimal('price_90_days', 10, 2)->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('services', function(Blueprint $table)
		{
			$table->dropColumn('price_7_days', 10, 2);
			$table->dropColumn('price_30_days', 10, 2);
			$table->dropColumn('price_60_days', 10, 2);
			$table->dropColumn('price_90_days', 10, 2);
		});
		
		Schema::table('rates', function(Blueprint $table)
		{
			$table->dropColumn('price_7_days', 10, 2);
			$table->dropColumn('price_30_days', 10, 2);
			$table->dropColumn('price_60_days', 10, 2);
			$table->dropColumn('price_90_days', 10, 2);
		});
	}

}
