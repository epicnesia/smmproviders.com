<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrders extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('re')->unsigned();
			$table->string('link')->nullable();
			$table->string('charge')->default('0');
			$table->integer('start_count')->unsigned()->nullable();
			$table->integer('quantity')->unsigned();
			$table->integer('service_type_id')->unsigned()->nullable();
			$table->integer('status')->unsigned()->default('0');
			$table->string('remains')->nullable();	
			$table->string('user_id');
			
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
