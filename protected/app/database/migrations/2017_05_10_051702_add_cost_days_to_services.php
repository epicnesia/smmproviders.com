<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddCostDaysToServices extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('services', function(Blueprint $table)
		{
			$table->decimal('cost_7_days', 10, 2)->default(0);
			$table->decimal('cost_30_days', 10, 2)->default(0);
			$table->decimal('cost_60_days', 10, 2)->default(0);
			$table->decimal('cost_90_days', 10, 2)->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('services', function(Blueprint $table)
		{
			$table->dropColumn('cost_90_days');
			$table->dropColumn('cost_60_days');
			$table->dropColumn('cost_30_days');
			$table->dropColumn('cost_7_days');
		});
	}

}
