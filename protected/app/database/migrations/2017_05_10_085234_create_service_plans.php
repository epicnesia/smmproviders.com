<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateServicePlans extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('service_plans', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('service_id')->nullable()->unsigned();
			$table->string('name')->nullable();
			$table->integer('count')->unsigned()->default(0)->nullable();
			$table->decimal('price_7_days', 10, 2)->default(0);
			$table->decimal('price_30_days', 10, 2)->default(0);
			$table->decimal('price_60_days', 10, 2)->default(0);
			$table->decimal('price_90_days', 10, 2)->default(0);
			$table->decimal('cost_7_days', 10, 2)->default(0);
			$table->decimal('cost_30_days', 10, 2)->default(0);
			$table->decimal('cost_60_days', 10, 2)->default(0);
			$table->decimal('cost_90_days', 10, 2)->default(0);
			$table->tinyInteger('status')->default(1)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('service_plans');
	}

}
