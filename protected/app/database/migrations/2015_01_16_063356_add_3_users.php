<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Add3Users extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table -> string('balance')->default('0');
			$table -> string('spent')->default('0');

			$table -> datetime('lastlogin')->nullable();
			$table -> integer('rates')->nullable()->default(0);
			
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{		
	}

}
