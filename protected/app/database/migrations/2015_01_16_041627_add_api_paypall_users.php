<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddApiPaypallUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table -> string('social_bulk_api_key')->nullable();
			$table -> string('youTube_sevice_api_key')->nullable();

			$table -> string('paypall_api_username')->nullable();
			$table -> string('paypall_api_password')->nullable();
			$table -> string('paypall_api_signature')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		
	}

}
