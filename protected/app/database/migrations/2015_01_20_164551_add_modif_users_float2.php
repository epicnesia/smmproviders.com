<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddModifUsersFloat2 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement("ALTER TABLE `users` MODIFY `balance` decimal(10,2) NOT NULL DEFAULT '0.00';");
		DB::statement("ALTER TABLE `users` MODIFY `spent` decimal(10,2) NOT NULL DEFAULT '0.00';");
		DB::statement("ALTER TABLE `users` MODIFY `rates` decimal(10,2) NOT NULL DEFAULT '0.00';");
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		
	}

}
