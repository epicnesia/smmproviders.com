<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddModifUsersFloat extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement("ALTER TABLE `users` MODIFY `balance` float(8,2) NOT NULL DEFAULT '0.00';");
		DB::statement("ALTER TABLE `users` MODIFY `spent` float(8,2) NOT NULL DEFAULT '0.00';");
		DB::statement("ALTER TABLE `users` MODIFY `rates` float(8,2) NOT NULL DEFAULT '0.00';");
			
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		
	}

}
