@extends('layouts.default')

@section('title')
@parent
{{ trans('pages.wellcometo') }} {{ trans('pages.usermanagement') }}
@stop

@section('head')
        @include('template.head')
@stop

@section('navbar')
        @include('template.navbar')
@stop

@section('wellcome')
@parent
{{ trans('pages.wellcometo') }} {{ trans('pages.usermanagement') }}
@stop

@section('notifications')
@parent
@include('layouts/notifications')
@stop

@section('content')
        @include('template.page.list')
@stop

@section('footer')
        @include('template.footer')        
@stop
