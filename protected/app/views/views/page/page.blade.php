@extends('layouts.default')

@section('title')
@parent
{{ trans('pages.wellcometo') }} {{ trans('pages.usermanagement') }}
@stop

@section('head')
        @include('template.head')
@stop

@section('navbar')
        @include('template.navbar')
@stop

@section('wellcome')
@parent
{{ trans('pages.wellcometo') }} {{ trans('pages.usermanagement') }}
@stop

@section('notifications')
@parent
@include('layouts/notifications')
@stop

@section('content')
        @include('template.page.page')
@stop

@section('footer')
        @include('template.footer')
        {{ HTML::script('/protected/assets/ckeditor/ckeditor.js'); }}
        <script type="text/javascript">
	    $(function() {
	        // Replace the <textarea id="editor1"> with a CKEditor
	        // instance, using default configuration.
	        CKEDITOR.replace('editor1');
	        //bootstrap WYSIHTML5 - text editor
	        $(".textarea").wysihtml5();
	    });
		</script>
@stop
