@extends('layouts.default')

@section('title')
@parent
{{ trans('pages.wellcometo') }} {{ trans('pages.apiKeyGenerator') }}
@stop

@section('head')
        @include('template.head')
@stop

@section('navbar')
        @include('template.navbar')
@stop

@section('wellcome')
@parent
{{ trans('pages.wellcometo') }} {{ trans('pages.apiKeyGenerator') }}
@stop

@section('notifications')
@parent
@include('layouts/notifications')
@stop

@section('content')
        @include('template.admin.api_key_generator')
@stop

@section('footer')
        @include('template.footer')
@stop
