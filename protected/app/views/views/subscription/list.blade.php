@extends('layouts.default')

@section('title')
@parent
{{ trans('pages.wellcometo') }} {{ trans('pages.usermanagement') }}
@stop

@section('add_style')
@parent
{{ HTML::style('/protected/assets/css/intlTelInput.css') }}
@stop

@section('head')
        @include('template.head')
@stop

@section('navbar')
        @include('template.navbar')
@stop

@section('wellcome')
@parent
{{ trans('pages.wellcometo') }} {{ trans('pages.usermanagement') }}
@stop

@section('notifications')
@parent
@include('layouts/notifications')
@stop

@section('content')
        @include('template.subscription.list')
@stop

@section('add_script')
@parent
{{ HTML::script('/protected/assets/js/intlTelInput.min.js'); }}
@stop

@section('footer')
        @include('template.footer')
@stop
