<!DOCTYPE html>
<html lang="en">
        @yield('head')
        <body>
                
                <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                @yield('navbar')
                </div>
                
                <div class="container-fluid">
                        <!-- Notifications -->
                        @section('notifications')
                        @show
                        <!-- ./ notifications -->
                        @yield('content')
                
                </div><!-- /.container -->
                
                @yield('footer')
                
        </body>
</html>
