<h2>{{trans('pages.create')}} {{trans('pages.order') }}</h2>
    
<div class="well">
    {{ Form::open(array('action' => 'OrderController@store', 'class' => 'form-horizontal', 'role' => 'form', 'files'=> true)) }}

        <div class="form-group {{ ($errors->has('services')) ? 'has-error' : '' }}">
            
                {{ Form::label('type', trans('orders.type'), array('class' => 'col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    {{ Form::select('services', $services, Input::old('services'), array('class'=>'form-control', 'id'=>'selectService')) }}
                </div>
                <div class="col-sm-offset-2 col-sm-10">
                        {{ ($errors->has('services') ? $errors->first('services') : '') }}
                </div>
        
        </div>
        
        <div id="pricePer1000" class="form-group"></div>
        
        <div class="form-group {{ ($errors->has('link')) ? 'has-error' : '' }}">
                
                {{ Form::label('link', Session::has('service') && Session::get('service') == 5 ? 'Client Username @' : trans('orders.link'), array('class' => 'col-sm-2 control-label')) }}
                <div class="col-sm-10">
                	<?php $plhd = Session::has('service') && Session::get('service') == 5 ? 'Username' : 'Link'; ?>
                    {{ Form::text('link', Input::old('link'), array('class'=>'form-control', 'id'=>'link', 'placeholder'=> $plhd)) }}
                </div>
                <div class="col-sm-offset-2 col-sm-10">
                        {{ ($errors->has('link') ? $errors->first('link') : '') }}
                </div>
        
        </div>
        
        <?php 
        
                if(Session::has('service') && Session::get('service') == 1){
                        $display_q = 'style="display:none;"';
                        $display_c = 'style="display:block;"';
                        $display_d = 'style="display:none;"';
                        $display_mu = 'style="display:none;"';
                        $display_mh = 'style="display:none;"';
                        $display_cl = 'style="display:none;"';
                        $display_cm = 'style="display:none;"';
                        $display_clf = 'style="display:none;"';                        
                }elseif(Session::has('service') && Session::get('service') == 2){
                        $display_q = 'style="display:block;"';
                        $display_c = 'style="display:none;"';
                        $display_d = 'style="display:block;"';
                        $display_mu = 'style="display:none;"';
                        $display_mh = 'style="display:none;"';
                        $display_cl = 'style="display:none;"';
                        $display_cm = 'style="display:none;"';
                        $display_clf = 'style="display:none;"';                         
                }elseif(Session::has('service') && Session::get('service') == 3){
                        $display_q = 'style="display:block;"';
                        $display_c = 'style="display:none;"';
                        $display_d = 'style="display:none;"';
                        $display_mu = 'style="display:block;"';
                        $display_mh = 'style="display:block;"';
                        $display_cl = 'style="display:none;"';
                        $display_cm = 'style="display:block;"';
                        $display_clf = 'style="display:none;"';                         
                }elseif(Session::has('service') && Session::get('service') == 4){
                        $display_q = 'style="display:none;"';
                        $display_c = 'style="display:none;"';
                        $display_d = 'style="display:none;"';
                        $display_mu = 'style="display:none;"';
                        $display_mh = 'style="display:none;"';
                        $display_cl = 'style="display:block;"';
                        $display_cm = 'style="display:block;"';
                        $display_clf = 'style="display:block;"';                         
                }elseif(Session::has('service') && Session::get('service') == 5){
                        $display_q = 'style="display:block;"';
                        $display_c = 'style="display:none;"';
                        $display_d = 'style="display:none;"';
                        $display_mu = 'style="display:block;"';
                        $display_mh = 'style="display:block;"';
                        $display_cl = 'style="display:none;"';
                        $display_cm = 'style="display:none;"';
                        $display_clf = 'style="display:none;"';                    
                }else{
                        $display_q = 'style="display:block;"';
                        $display_c = 'style="display:none;"';
                        $display_d = 'style="display:none;"';
                        $display_mu = 'style="display:none;"';
                        $display_mh = 'style="display:none;"'; 
                        $display_cl = 'style="display:none;"';
                        $display_cm = 'style="display:none;"';
                        $display_clf = 'style="display:none;"';                        
                }
                
        ?>
        
        <div id="phoneForm" class="form-group {{ ($errors->has('phone')) ? 'has-error' : '' }}" {{$display_d}}>                
                {{ Form::label('phone', trans('orders.phonenumber'), array('class' => 'col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    <input class="form-control" type="tel" id="cobaphone" name="cobaphone" value="{{Input::old('cobaphone')}}"  />
                    <input type="hidden" id="phone" name="phone" value="" readonly />
                </div>
                <div class="col-sm-offset-2 col-sm-10">
                        {{ ($errors->has('phone') ? $errors->first('phone') : '') }}
                </div>        
        </div>
        
        <div id="usernamesForm" class="form-group {{ ($errors->has('mUsernames')) ? 'has-error' : '' }}" {{ $display_mu }}>                
                {{ Form::label('mUsernames', trans('orders.mUsernames'), array('class' => 'col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    {{ Form::textarea('mUsernames', Input::old('mUsernames'), array('class'=>'form-control', 'placeholder'=>trans('orders.mUsernames'), 'rows'=>'5')) }}
                </div>
                <div class="col-sm-offset-2 col-sm-10">
                        {{ ($errors->has('mUsernames') ? $errors->first('mUsernames') : '') }}
                </div>
        
        </div>
        
        <div id="custom_listForm" class="form-group {{ ($errors->has('custom_list')) ? 'has-error' : '' }}" {{ $display_cl }}>                
                {{ Form::label('custom_list', trans('orders.custom_list'), array('class' => 'col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    {{ Form::textarea('custom_list', Input::old('custom_list'), array('class'=>'form-control', 'placeholder'=>trans('orders.custom_list'), 'rows'=>'5')) }}
                </div>
                <div class="col-sm-offset-2 col-sm-10">
                        {{ ($errors->has('custom_list') ? $errors->first('custom_list') : '') }}
                </div>
        
        </div>
        
        <div id="custom_listFileForm" class="form-group {{ ($errors->has('custom_listFile')) ? 'has-error' : '' }}" {{ $display_clf }}>                
                {{ Form::label('custom_listFile', trans('orders.custom_listFile'), array('class' => 'col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    {{ Form::file('custom_listFile') }}
                </div>
                <div class="col-sm-offset-2 col-sm-10">
                        {{ ($errors->has('custom_listFile') ? $errors->first('custom_listFile') : '') }}
                </div>
        
        </div>
        
        <div id="hashtagsForm" class="form-group {{ ($errors->has('mHashtags')) ? 'has-error' : '' }}" {{ $display_mh }}>                
                {{ Form::label('mHashtags', trans('orders.mHashtags'), array('class' => 'col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    {{ Form::textarea('mHashtags', Input::old('mHashtags'), array('class'=>'form-control', 'placeholder'=>trans('orders.mHashtags'), 'rows'=>'5')) }}
                </div>
                <div class="col-sm-offset-2 col-sm-10">
                        {{ ($errors->has('mHashtags') ? $errors->first('mHashtags') : '') }}
                </div>
        
        </div>
        
        <div id="quantityForm" class="form-group {{ ($errors->has('quantity')) ? 'has-error' : '' }}" {{ $display_q }}>                
                {{ Form::label('quantity', trans('orders.quantity'), array('class' => 'col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    {{ Form::text('quantity', Input::old('quantity'), array('class'=>'form-control', 'placeholder'=>'Quantity')) }}
                </div>
                <div class="col-sm-offset-2 col-sm-10">
                        {{ ($errors->has('quantity') ? $errors->first('quantity') : '') }}
                </div>
        
        </div>
        
        <div id="commentsForm" class="form-group {{ ($errors->has('quantity')) ? 'has-error' : '' }}" {{ $display_c }}>
                
                {{ Form::label('comments', trans('Comments'), array('class' => 'col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    {{ Form::textarea('comments', Input::old('comments'), array('class'=>'form-control', 'placeholder'=>'Comments')) }}
                </div>
                <div class="col-sm-offset-2 col-sm-10">
                        {{ ($errors->has('comments') ? $errors->first('comments') : '') }}
                </div>
        
        </div>
        
        <div id="commentsMentionForm" class="form-group {{ ($errors->has('quantity')) ? 'has-error' : '' }}" {{ $display_cm }}>
                
                {{ Form::label('comments', trans('Comments'), array('class' => 'col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    {{ Form::textarea('mComments', Input::old('mComments'), array('class'=>'form-control', 'placeholder'=>'Comments')) }}
                    <p class="help-block">"Additional Comments (support spintax {Hello|Howdy|Hola} to you, {Mr.|Mrs.|Ms.} {Smith|Williams|Davis}!)"</p>
                </div>
                <div class="col-sm-offset-2 col-sm-10">
                        {{ ($errors->has('mComments') ? $errors->first('mComments') : '') }}
                </div>
        
        </div>
        
        <div id="totalCharge" class="form-group"></div>
        
        <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    
                        
                        {{ Form::submit(trans('pages.actionsave'), array('class' => 'btn btn-primary', 'id' => 'saveorder')) }}
                        
                </div>
        </div>

    {{ Form::close() }}
</div>
