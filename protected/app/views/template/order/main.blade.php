<div class="well">
    {{ Form::open(array('action' => ($status_on=='')?'OrderController@index':array('OrderController@show', $status_on), 'method'=>'get', 'class' => 'form-horizontal')) }}
    <div class="form-group">
    	<div class="col-sm-6">
        @foreach($status_btn as $sb)
        <a class="btn btn-default {{($sb['id']==$status_on)?'active':''}} " href="{{ action('OrderController@show', array($sb['id'])) }}">{{ $sb['name'] }} ({{ $sb['count'] }})</a>
        @endforeach
		</div>
        
        	<div class="col-sm-6">
        		<div class="row">
        		<div class="col-sm-3">
            	{{ Form::select('o', $option_search, Input::get('o'), array('class'=>'form-control')) }}
            	</div>	
            	<div class="col-sm-9">
            		<div class="row">
            		<div class="col-sm-9">
            		{{ Form::text('q', Input::has('q')?Input::get('q'):'', array('autofocus','class'=>'form-control')) }}
            		</div>
            		<div class="col-sm-3">
        			{{ Form::submit('Search', array('class' => 'btn btn-default'))}}
        			</div>
        			</div>
        		</div>
        		</div>
        	</div>
        	
        
    </div>
    {{ Form::close() }}
</div>
@if(count($orders)!=0)
<div class="row">
    <div class="col-md-12">
        <div class="">
            <table  class="table table-striped table-hover table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>{{trans('users.id')}}</th>
                        <th>{{trans('users.user')}}</th>
                        <th>{{trans('orders.link')}}</th>
                        <th>{{trans('orders.charge')}}</th>
                        <th>{{trans('orders.startcount')}}</th>
                        <th>{{trans('orders.quantity')}}</th>
                        <th>{{trans('orders.type')}}</th>
                        <th>{{trans('orders.status')}}</th>
                        <th>{{trans('orders.remains')}}</th>
                        <th>{{trans('orders.date')}}</th>
                        <th>Description</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $order)
                    <tr>
                        <td>
                        <input type="checkbox" name="orders[{{$order['id']}}]" value='{{ $order['id'] }}' />
                        </td>
                        <td>{{ $order->id }}</td>
                        <td>{{ $order->one_user?$order->one_user->username:'' }}</td>
                        <td>{{ $order->link }}</td>
                        <td>{{ $order->charge }}</td>
                        <td>{{ $order->start_count }}</td>
                        <td>{{ $order->quantity }}</td>
                        <td>{{ $order->one_service?$order->one_service->name:'' }}</td>
                        <td>{{ $status_arr[$order->status] }}</td>
                        <td>{{ $order->remains }}</td>
                        <td>{{ $order->created_at }}</td>
                        <td>{{ $order->desc }}</td>
                        <td>
                        <div class="btn-group">
                            <button type="button" data-toggle="dropdown" class="btn btn-default btn-xs dropdown-toggle">
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li class="dropdown-submenu pull-left">
                                    <a href="#">Change Status</a>
                                    <ul class="dropdown-menu">
                                        @if(isset($statuses))
                                        @foreach ($statuses as $status)
                                        <li>
                                            <a href="{{ action('OrderController@changeStatus', array('id'=> $order['id'], 'status'=> $status->id)) }}">{{$status->name}}</a>
                                        </li>
                                        @endforeach
                                        @endif
                                    </ul>
                                </li>
                                @if(Sentry::getUser() -> hasAccess('su_admin'))
                                <li>
                                    <a onclick="return confirm('Are you sure you want to Delete?')" href="{{ action('OrderController@cancelOrder', $order['id']) }}">Cancel</a>
                                </li>
                                @endif
                                <li>
                                    <a href="#" class="startCountButton" order-id="{{ $order['id'] }}">Set Start Count</a>
                                </li>
                            </ul>
                        </div></td>
                    </tr>

                    @endforeach
                </tbody>
            </table>
            {{$orders->appends(array('q' => Input::has('q')?Input::get('q'):''))->links()}}
        </div>
    </div>
</div>
<div id="test"></div>
@endif
