<div class="well">

    <div class="form-group">
        @foreach($status_btn as $sb)
        <a class="btn btn-default {{($sb['id']==$status_on)?'active':''}} " href="{{ action('OrderController@historyShow', array($sb['id'])) }}">{{ $sb['name'] }} ({{ $sb['count'] }})</a>
        @endforeach
    </div>

</div>
@if(count($orders)!=0)
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table  class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th>{{trans('users.id')}}</th>
                    <th>{{trans('users.user')}}</th>
                    <th>{{trans('orders.link')}}</th>
                    <th>{{trans('orders.charge')}}</th>
                    <th>{{trans('orders.startcount')}}</th>
                    <th>{{trans('orders.quantity')}}</th>
                    <th>{{trans('orders.type')}}</th>
                    <th>{{trans('orders.status')}}</th>
                    <th>{{trans('orders.remains')}}</th>
                    <th>{{trans('orders.date')}}</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($orders as $order)
                <tr>
                    <td>{{ $order->id }}</td>
                    <td>{{ $order->one_user?$order->one_user->username:'' }}</td>
                    <td>{{ $order->link }}</td>
                    <td>{{ $order->charge }}</td>
                    <td>{{ $order->start_count }}</td>
                    <td>{{ $order->quantity }}</td>
                    <td>{{ $order->one_service?$order->one_service->name:'' }}</td>
                    <td>{{ $status_arr[$order->status] }}</td>
                    <td>{{ $order->remains }}</td>
                    <td>{{ $order->created_at }}</td>
                    <td>{{ $order->desc }}</td>
                </tr>

                @endforeach
                </tbody>
            </table>
            {{$orders->appends(array('q' => Input::has('q')?Input::get('q'):''))->links()}}
        </div>
    </div>
</div>
@endif

