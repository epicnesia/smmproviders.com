        <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <meta name="description" content="">
                <meta name="author" content="">
                
                
                <title>
                        @section('title') 
                        @show 
                </title>
                
                <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
                
                <!-- Bootstrap core CSS -->
                {{ HTML::style('/protected/assets/css/bootstrap.min.css') }}
                {{ HTML::style('/protected/assets/css/overwrite.css') }}
                {{ HTML::style('/protected/assets/css/font-awesome.min.css') }}
                {{ HTML::style('/protected/assets/css/datatables/dataTables.bootstrap.css') }}
                {{ HTML::style('/protected/assets/css/jquery.circliful.css') }}
                {{ HTML::style('/protected/assets/css/style.css') }}
                @section('add_style')
                @show
                
                <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
                <!--[if lt IE 9]>
                        {{ HTML::script('/protected/assets/js/html5shiv.js'); }}
                        {{ HTML::script('/protected/assets/js/respond.min.js'); }}            
                <![endif]-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-97408303-3', 'auto');
  ga('send', 'pageview');

</script>
        </head>