<div class="row">
	<div id="form-wrapper" class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-12">
		{{ Form::open(array('action' => 'SessionController@store', 'class' => 'form-horizontal col-lg-12')) }}
		<h3>Login</h3>
		<div class="form-group {{ ($errors->has('email')) ? 'has-error' : '' }}">

			{{ Form::text('email', $email_old, array('class' => 'form-control', 'placeholder' => trans('users.username') . ' or ' . trans('users.email'), 'autofocus')) }}
			{{ ($errors->has('email') ? $errors->first('email') : '') }}

		</div>
		<div class="form-group">

			<input type="password" name="password" value="{{$password_old}}" class="form-control" placeholder="{{trans('users.pword')}}" />
			{{ ($errors->has('password') ?  $errors->first('password') : '') }}

		</div>
		<div class="form-group">
			<div class="col-lg-6">
				{{ Form::submit(trans('pages.login'), array('class' => 'btn btn-primary'))}}
				{{-- <a class="btn btn-link" href="{{ route('forgotPasswordForm') }}">{{trans('users.forgot')}}?</a> --}}
			</div>
			<div class="col-lg-6">
				<label class="checkbox" style="margin-left:20px"> {{ Form::checkbox('rememberMe', 1, $rememberMe_old) }} {{trans('users.remember')}}? </label>
			</div>
		</div>
		{{ Form::close() }}
	</div>
</div>
