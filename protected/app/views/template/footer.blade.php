                 <!-- Bootstrap core JavaScript
                ================================================== -->
                <!-- Placed at the end of the document so the pages load faster -->
                {{ HTML::script('/protected/assets/js/jquery-1.11.2.min.js'); }}
                {{ HTML::script('/protected/assets/js/bootstrap.min.js'); }}
                {{ HTML::script('/protected/assets/js/jquery.easing.1.3.js'); }}
                {{ HTML::script('/protected/assets/js/restfulizer.min.js'); }}
                {{ HTML::script('/protected/assets/js/jquery.popconfirm.min.js'); }}
                {{ HTML::script('/protected/assets/js/zeroClipboard/ZeroClipboard.min.js'); }}
                {{ HTML::script('/protected/assets/js/datatables/jquery.dataTables.js'); }}
		        {{ HTML::script('/protected/assets/js/datatables/dataTables.bootstrap.js'); }}
		        {{ HTML::script('/protected/assets/js/jquery.circliful.min.js'); }}
		        {{ HTML::script('/protected/assets/js/custom.js'); }}
		@section('add_script')
                @show
<script type='text/javascript'>
	
	$(".circli").circliful();
                		
	$('#tbl_example2').dataTable({ "iDisplayLength": 100});
	$('#delete_button').click(function(e){
		return confirm('{{trans('pages.sureto')}} {{trans('pages.actiondelete')}}?');
	});
                    
	$('#delete_button2').popConfirm({
		title: "{{trans('pages.actiondelete')}}",
		content: "{{trans('pages.sureto')}} {{trans('pages.actiondelete')}}?",
		placement: "bottom",
		classBtn: "btn-danger"
	});
                        
	$('#accept_button').popConfirm({
		title: "{{trans('orders.accept')}}",
		content: "{{trans('pages.sureto')}} {{trans('orders.accept')}}?",
		placement: "bottom"
	});
                        
	$('.startCountButton').click(function(e){
		var startCount = prompt("Set Start Count", '0');
		var orderId = $(this).attr('order-id');
                           
		if(startCount != null){
			$.ajax({
				type: "POST",
        		url: "<?php echo URL::to('order/setStartCount'); ?>",
 				data: { id: orderId, startCount: startCount, submit: "submit" },
              	async:false
			}).done(
				window.location.replace("<?php echo URL::to('order'); ?>")
            )}
	});
                        
	$('#selectService').change(function(e){
		var select = $(this).val();
                                
		$.ajax({
			type: "POST",
			url: "<?php echo URL::to('order/checkServiceType'); ?>",
			data: { id: select, submit: "submit" }
		}).done(function(r){
                                        
			if(r == 1){
				$('#phoneForm').css('display', 'none');
				$('#quantityForm').css('display', 'none');
				$('#commentsForm').css('display', 'block');
				$('#usernamesForm').css('display', 'none');
				$('#hashtagsForm').css('display', 'none');
				$('#custom_listForm').css('display', 'none');
				$('#custom_listFileForm').css('display', 'none');
				$('#commentsMentionForm').css('display', 'none');
				$('#totalCharge').css('display', 'block');
				$('label[for=link]').html('Link');
				$('#link').attr('placeholder', 'Link');
			} else if (r == 3){
				$('#phoneForm').css('display', 'none');
				$('#quantityForm').css('display', 'block');
				$('#commentsForm').css('display', 'none');
				$('#usernamesForm').css('display', 'block');
				$('#hashtagsForm').css('display', 'block');
				$('#custom_listForm').css('display', 'none');
				$('#custom_listFileForm').css('display', 'none');
				$('#commentsMentionForm').css('display', 'block');
				$('#totalCharge').css('display', 'block');
				$('label[for=link]').html('Link');
				$('#link').attr('placeholder', 'Link');
			} else if (r == 4){
				$('#phoneForm').css('display', 'none');
				$('#quantityForm').css('display', 'none');
				$('#commentsForm').css('display', 'none');
				$('#usernamesForm').css('display', 'none');
				$('#hashtagsForm').css('display', 'none');
				$('#custom_listForm').css('display', 'block');
				$('#custom_listFileForm').css('display', 'block');
				$('#commentsMentionForm').css('display', 'block');
				$('#totalCharge').css('display', 'none');
				$('label[for=link]').html('Link');
				$('#link').attr('placeholder', 'Link');
			} else if (r == 5){
				$('#phoneForm').css('display', 'none');
				$('#quantityForm').css('display', 'block');
				$('#commentsForm').css('display', 'none');
				$('#usernamesForm').css('display', 'block');
				$('#hashtagsForm').css('display', 'block');
				$('#custom_listForm').css('display', 'none');
				$('#custom_listFileForm').css('display', 'none');
				$('#commentsMentionForm').css('display', 'none');
				$('#totalCharge').css('display', 'block');
				$('label[for=link]').html('Client Username @');
				$('#link').attr('placeholder', 'Username');
			}else{
				if(r == 2){$('#phoneForm').css('display', 'block');}
				$('#quantityForm').css('display', 'block');
				$('#commentsForm').css('display', 'none');
				$('#usernamesForm').css('display', 'none');
				$('#hashtagsForm').css('display', 'none');
				$('#custom_listForm').css('display', 'none');
				$('#custom_listFileForm').css('display', 'none');
				$('#commentsMentionForm').css('display', 'none');
				$('#totalCharge').css('display', 'block');
				$('label[for=link]').html('Link');
				$('#link').attr('placeholder', 'Link');
			}
			
		})
		e.preventDefault();
	});
	
	$('#selectService').change(function(e){
		var id  = $('#selectService').val();
			$.ajax({
				type: "POST",
				url: "<?php echo URL::to('order/getPrice'); ?>",
				data: { id: id, submit: "submit" }
			}).done(function(r){
				$('#pricePer1000').html('<label class="col-sm-2 control-label">Price Per 1000</label>'
                                  		+ '<div class="col-sm-10"><input class="form-control" placeholder="Price Per 1000" name="price_per1000" type="text" id="price_per1000" value="' + r + '" readonly="readonly"></div>');
            })
            
            e.preventDefault();
            
 	});
                        
	$('input[name=quantity], textarea[name=comments]').keyup(function(e){
                                
		var id  = $('#selectService').val();
		var type= $(this).attr('name');
		var qty = $(this).val();
		
		$.ajax({
			type: "POST",
			url: "<?php echo URL::to('order/getCharge'); ?>",
			data: { id: id, type: type, qty: qty, submit: "submit" }
		}).done(function(r){
			$('#totalCharge').html('<label class="col-sm-2 control-label">Charge</label>'
									+ '<div class="col-sm-10"><input class="form-control" placeholder="Estimated Charge" name="est_charge" type="text" id="est_sharge" value="' + r + '" readonly="readonly"></div>');
		})
		
		e.preventDefault();
		
	});
	
	$('#selectPlan').change(function(e){
		if(!$('#daysPriceWrap').hasClass('hidden')){
			$('#daysPriceWrap').addClass('hidden');
		}
		var id  = $('#selectPlan').val();
			$.ajax({
				type: "POST",
				url: "<?php echo URL::to('subscription/get_plan_price'); ?>",
				data: { id: id, submit: "submit" }
			}).done(function(r){
				if($('#daysPriceWrap').hasClass('hidden')){
					$('#daysPriceWrap').removeClass('hidden');
				}
				
				$('#daysPriceSelect').html(r);
            })
            
            e.preventDefault();
            
 	});
	
	// ==== Zero Clipboard Setting ===
	ZeroClipboard.config( { swfPath: "<?php echo URL::to('/protected/assets/js/zeroClipboard/ZeroClipboard.swf'); ?>" } );
    var client = new ZeroClipboard( $("button#copy_key") );
                        
    //================================
    
    var input_phone = $("#cobaphone");
    input_phone.intlTelInput({
    	nationalMode: true,
    	preferredCountries: ["kw", "ae", "sa", "om", "bh", "qa"],
    	utilsScript: "<?php echo URL::to('/protected/assets/lib/libphonenumber/build/utils.js'); ?>"
    });
    
    $('#saveorder').click(function(e){
    	var intlNumber = input_phone.intlTelInput("getNumber");
    	if (intlNumber) {
    		$("#phone").val(intlNumber);
    	}
    	
    	return true;
    });

</script>