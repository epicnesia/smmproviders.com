<br />
<p>
    Send POST query to {{Config::get('app.api_url') . '/neworder'}}
</p>

<h4>Method Order </h4>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne1">
            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne1"> Middle East Instagram Likes</a></h4>
        </div>
        <div id="collapseOne1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne1">
            <div class="panel-body">
                <ul class="list-group">
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-md-12">

                                <table  class="table table-striped table-hover table-bticketed">
                                    <thead>
                                        <tr>
                                            <th>Parameters</th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>api_key</td>
                                            <td>Your API key</td>
                                        </tr>
                                        <tr>
                                            <td>link</td>
                                            <td>Link to page</td>
                                        </tr>
                                        <tr>
                                            <td>quantity</td>
                                            <td>Needed quantity</td>
                                        </tr>
                                        <tr>
                                            <td>services</td>
                                            <td>11</td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingThree2">
            <h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#accordion"
            href="#collapseThree2" aria-expanded="false" aria-controls="collapseThree2">  Middle East Instagram Followers </a></h4>
        </div>
        <div id="collapseThree2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree2">        
            <div class="panel-body">
                <ul class="list-group">
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-md-12">

                                <table  class="table table-striped table-hover table-bticketed">
                                    <thead>
                                        <tr>
                                            <th>Parameters</th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>api_key</td>
                                            <td>Your API key</td>
                                        </tr>
                                        <tr>
                                            <td>link</td>
                                            <td>Link to page</td>
                                        </tr>
                                        <tr>
                                            <td>quantity</td>
                                            <td>Needed quantity</td>
                                        </tr>
                                        <tr>
                                            <td>services</td>
                                            <td>2</td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
   
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwo1">
            <h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#accordion"
            href="#collapseTwo1" aria-expanded="false" aria-controls="collapseTwo1">  Middle East Instagram Comments</a></h4>
        </div>
        <div id="collapseTwo1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo1">
            <div class="panel-body">
                <ul class="list-group">
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-md-12">

                                <table  class="table table-striped table-hover table-bticketed">
                                    <thead>
                                        <tr>
                                            <th>Parameters</th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>api_key</td>
                                            <td>Your API key</td>
                                        </tr>
                                        <tr>
                                            <td>link</td>
                                            <td>Link to page</td>
                                        </tr>
                                        <tr>
                                            <td>comments</td>
                                            <td>Comments</td>
                                        </tr>
                                        <tr>
                                            <td>services</td>
                                            <td>3</td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </li>
                </ul>

            </div>
        </div>
    </div>
    
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingThree3">
            <h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#accordion"
            href="#collapseThree3" aria-expanded="false" aria-controls="collapseThree3">  Instagram High Quality Likes </a></h4>
        </div>
        <div id="collapseThree3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree3">    
            
            <div class="panel-body">
                <ul class="list-group">
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-md-12">

                                <table  class="table table-striped table-hover table-bticketed">
                                    <thead>
                                        <tr>
                                            <th>Parameters</th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>api_key</td>
                                            <td>Your API key</td>
                                        </tr>
                                        <tr>
                                            <td>link</td>
                                            <td>Link to page</td>
                                        </tr>
                                        <tr>
                                            <td>quantity</td>
                                            <td>Needed quantity</td>
                                        </tr>
                                        <tr>
                                            <td>services</td>
                                            <td>4</td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwo2">
            <h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#accordion"
            href="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2">  Instagram High Quality Comments </a></h4>
        </div>
        <div id="collapseTwo2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo2">
            <div class="panel-body">
                <ul class="list-group">
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-md-12">

                                <table  class="table table-striped table-hover table-bticketed">
                                    <thead>
                                        <tr>
                                            <th>Parameters</th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>api_key</td>
                                            <td>Your API key</td>
                                        </tr>
                                        <tr>
                                            <td>link</td>
                                            <td>Link to page</td>
                                        </tr>
                                        <tr>
                                            <td>comments</td>
                                            <td>Comments</td>
                                        </tr>
                                        <tr>
                                            <td>services</td>
                                            <td>5</td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </li>
                </ul>

            </div>
        </div>
    </div>
     <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne4">
            <h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne4" aria-expanded="false" aria-controls="collapseOne4"> Instagram High Quality Followers</a></h4>
        </div>
        <div id="collapseOne4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne4">
            <div class="panel-body">
                <ul class="list-group">
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-md-12">

                                <table  class="table table-striped table-hover table-bticketed">
                                    <thead>
                                        <tr>
                                            <th>Parameters</th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>api_key</td>
                                            <td>Your API key</td>
                                        </tr>
                                        <tr>
                                            <td>link</td>
                                            <td>Link to page</td>
                                        </tr>
                                        <tr>
                                            <td>quantity</td>
                                            <td>Needed quantity</td>
                                        </tr>
                                        <tr>
                                            <td>services</td>
                                            <td>6</td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingThree">
            <h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#accordion"
            href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">  Instagram Mentions </a></h4>
        </div>
        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
                <ul class="list-group">
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-md-12">

                                <table  class="table table-striped table-hover table-bticketed">
                                    <thead>
                                        <tr>
                                            <th>Parameters</th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>api_key</td>
                                            <td>Your API key</td>
                                        </tr>
                                        <tr>
                                            <td>link</td>
                                            <td>Link to page</td>
                                        </tr>
                                        <tr>
                                            <td>quantity</td>
                                            <td>Needed quantity</td>
                                        </tr>
                                        <tr>
                                            <td>usernames</td>
                                            <td>Usernames to scrape followers (optional)</td>
                                        </tr>
                                        <tr>
                                            <td>hashtags</td>
                                            <td>Hashtags to scrape uploader and followers (Required Minimal 2 Hashtags)</td>
                                        </tr>
                                        <tr>
                                            <td>comments</td>
                                            <td>Additional Comments (support spintax {Hello|Howdy|Hola} to you, {Mr.|Mrs.|Ms.} {Smith|Williams|Davis}!)</td>
                                        </tr>
                                        <tr>
                                            <td>services</td>
                                            <td>7</td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingThree4">
            <h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#accordion"
            href="#collapseThree4" aria-expanded="false" aria-controls="collapseThree4">  Instagram Mentions Custom List </a></h4>
        </div>
        <div id="collapseThree4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree4">
            <div class="panel-body">
                <ul class="list-group">
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-md-12">

                                <table  class="table table-striped table-hover table-bticketed">
                                    <thead>
                                        <tr>
                                            <th>Parameters</th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>api_key</td>
                                            <td>Your API key</td>
                                        </tr>
                                        <tr>
                                            <td>link</td>
                                            <td>Link to page</td>
                                        </tr>
                                        <tr>
                                            <td>custom_list</td>
                                            <td>Instagram Username Lists</td>
                                        </tr>
                                        <tr>
                                            <td>comments</td>
                                            <td>Additional Comments (support spintax {Hello|Howdy|Hola} to you, {Mr.|Mrs.|Ms.} {Smith|Williams|Davis}!)</td>
                                        </tr>
                                        <tr>
                                            <td>services</td>
                                            <td>8</td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingThree3">
            <h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#accordion"
            href="#collapseThree5" aria-expanded="false" aria-controls="collapseThree5">  Instagram Views </a></h4>
        </div>
        <div id="collapseThree5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree3">    
            
            <div class="panel-body">
                <ul class="list-group">
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-md-12">

                                <table  class="table table-striped table-hover table-bticketed">
                                    <thead>
                                        <tr>
                                            <th>Parameters</th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>api_key</td>
                                            <td>Your API key</td>
                                        </tr>
                                        <tr>
                                            <td>link</td>
                                            <td>Link to page</td>
                                        </tr>
                                        <tr>
                                            <td>quantity</td>
                                            <td>Views to get</td>
                                        </tr>
                                        <tr>
                                            <td>services</td>
                                            <td>9</td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingThree6">
            <h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#accordion"
            href="#collapseThree6" aria-expanded="false" aria-controls="collapseThree6">  Instagram User Mentions </a></h4>
        </div>
        <div id="collapseThree6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree6">    
            
            <div class="panel-body">
                <ul class="list-group">
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-md-12">

                                <table  class="table table-striped table-hover table-bticketed">
                                    <thead>
                                        <tr>
                                            <th>Parameters</th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>api_key</td>
                                            <td>Your API key</td>
                                        </tr>
                                        <tr>
                                            <td>instagram_username</td>
                                            <td>Client username without @</td>
                                        </tr>
                                        <tr>
                                            <td>quantity</td>
                                            <td>Needed quantity</td>
                                        </tr>
                                        <tr>
                                            <td>usernames</td>
                                            <td>Usernames to scrape followers (optional)</td>
                                        </tr>
                                        <tr>
                                            <td>hashtags</td>
                                            <td>Hashtags to scrape uploader and followers (Required Minimal 2 Hashtags)</td>
                                        </tr>
                                        <tr>
                                            <td>services</td>
                                            <td>10</td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<ul class="list-group">
    <li class="list-group-item">
        <div class="row">
            <div class="col-md-12">

                <table  class="table table-striped table-hover table-bticketed">
                    <thead>
                        <tr>
                            <th>Response</th>
                            <th>Value</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>$result</td>
                            <td>
                            <ul class="list-unstyled">

                                <li>
                                    error
                                </li>
                                <li>
                                    success
                                </li>
                            </ul></td>
                        </tr>
                        <tr>
                            <td>$id</td>
                            <td>Order Id</td>
                        </tr>
                        <tr>
                            <td>$message</td>
                            <td>
                            <ul class="list-unstyled">

                                <li>
                                    New Order cannot be saved
                                </li>
                                <li>
                                    Please Insert Comments
                                </li>
                                <li>
                                    All input form must be required!!!
                                </li>
                                <li>
                                    User not found
                                </li>
                                <li>
                                    Sorry, Admin Balance Not Enough
                                </li>
                                <li>
                                    Sorry, the order with same service and instagram username  already exist and still active on isocialpanel
                                </li>
                                <li>
                                    Order success
                                </li>
                            </ul></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <p>
            Response is in json format
        </p>
    </li>
</ul>
