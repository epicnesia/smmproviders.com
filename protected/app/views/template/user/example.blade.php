<div class="well">
    <div class="row">
        <pre>
            /**
             * This example illustrates the isocialpanel reseller api to send the request to,
             * and place order and get the api key status
             *
             * @copyright iSocialPanel.com 
             *
             */
            
            class IsocialpanelReseller {
                // API URL
                public $api_url = '{{Config::get('app.api_url')}}';
                // Your API key
                public $api_key = 'example_api_key_string';
            
                /*
                 * Place an order
                 */
            
                public function order($link, $instagram_username, $quantity, $phone, $comments, $usernames, $hashtags, $custom_list, $services) {// Add order
                    $url = $this -> api_url . '/neworder';
                    $is_post = 1;
                    return json_decode($this -> connect($url, $is_post, array(
                        'api_key' => $this -> api_key, 
                		'link' => $link, 
                		'instagram_username' => $instagram_username, 
                		'quantity' => $quantity,
                		'phone' => $phone, 
                		'comments' => $comments, 
                		'usernames'=>$usernames, 
                		'hashtags'=>$hashtags, 
                		'custom_list'=>$custom_list, 
                		'services' => $services
                    )));
                }
            
               
                
                /*
                 * Get Order Status
                 */
            
                public function order_status($id) {
                    $url = $this -> api_url . '/orderstatus';
                    $is_post = 0;
                    return json_decode($this -> connect($url, $is_post, array('api_key' => $this -> api_key, 'id' => $id, )));
                }
            
                /*
                 * Send curl request
                 */
            
                private function connect($url, $is_post, $data) {
                    $paramsq = http_build_query($data, '', '&');
            
                    $ch1 = curl_init();
            
                    curl_setopt($ch1, CURLOPT_POST, $is_post);
                    curl_setopt($ch1, CURLOPT_HEADER, 0);
                    curl_setopt($ch1, CURLOPT_URL, $url . "?" . $paramsq);
                    curl_setopt($ch1, CURLOPT_FRESH_CONNECT, 1);
                    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
            
                    if (!$result = curl_exec($ch1)) {
                        $result = false;
                    }
            
                    curl_close($ch1);
            
                    return $result;
                }
        }
        
        $reseller = new IsocialpanelReseller();
        
        //place an order
        $response = $reseller -> order($link, $instagram_username, $quantity, $phone, $comments, $usernames, $hashtags, $custom_list, $services);
        echo ($response) ? 'result:' . $response -> result .'; id:' . $response ->id . '; message:' . $response -> message : 'none';
        
        //get order status
        $response = $reseller -> order_status($id);
        echo ($response) ? 'result:' . $response -> result . '; status:' . $response -> status . '; start count:' . $response -> start . '; message:' . $response -> message : 'none';

        </pre>
    </div>
</div>