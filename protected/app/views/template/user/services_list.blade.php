<h3>{{trans('pages.listof')}} {{trans('services.services')}}</h3>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		
		<div class="table-responsive">
			<table class="table table-striped table-hover table-bordered">
				<thead>
					<tr>
						<th colspan="4">{{trans('services.Regular Service')}}</th>
					</tr>
					<tr>
						<th>id</th>
						<th>{{trans('services.serviceName')}}</th>
						<th>{{trans('services.serviceMaxAmount')}}</th>
						<th>{{trans('services.serviceSubtypesPrice/1000')}}</th>
					</tr>
				</thead>
				<tbody>
					@if(isset($services))

					@foreach($services as $service)
					<tr>
						<td>{{$service->id}}</td>
						<td>{{$service->name}}</td>
						<td>{{$service->max_amount}}</td>
						<td>{{$service->price}}</td>
					</tr>
					@endforeach

					@endif
				</tbody>
			</table>
		</div>
		
		<div class="table-responsive">
			<table class="table table-striped table-hover table-bordered">
				<thead>
					<tr>
						<th colspan="7">{{trans('services.Auto Likes Plans')}}</th>
					</tr>
					<tr>
						<th>id</th>
						<th>{{trans('services.serviceName')}}</th>
						<th>{{trans('services.Count')}}</th>
						<th>{{trans('services.price_7_days')}}</th>
						<th>{{trans('services.price_30_days')}}</th>
						<th>{{trans('services.price_60_days')}}</th>
						<th>{{trans('services.price_90_days')}}</th>
					</tr>
				</thead>
				<tbody>
					@if(isset($autoLikes))

					@foreach($autoLikes as $autoLikesPlan)
					<tr>
						<td>{{$autoLikesPlan->id}}</td>
						<td>{{$autoLikesPlan->name}}</td>
						<td>{{$autoLikesPlan->count}}</td>
						<td>{{$autoLikesPrice[$autoLikesPlan->id]['price_7_days']}}</td>
						<td>{{$autoLikesPrice[$autoLikesPlan->id]['price_30_days']}}</td>
						<td>{{$autoLikesPrice[$autoLikesPlan->id]['price_60_days']}}</td>
						<td>{{$autoLikesPrice[$autoLikesPlan->id]['price_90_days']}}</td>
					</tr>
					@endforeach

					@endif
				</tbody>
			</table>
		</div>
		
		<div class="table-responsive">
			<table class="table table-striped table-hover table-bordered">
				<thead>
					<tr>
						<th colspan="7">{{trans('services.Auto Views Plans')}}</th>
					</tr>
					<tr>
						<th>id</th>
						<th>{{trans('services.serviceName')}}</th>
						<th>{{trans('services.Count')}}</th>
						<th>{{trans('services.price_7_days')}}</th>
						<th>{{trans('services.price_30_days')}}</th>
						<th>{{trans('services.price_60_days')}}</th>
						<th>{{trans('services.price_90_days')}}</th>
					</tr>
				</thead>
				<tbody>
					@if(isset($autoViews))

					@foreach($autoViews as $autoViewsPlan)
					<tr>
						<td>{{$autoViewsPlan->id}}</td>
						<td>{{$autoViewsPlan->name}}</td>
						<td>{{$autoViewsPlan->count}}</td>
						<td>{{$autoViewsPrice[$autoViewsPlan->id]['price_7_days']}}</td>
						<td>{{$autoViewsPrice[$autoViewsPlan->id]['price_30_days']}}</td>
						<td>{{$autoViewsPrice[$autoViewsPlan->id]['price_60_days']}}</td>
						<td>{{$autoViewsPrice[$autoViewsPlan->id]['price_90_days']}}</td>
					</tr>
					@endforeach

					@endif
				</tbody>
			</table>
		</div>
		
	</div>
</div>
