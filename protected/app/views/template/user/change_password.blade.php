    <h4>{{trans('pages.changePassword')}}</h4>
    <div class="well">
        {{ Form::open(array(
        	'action' => array('UserController@ubah', $user->id),
          	'class' => 'form-horizontal',
           	'role' => 'form'
        )) }}

        <div class="form-group {{ $errors->has('oldPassword') ? 'has-error' : '' }}">
            {{ Form::label('oldPassword', trans('users.oldPassword'), array('class' => 'sr-only')) }}
            {{ Form::password('oldPassword', array('class' => 'form-control', 'placeholder' => trans('users.oldpassword_lbl'))) }}
        </div>
        
        <div class="form-group {{ $errors->has('newPassword') ? 'has-error' : '' }}">
            {{ Form::label('newPassword', trans('users.newpassword_lbl'), array('class' => 'sr-only')) }}
            {{ Form::password('newPassword', array('class' => 'form-control', 'placeholder' => trans('users.newpassword_lbl'))) }}
        </div>
        
        <div class="form-group {{ $errors->has('newPassword_confirmation') ? 'has-error' : '' }}">
            {{ Form::label('newPassword_confirmation', trans('users.newPassword_confirmation'), array('class' => 'sr-only')) }}
            {{ Form::password('newPassword_confirmation', array('class' => 'form-control', 'placeholder' => trans('users.newcompassword_lbl'))) }}
        </div>

        {{ Form::submit(trans('pages.actionsave'), array('class' => 'btn btn-primary'))}}
        {{ Form::close() }}
    </div>

