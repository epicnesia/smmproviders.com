<div class="well">
        <div class="container-fluid">
                <div align="center">
                        <h4>{{ trans('api.yourApiKey') }}</h4>

                        <div class="guide" align="center">

                                {{ Form::open(array('action' => 'ApiKeyGeneratorsController@generateKey', 'class' => '', 'role' => 'form')) }}

                                <div class="form-group {{ ($errors->has('api_key')) ? 'has-error' : '' }}">
                                        <div class="col-sm-10">
                                                {{ Form::text('api_key', $api_key_string, array('id' => 'api_key', 'class' => 'form-control', 'readOnly')) }}
                                        </div>
                                        {{ Form::submit(trans('api.generate_key'), array('class' => 'btn btn-primary')) }}
                                        {{ Form::button(trans('api.copy_key'), array('class' => 'btn btn-info copy_key', 'id'=>'copy_key', 'data-clipboard-target'=>'api_key')) }}
                                        <div class="col-sm-10">
                                                <font color="#ff0000">{{ ($errors->has('username') ? $errors->first('username') : '') }}</font>
                                        </div>
                                </div>

                                {{Form::close()}}
                        </div>
                </div>
        </div>
</div>