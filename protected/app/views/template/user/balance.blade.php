<div class="well">
    <div class="container-fluid">
        <div align="center">
            <h4>Balance: ${{ round($balance,2)}}</h4>

            <div class="guide" align="center">
                
                {{ Form::open(array('action' => 'PaymentController@buyBalance', 'class' => '', 'role' => 'form')) }}

                    <label class="payment">PayPal</label>
                    <div class="clr"></div>
                    <div class="payment-form p1" style="display:block;">
                        <div class="border b1"></div>
                        <h4>Amount:</h4>
                        <div class="control-group">
                            <div class="controls">
                                <input name="amount" type="text" >
                            </div>
                        </div>                        
                        <input value="Pay" class="btn btn-default" name="paypal"  type="submit" style="margin-top:10px">
                        <div class="clr"></div>
                    </div>

                {{Form::close()}}
            </div>
        </div>
    </div>
</div>