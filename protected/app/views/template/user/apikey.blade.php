<div class="container-fluid">
    <div align="center">

        <div style="max-width: 500px" class="text-left">
            <h4>Your API Key</h4>
            {{ Form::open(array('action' => 'ApiKeyGeneratorsController@generateKey', 'class' => '', 'role' => 'form')) }}
            <div class="form-group {{ ($errors->has('api_key')) ? 'has-error' : '' }}">

                {{ Form::text('api_key', $user_key, array('id' => 'api_key', 'class' => 'form-control')) }}
                <br />
                {{ Form::button(trans('api.copy_key'), array('class' => 'btn btn-info btn-block copy_key', 'id'=>'copy_key', 'data-clipboard-target'=>'api_key')) }}
                <br />

                {{ Form::submit(trans('api.generate_key'), array('class' => 'btn btn-primary btn-block ')) }}

                <font color="#ff0000">{{ ($errors->has('username') ? $errors->first('username') : '') }}</font>

            </div>

            {{Form::close()}}

        </div>
    </div>
    <br />
    <h3>API Help</h3>
    @include('template.user.order')
    @include('template.user.orderstatus')
    
    <br />
    <a href="{{URL::to('/api/example')}}">See Example</a>
</div>