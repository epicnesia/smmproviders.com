<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="col-lg-3 col-md-6">
			<div class="col-lg-12 cardview">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cyclewrap">
					<div class="circli" data-animationStep="5" data-percent="{{ $totalOrder > 0 ? ($totalOrder/$totalOrder)*100 : 0 }}"></div>
				</div>
				<div class="col-lg-8">
					<h2>{{$totalOrder}}</h2>
					<p>{{trans('orders.Total Orders')}}</p>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="col-lg-12 cardview">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cyclewrap">
					<div class="circli" data-animationStep="5" data-percent="{{ $totalOrder > 0 ? ($totalCompleted/$totalOrder)*100 : 0 }}" data-foregroundColor="#44af4e"></div>
				</div>
				<div class="col-lg-8">
					<h2>{{$totalCompleted}}</h2>
					<p>{{trans('orders.Orders Completed')}}</p>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="col-lg-12 cardview">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cyclewrap">
					<div class="circli" data-animationStep="5" data-percent="{{ $totalOrder > 0 ? ($totalProcessing/$totalOrder)*100 : 0 }}" data-foregroundColor="#449baf"></div>
				</div>
				<div class="col-lg-8">
					<h2>{{$totalProcessing}}</h2>
					<p>{{trans('orders.Orders Processing')}}</p>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="col-lg-12 cardview">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cyclewrap">
					<div class="circli" data-animationStep="5" data-percent="{{ $totalOrder > 0 ? ($totalPending/$totalOrder)*100 : 0 }}" data-foregroundColor="#af44aa"></div>
				</div>
				<div class="col-lg-8">
					<h2>{{$totalPending}}</h2>
					<p>{{trans('orders.Orders Pending')}}</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="clearfix">&nbsp;</div>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="col-lg-12">
			<div class="col-lg-12 cardview">
				<div class="col-lg-12">
					<h3>{{trans('users.Account Overview')}}</h3>
					<div class="table-responsive">
						<table class="table table-hover">
							<tbody>
								<tr>
									<td>{{trans('users.username')}}</td><td>{{$user->username}}</td>
								</tr>
								<tr>
									<td>{{trans('users.email')}}</td><td>{{$user->email}}</td>
								</tr>
								<tr>
									<td>{{trans('users.skype_username')}}</td><td>{{$user->skype_username}}</td>
								</tr>
								<tr>
									<td>{{trans('users.balance')}}</td><td>{{$user->balance}}</td>
								</tr>
								<tr>
									<td>{{trans('users.spent')}}</td><td>{{$user->spent}}</td>
								</tr>
								<tr>
									<td>{{trans('orders.Orders Completed')}}</td><td>{{$totalCompleted}}</td>
								</tr>
								<tr>
									<td>{{trans('orders.Total Orders')}}</td><td>{{$totalOrder}}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
