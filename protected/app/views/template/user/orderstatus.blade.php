<br />
<p>
    Send GET query to {{Config::get('app.api_url') . '/orderstatus' }}
</p>
<h4>Method Order Status</h4>
<ul class="list-group">
    <li class="list-group-item">
        <div class="row">
            <div class="col-md-12">

                <table  class="table table-striped table-hover table-bticketed">
                    <thead>
                        <tr>
                            <th>Parameters</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>api_key</td>
                            <td>Your API key</td>
                        </tr>
                        <tr>
                            <td>id</td>
                            <td>Order Id</td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </li>
</ul>

<ul class="list-group">
    <li class="list-group-item">
        <div class="row">
            <div class="col-md-12">

                <table  class="table table-striped table-hover table-bticketed">
                    <thead>
                        <tr>
                            <th>Response</th>
                            <th>Value</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>$result</td>
                            <td>
                            <ul class="list-unstyled">

                                <li>
                                    error
                                </li>
                                <li>
                                    success
                                </li>
                            </ul></td>
                        </tr>
                        <tr>
                            <td>status</td>
                            <td><ul class="list-unstyled">

                                <li>
                                    0 - Pending
                                </li>
                                <li>
                                    1 - Processing
                                </li>
                                <li>
                                    2 - Completed
                                </li>
                                <li>
                                    3 - Refunded
                                </li>
                                <li>
                                    8 - Failed
                                </li>                                
                            </ul></td>
                        </tr>
                        <tr>
                            <td>start</td>
                            <td>Start Count</td>
                        </tr>
                        <tr>
                            <td>remains</td>
                            <td>Remains</td>
                        </tr>
                        <tr>
                            <td>charge</td>
                            <td>Charge</td>
                        </tr>                        
                        <tr>
                            <td>$message</td>
                            <td>
                            <ul class="list-unstyled">

                                <li>
                                    Order Not Found
                                </li>                                
                                <li>
                                    Order with id -Order Id- Not Found
                                </li>
                                <li>
                                    Order with id -Order Id- is not your order
                                </li>
                                <li>
                                    Order Found
                                </li>                                
                            </ul></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <p>
            Response is in json format
        </p>
    </li>
</ul>
