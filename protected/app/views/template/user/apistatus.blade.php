<br />
<p>
    Send GET query to {{Config::get('app.api_url') }}
</p>
<h4>Method API key Status</h4>
<ul class="list-group">
    <li class="list-group-item">
        <div class="row">
            <div class="col-md-12">

                <table  class="table table-striped table-hover table-bticketed">
                    <thead>
                        <tr>
                            <th>Parameters</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>key</td>
                            <td>Your API key</td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </li>
</ul>

<ul class="list-group">
    <li class="list-group-item">
        <div class="row">
            <div class="col-md-12">

                <table  class="table table-striped table-hover table-bticketed">
                    <thead>
                        <tr>
                            <th>Response</th>
                            <th>Value</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>$result</td>
                            <td>
                            <ul class="list-unstyled">

                                <li>
                                    error
                                </li>
                                <li>
                                    success
                                </li>
                            </ul></td>
                        </tr>
                        <tr>
                            <td>$id</td>
                            <td>User Id</td>
                        </tr>
                        <tr>
                            <td>$message</td>
                            <td>
                            <ul class="list-unstyled">

                                <li>
                                    Invalid user api key
                                </li>
                                <li>
                                    User api not found
                                </li>
                                <li>
                                    User api is not active
                                </li>
                                <li>
                                    User api not found
                                </li>
                                <li>
                                    api key is valid
                                </li>                                
                            </ul></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <p>
            Response is in json format
        </p>
    </li>
</ul>
