<div class="well container-fluid">
	<div class="row">
		@include('template.subscription.buttons')
		
		<div class="col-lg-4">
			
		</div>
	</div>
</div>

<h3>{{trans('pages.create')}} {{trans('pages.order') }}</h3>
    
<div class="well">
    {{ Form::open(array('action' => 'SubscriptionController@store', 'class' => 'form-horizontal', 'role' => 'form', 'files'=> true)) }}

        <div class="form-group {{ ($errors->has('services')) ? 'has-error' : '' }}">
            
                {{ Form::label('services', trans('orders.service'), array('class' => 'col-sm-2 control-label')) }}
                <div class="col-sm-10" style="padding-top: 7px;">
                	{{ Form::hidden('services', $service->id, ['id'=>'selectService']) }}
                    {{ $service->name }}
                </div>
                <div class="col-sm-offset-2 col-sm-10">
                        {{ ($errors->has('services') ? $errors->first('services') : '') }}
                </div>
        
        </div>

        <div class="form-group {{ ($errors->has('plan')) ? 'has-error' : '' }}">
            
                {{ Form::label('plan', trans('orders.plan'), array('class' => 'col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    {{ Form::select('plan', $plans, null, array('class'=>'form-control', 'id'=>'selectPlan')) }}
                </div>
                <div class="col-sm-offset-2 col-sm-10">
                        {{ ($errors->has('plan') ? $errors->first('plan') : '') }}
                </div>
        
        </div>

        <div id="daysPriceWrap" class="form-group {{ ($errors->has('daysPrice')) ? 'has-error' : '' }} {{ Input::old('plan') == 0 ? 'hidden' : '' }}">
            
                {{ Form::label('daysPrice', trans('orders.time'), array('class' => 'col-sm-2 control-label')) }}
                <div id="daysPriceSelect" class="col-sm-10">
                	@if(Input::old('arrDaysPrice'))
                	<select class="form-control" name="daysPrice">
                		@foreach(Input::old('arrDaysPrice') as $key => $val)
                			<option value="{{ $key }}" {{ Input::old('daysPrice') == $key ? 'selected' : '' }}>{{ $val }}</option>
                		@endforeach
                	</select>
                	@foreach(Input::old('arrDaysPrice') as $key => $val)
                		{{ Form::hidden('arrDaysPrice['.$key.']', $val) }}
                	@endforeach
                	@endif
                </div>
                <div class="col-sm-offset-2 col-sm-10">
                        {{ ($errors->has('daysPrice') ? $errors->first('daysPrice') : '') }}
                </div>
        
        </div>
        
        <div class="form-group {{ ($errors->has('username')) ? 'has-error' : '' }}">
                
                {{ Form::label('username', 'Instagram Username', array('class' => 'col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    {{ Form::text('username', Input::old('username'), array('class'=>'form-control', 'id'=>'username', 'placeholder'=> 'Username')) }}
                </div>
                <div class="col-sm-offset-2 col-sm-10">
                        {{ ($errors->has('username') ? $errors->first('username') : '') }}
                </div>
        
        </div>
        
        <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    
                        
                        {{ Form::submit(trans('pages.actionsave') . ' Order', array('class' => 'btn btn-primary', 'id' => 'saveorder')) }}
                        
                </div>
        </div>

    {{ Form::close() }}
</div>
