<h2>{{trans('pages.create')}} {{trans('pages.order') }}</h2>
    
<div class="well">
    {{ Form::open(array('action' 	=> array('SubscriptionController@update', $order->id), 
    					'method' 	=> 'put', 
    					'class'		=> 'form-horizontal', 
    					'role' 		=> 'form', 
    					'files'		=> true)) }}

        <div class="form-group {{ ($errors->has('services')) ? 'has-error' : '' }}">
            
                {{ Form::label('services', trans('orders.service'), array('class' => 'col-sm-2 control-label')) }}
                <div class="col-sm-10" style="padding-top: 7px;">
                	{{ Form::hidden('services', $order->service->id, ['id'=>'selectService']) }}
                    {{ $order->service->name }}
                </div>
                <div class="col-sm-offset-2 col-sm-10">
                        {{ ($errors->has('services') ? $errors->first('services') : '') }}
                </div>
        
        </div>

        <div class="form-group {{ ($errors->has('plan')) ? 'has-error' : '' }}">
            
                {{ Form::label('plan', trans('orders.plan'), array('class' => 'col-sm-2 control-label')) }}
                <div class="col-sm-10" style="padding-top: 7px;">
                	{{ $order->plan->name }}
                </div>
                <div class="col-sm-offset-2 col-sm-10">
                        {{ ($errors->has('plan') ? $errors->first('plan') : '') }}
                </div>
        
        </div>

        <div class="form-group {{ ($errors->has('username')) ? 'has-error' : '' }}">
            
                {{ Form::label('username', trans('orders.username'), array('class' => 'col-sm-2 control-label')) }}
                <div class="col-sm-10" style="padding-top: 7px;">
                    {{ $order->link }}
                </div>
                <div class="col-sm-offset-2 col-sm-10">
                        {{ ($errors->has('username') ? $errors->first('username') : '') }}
                </div>
        
        </div>

        <div class="form-group {{ ($errors->has('daysPrice')) ? 'has-error' : '' }}">
            
                {{ Form::label('daysPrice', trans('orders.Extend Days'), array('class' => 'col-sm-2 control-label')) }}
                <div class="col-sm-10">
                    {{ $price }}
                </div>
                <div class="col-sm-offset-2 col-sm-10">
                        {{ ($errors->has('daysPrice') ? $errors->first('daysPrice') : '') }}
                </div>
        
        </div>
        
        <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    
                        
                        {{ Form::submit(trans('pages.actionupdate') . ' Order', array('class' => 'btn btn-primary', 'id' => 'saveorder')) }}
                        
                </div>
        </div>

    {{ Form::close() }}
</div>
