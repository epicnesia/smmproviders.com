<div class="well container-fluid">
	<div class="row">
		@include('template.subscription.buttons')

		<div class="col-lg-4">

		</div>
	</div>
</div>

<h3>{{ $service->name }} Plans</h3>

<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
			{{ Form::open(array('action' => 'SubscriptionController@deleteList', 'id'=>'deleteForm')) }}
			{{ Form::hidden('service_id', $service->id) }}
			<div class="buttonOnTop">
				<a class="btn btn-primary" href="{{ URL::to('subscription/'.$service->id.'/add_plan') }}"> <i class="fa fa-plus fa-lg"></i> &nbsp{{trans('services.Add Plan')}}</a>
				{{ Form::submit('Delete List', array('name'=>'submit', 'id'=>'delete_button', 'class' => 'btn btn-danger')) }}
			</div>
			<br />
            <table  class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
					<th rowspan="2">#</th>
					<th rowspan="2">id</th>
                    <th rowspan="2">{{ trans('services.Name') }}</th>
                    <th rowspan="2">{{ trans('services.Count') }}</th>
                    <th colspan="2">{{ trans('services.7_days') }}</th>
                    <th colspan="2">{{ trans('services.30_days') }}</th>
                    <th colspan="2">{{ trans('services.60_days') }}</th>
                    <th colspan="2">{{ trans('services.90_days') }}</th>
                    <th rowspan="2">{{ trans('services.Option') }}</th>
                </tr>
                <tr>
                	<th>{{ trans('services.Price') }}</th>
                	<th>{{ trans('services.Cost') }}</th>
                	<th>{{ trans('services.Price') }}</th>
                	<th>{{ trans('services.Cost') }}</th>
                	<th>{{ trans('services.Price') }}</th>
                	<th>{{ trans('services.Cost') }}</th>
                	<th>{{ trans('services.Price') }}</th>
                	<th>{{ trans('services.Cost') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($plans as $plan)
                <tr>
					<td>
						<input type="checkbox" name="item[]" id="plans[]" value="{{ $plan->id }}" />
					</td>
					<td>{{$plan->id}}</td>
                	<td>{{$plan->name}}</td>
                	<td>{{$plan->count}}</td>
                	<td>{{$plan->price_7_days}}</td>
                	<td>{{$plan->cost_7_days}}</td>
                	<td>{{$plan->price_30_days}}</td>
                	<td>{{$plan->cost_30_days}}</td>
                	<td>{{$plan->price_60_days}}</td>
                	<td>{{$plan->cost_60_days}}</td>
                	<td>{{$plan->price_90_days}}</td>
                	<td>{{$plan->cost_90_days}}</td>
                	<td>
                		<a class="btn btn-info" href="{{ action('SubscriptionController@editPlan', array($plan->id)) }}"> <i class="fa fa-edit fa-lg"></i> {{trans('pages.actionedit')}}</a>
						<a class="btn btn-danger action_confirm" href="{{ action('SubscriptionController@destroy', array($plan->id)) }}" data-token="{{ Session::getToken() }}" data-method="delete"> <i class="fa fa-trash fa-lg"></i> {{trans('pages.actiondelete')}}</a> 
						@if($plan->status == 1) 
							<a class="btn btn-success" href="{{ action('SubscriptionController@changeStatus', array($plan->id)) }}"> <i class="fa fa-close fa-lg"></i> {{trans('pages.actionDisable')}}</a> 
						@elseif($plan->status == 0) 
							<a class="btn btn-danger" href="{{ action('SubscriptionController@changeStatus', array($plan->id)) }}"> <i class="fa fa-check fa-lg"></i> {{trans('pages.actionEnable')}}</a> 
						@endif 
                	</td>
                </tr>
                @endforeach
                </tbody>
            </table>
            {{$plans->links()}}
			{{ Form::close() }}
        </div>
    </div>
</div>

