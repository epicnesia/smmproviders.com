<div class="well container-fluid">
	<div class="row">
		@include('template.subscription.buttons')

		<div class="col-lg-4">

		</div>
	</div>
</div>

<h3>{{ trans('services.Edit Plan') }}</h3>

<div class="well">
	{{ Form::open(array('action' => array('SubscriptionController@updatePlan', $plan->id), 'class' => 'form-horizontal', 'role' => 'form', 'files'=> true)) }}

	<div class="form-group {{ ($errors->has('name')) ? 'has-error' : '' }}">
		{{ Form::hidden('service_id', $service->id) }}
		{{ Form::label('name', trans('services.serviceName'), array('class' => 'col-sm-2 control-label')) }}
		<div class="col-sm-10">
			<?php $def_name = Input::old('name') !== null ? Input::old('name') : $plan->name; ?>
			{{ Form::text('name', $def_name, array('class'=>'form-control', 'placeholder'=>trans('services.serviceName'))) }}
		</div>
		<div class="col-sm-offset-2 col-sm-10">
			{{ ($errors->has('name') ? $errors->first('name') : '') }}
		</div>
	</div>

	<div class="form-group {{ ($errors->has('count')) ? 'has-error' : '' }}">
		{{ Form::label('count', trans('services.Count'), array('class' => 'col-sm-2 control-label')) }}
		<div class="col-sm-10">
			<?php $def_count = Input::old('count') !== null ? Input::old('count') : $plan->count; ?>
			{{ Form::text('count', $def_count, array('class'=>'form-control', 'placeholder'=>trans('services.Count'))) }}
		</div>
		<div class="col-sm-offset-2 col-sm-10">
			{{ ($errors->has('count') ? $errors->first('count') : '') }}
		</div>
	</div>

	<div class="form-group {{ ($errors->has('price_7_days') || $errors->has('cost_7_days')) ? 'has-error' : '' }}">
		{{ Form::label('price_7_days', trans('services.price_7_days'), array('class' => 'col-sm-2 control-label')) }}
		<div class="col-sm-4">
			<?php $def_price_7_days = Input::old('price_7_days') !== null ? Input::old('price_7_days') : $plan->price_7_days; ?>
			{{ Form::text('price_7_days', $def_price_7_days, array('class'=>'form-control', 'placeholder'=>trans('services.price_7_days'))) }}
		</div>
		{{ Form::label('cost_7_days', trans('services.cost_7_days'), array('class' => 'col-sm-2 control-label')) }}
		<div class="col-sm-4">
			<?php $def_cost_7_days = Input::old('cost_7_days') !== null ? Input::old('cost_7_days') : $plan->cost_7_days; ?>
			{{ Form::text('cost_7_days', $def_cost_7_days, array('class'=>'form-control', 'placeholder'=>trans('services.cost_7_days'))) }}
		</div>
		<div class="col-sm-offset-2 col-sm-4">
			{{ ($errors->has('price_7_days') ? $errors->first('price_7_days') : '') }}
		</div>
		<div class="col-sm-offset-2 col-sm-4">
			{{ ($errors->has('cost_7_days') ? $errors->first('cost_7_days') : '') }}
		</div>
	</div>

	<div class="form-group {{ ($errors->has('price_30_days') || $errors->has('cost_30_days')) ? 'has-error' : '' }}">
		{{ Form::label('price_30_days', trans('services.price_30_days'), array('class' => 'col-sm-2 control-label')) }}
		<div class="col-sm-4">
			<?php $def_price_30_days = Input::old('price_30_days') !== null ? Input::old('price_30_days') : $plan->price_30_days; ?>
			{{ Form::text('price_30_days', $def_price_30_days, array('class'=>'form-control', 'placeholder'=>trans('services.price_30_days'))) }}
		</div>
		{{ Form::label('cost_30_days', trans('services.cost_30_days'), array('class' => 'col-sm-2 control-label')) }}
		<div class="col-sm-4">
			<?php $def_cost_30_days = Input::old('cost_30_days') !== null ? Input::old('cost_30_days') : $plan->cost_30_days; ?>
			{{ Form::text('cost_30_days', $def_cost_30_days, array('class'=>'form-control', 'placeholder'=>trans('services.cost_30_days'))) }}
		</div>
		<div class="col-sm-offset-2 col-sm-4">
			{{ ($errors->has('price_30_days') ? $errors->first('price_30_days') : '') }}
		</div>
		<div class="col-sm-offset-2 col-sm-4">
			{{ ($errors->has('cost_30_days') ? $errors->first('cost_30_days') : '') }}
		</div>
	</div>

	<div class="form-group {{ ($errors->has('price_60_days') || $errors->has('cost_60_days')) ? 'has-error' : '' }}">
		{{ Form::label('price_60_days', trans('services.price_60_days'), array('class' => 'col-sm-2 control-label')) }}
		<div class="col-sm-4">
			<?php $def_price_60_days = Input::old('price_60_days') !== null ? Input::old('price_60_days') : $plan->price_60_days; ?>
			{{ Form::text('price_60_days', $def_price_60_days, array('class'=>'form-control', 'placeholder'=>trans('services.price_60_days'))) }}
		</div>
		{{ Form::label('cost_60_days', trans('services.cost_60_days'), array('class' => 'col-sm-2 control-label')) }}
		<div class="col-sm-4">
			<?php $def_cost_60_days = Input::old('cost_60_days') !== null ? Input::old('cost_60_days') : $plan->cost_60_days; ?>
			{{ Form::text('cost_60_days', $def_cost_60_days, array('class'=>'form-control', 'placeholder'=>trans('services.cost_60_days'))) }}
		</div>
		<div class="col-sm-offset-2 col-sm-4">
			{{ ($errors->has('price_60_days') ? $errors->first('price_60_days') : '') }}
		</div>
		<div class="col-sm-offset-2 col-sm-4">
			{{ ($errors->has('cost_60_days') ? $errors->first('cost_60_days') : '') }}
		</div>
	</div>

	<div class="form-group {{ ($errors->has('price_90_days') || $errors->has('cost_90_days')) ? 'has-error' : '' }}">
		{{ Form::label('price_90_days', trans('services.price_90_days'), array('class' => 'col-sm-2 control-label')) }}
		<div class="col-sm-4">
			<?php $def_price_90_days = Input::old('price_90_days') !== null ? Input::old('price_90_days') : $plan->price_90_days; ?>
			{{ Form::text('price_90_days', $def_price_90_days, array('class'=>'form-control', 'placeholder'=>trans('services.price_90_days'))) }}
		</div>
		{{ Form::label('cost_90_days', trans('services.cost_90_days'), array('class' => 'col-sm-2 control-label')) }}
		<div class="col-sm-4">
			<?php $def_cost_90_days = Input::old('cost_90_days') !== null ? Input::old('cost_90_days') : $plan->cost_90_days; ?>
			{{ Form::text('cost_90_days', $def_cost_90_days, array('class'=>'form-control', 'placeholder'=>trans('services.cost_90_days'))) }}
		</div>
		<div class="col-sm-offset-2 col-sm-4">
			{{ ($errors->has('price_90_days') ? $errors->first('price_90_days') : '') }}
		</div>
		<div class="col-sm-offset-2 col-sm-4">
			{{ ($errors->has('cost_90_days') ? $errors->first('cost_90_days') : '') }}
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">

			{{ Form::submit(trans('pages.actionsave') . ' Plan', array('class' => 'btn btn-primary', 'id' => 'saveorder')) }}

		</div>
	</div>

	{{ Form::close() }}
</div>
