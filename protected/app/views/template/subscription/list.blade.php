<div class="well container-fluid">
	<div class="row">
		@include('template.subscription.buttons')
		
		<div class="col-lg-4">
			{{ Form::open(array('url' => Request::url(), 'method' => 'get', 'class' => 'pull-right')) }}
                <div class="input-group">
                    {{ Form::text('s', null, array('class' => 'form-control', 'placeholder' => trans('pages.Search'), 'id'=>'search')) }}
                    <div class="input-group-btn">
                        {{ Form::submit(trans('pages.Search'), array('class' => 'btn btn-primary')) }}
                    </div>
                </div>
        	{{ Form::close() }}
		</div>
	</div>
</div>

@if(count($orders)!=0)
<div class="row">
    <div class="col-md-12">
        <div >
            <table  class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th>{{trans('users.id')}}</th>
                    <th>{{trans('users.user')}}</th>
                    <th>{{trans('orders.link')}}</th>
                    <th>{{trans('orders.charge')}}</th>
                    <th>{{trans('orders.startcount')}}</th>
                    <th>{{trans('orders.quantity')}}</th>
                    <th>{{trans('orders.type')}}</th>
                    <th>{{trans('orders.status')}}</th>
                    <th>{{trans('orders.remains')}}</th>
                    <th>{{trans('orders.created')}}</th>
                    <th>{{trans('orders.expired')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($orders as $order)
                <tr>
                    <td>{{ $order->id }}</td>
                    <td>{{ $order->seller->username }}</td>
                    @if(Sentry::getUser()->id == $order->seller->id)
                    <td><a href="{{ action('SubscriptionController@edit', $order->id) }}">{{ $order->link }}</a></td>
                    @else
                    <td>{{ $order->link }}</td>
                    @endif
                    <td>{{ $order->charge }}</td>
                    <td>{{ $order->start_count }}</td>
                    <td>{{ $order->quantity }}</td>
                    <td>{{ $order->service->name }}</td>
                    <td>{{ $status_arr[$order->status] }}</td>
                    <td>{{ $order->remains }}</td>
                    <td>{{ $order->created_at }}</td>
                    <td>{{ $order->expired_date }}</td>
                </tr>

                @endforeach
                </tbody>
            </table>
            {{$orders->links()}}
        </div>
    </div>
</div>
@endif