<div class="col-lg-8">
	<?php 
		$red_url 		= str_replace(" ", "_", strtolower($service -> name));
	?>
	@if(Sentry::getUser()->hasAccess('admin'))
	{{ link_to($red_url, 'Plans', ['class' => 'btn btn-default']) }}
	@else
	{{ link_to($red_url, 'Add Order', ['class' => 'btn btn-default']) }}
	@endif
	
	{{ link_to($red_url.'/active', 'Active Clients ('.$active_count.')', ['class' => 'btn btn-default']) }}
	{{ link_to($red_url.'/expired', 'Expired Clients ('.$expired_count.')', ['class' => 'btn btn-default']) }}
</div>