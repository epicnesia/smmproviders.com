<h3>{{trans('pages.listof')}} {{trans('users.users')}}</h3>
<br/>

<div class="row">
        <div class="col-md-12">
                <div class="">
                        <table id="tbl_example" class="table table-striped table-hover table-bordered">
                                <thead>
                                        <tr>
                                                <th>{{trans('users.id')}}</th>
                                                <th>{{trans('users.username')}}</th>
                                                <th>{{trans('api.key')}}</th>
                                                <th></th>
                                        </tr>
                                </thead>
                                <tbody>
                                        @foreach ($users as $user)
                                        <tr>
                                                <td>{{$user->id}}</td>
                                                <td>{{ $user->username }}</td>
                                                <td>{{ $api_key[$user->id] }}</td>
                                                <td>
                                                        <a class="btn btn-info btn-xs" href="{{ action('ApiKeyGeneratorsController@generateKeyFor', array($user->id)) }}"> <i class="fa fa-refresh fa-lg"></i></a>
                                                        <?php
                                                        if ($api_enable[$user->id] == 1) {
                                                        echo '<a class="btn btn-warning btn-xs" href="' . action('ApiKeyGeneratorsController@changeApiAccess', array('id' => $user -> id, 'status' => '0')) . '">
<i class="fa fa-close fa-lg"></i> Disable</a>';
                                                } else {
                                                        echo '<a class="btn btn-success btn-xs" href="' . action('ApiKeyGeneratorsController@changeApiAccess', array('id' => $user -> id, 'status' => '1')) . '">
<i class="fa fa-check fa-lg"></i> Enable</a>';
                                                }
                                                ?>
                                                </td>

                                        </tr>
                                        @endforeach
                                </tbody>
                        </table>
                </div>
        </div>
</div>

