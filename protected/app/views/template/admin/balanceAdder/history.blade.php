<div class="well">
        <div class="btn-group">
                <a class="btn btn-default" href="{{ action('PaymentController@balanceAdder') }}">{{ trans('payments.user_list') }}</a>
                <a class="btn btn-default" href="{{ action('PaymentController@balanceHistory') }}">{{ trans('payments.history') }}</a>
        </div>
</div> 

<h3>{{trans('pages.listof')}} {{trans('payments.balanceAdderHistory')}}</h3>      
@if(count($histories)!=0)
<div class="row">
        <div class="col-md-12">
            <div class="">
                <table class="table table-striped table-hover table-bordered">
                <thead>
                    <tr>
                        <th>{{trans('payments.id')}}</th>
                        <th>{{trans('payments.username')}}</th>
                        <th>{{trans('payments.balance')}}</th>
                        <th>{{trans('payments.balanceAmount')}} Added</th>
                        <th>{{trans('payments.created')}}</th>
                        <th>{{trans('payments.updated')}}</th>
                        <th>{{trans('payments.action')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($histories as $history)
                    <tr>
                        <td>{{ $history->id }}</td>
                        <td>{{ $history->one_user?$history->one_user->username:'' }}</td>
                        <td>{{ round($history->balance,2) }}</td>
                        <td>{{ round($history->amount,2) }}</td>
                        <td>{{ $history->created_at }}</td>
                        <td>{{ $history->updated_at }}</td>                        
                        <td>
                        	@if (Sentry::getUser() -> hasAccess('su_admin'))
                        		<a class="btn btn-primary btn-xs action_confirm" href="{{action('PaymentController@editBalance', array($history->id))}}">
                                                                                <i class="fa fa-edit fa-lg"></i></a>
                                <a class="btn btn-danger btn-xs action_confirm" href="{{action('PaymentController@cancelBalance', array($history->id))}}" data-token="{{Session::getToken()}}" data-method="delete">
                                                                                    <i class="fa fa-trash fa-lg"></i></a>
                        	@endif 
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{$histories->links()}}
        </div>
    </div>
</div>
@endif
