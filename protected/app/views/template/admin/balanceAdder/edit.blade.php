<h3>{{trans('payments.editBalance')}} Of {{ $users->username }}</h3>
<div class="well">
    {{ Form::open(array('action' => 'PaymentController@updateBalance', 'method' => 'post', 'class' => 'form-horizontal', 'role' => 'form')) }}

    <div class="form-group {{ ($errors->has('b_amount')) ? 'has-error' : '' }}">
        {{ Form::label('b_amount', trans('payments.balanceAmount'), array('class' => 'col-sm-2 control-label')) }}
        <div class="col-sm-10">
            <?php 
                $oldAmount = isset($balance) ? $balance->amount : Input::old('b_amount'); 
                $oldBalanceId = isset($balance) ? $balance->id : Input::old('balance_id');
                $oldUserId = isset($users) ? $users->id : Input::old('user_id');
            ?>
            {{ Form::hidden('user_id', $oldUserId, array('class' => 'form-control', 'placeholder' => trans('payments.balanceAmount'))) }}
            {{ Form::hidden('balance_id', $oldBalanceId, array('class' => 'form-control', 'placeholder' => trans('payments.balanceAmount'))) }}
            {{ Form::text('b_amount', $oldAmount, array('class' => 'form-control', 'placeholder' => trans('payments.balanceAmount'))) }}
        </div>
        <div class="col-sm-offset-2 col-sm-10">
                {{ ($errors->has('b_amount') ? $errors->first('b_amount') : '') }}
        </div>
    </div>
    
    <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                        {{ Form::submit(trans('payments.editBalance'), array('class' => 'btn btn-primary')) }}
                </div>
    </div>

    {{ Form::close() }}
</div>
