<div class="well">
        <div class="btn-group">
                <a class="btn btn-default" href="{{ action('PaymentController@balanceAdder') }}">{{ trans('payments.user_list') }}</a>
                <a class="btn btn-default" href="{{ action('PaymentController@balanceHistory') }}">{{ trans('payments.history') }}</a>
        </div>
</div> 

<h3>{{trans('pages.listof')}} {{trans('users.users')}}</h3>     

<div class="row">
        <div class="col-md-12">
            <div class="">
                <table id="tbl_example" class="table table-striped table-hover table-bordered">
                <thead>
                    <tr>
                        <th>{{trans('users.id')}}</th>
                        <th>{{trans('users.username')}}</th>
<!--                        <th>{{trans('users.password')}}</th>-->
                        <th>{{trans('users.balance')}}</th>
<!--                        <th>{{trans('users.spent')}}</th>-->
<!--                        <th>{{trans('users.registrationdate')}}</th>-->
                        <th>{{trans('users.lastlogin')}}</th>
<!--                        <th>{{trans('users.rates')}}</th>                        -->
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td>{{ $user->username }}</td>
<!--                        <td>{{ $user->password_nohash }}</td>-->
                        <td>{{ round($user->balance,2) }}</td>
<!--                        <td>{{ round($user->spent,2) }}</td>-->
<!--                        <td>{{ $user->created_at }}</td>-->
                        <td>{{ $user->lastlogin }}</td>
<!--                        <td><a href="{{ action('RateController@edit', array($user->id)) }}">edit rates</a></td>                        -->
                        <td>
                                <a class="btn btn-primary btn-xs action_confirm" href="{{ action('PaymentController@addBalance', array($user->id)) }}">
                                <i class="fa fa-plus fa-lg"></i></a>
                        </td>

                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

