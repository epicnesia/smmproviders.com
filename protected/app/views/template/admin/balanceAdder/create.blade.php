<h3>{{trans('payments.addBalance')}} Of {{ $users->username }}</h3>
<div class="well">
    {{ Form::open(array('action' => 'PaymentController@storeBalance', 'method' => 'post', 'class' => 'form-horizontal', 'role' => 'form')) }}

    <div class="form-group {{ ($errors->has('b_amount')) ? 'has-error' : '' }}">
        {{ Form::label('b_amount', trans('payments.balanceAmount'), array('class' => 'col-sm-2 control-label')) }}
        <div class="col-sm-10">
            {{ Form::hidden('id', $users->id, array('class' => 'form-control', 'placeholder' => trans('payments.balanceAmount'))) }}
            {{ Form::text('b_amount', Input::old('b_amount'), array('class' => 'form-control', 'placeholder' => trans('payments.balanceAmount'))) }}
        </div>
        <div class="col-sm-offset-2 col-sm-10">
                {{ ($errors->has('b_amount') ? $errors->first('b_amount') : '') }}
        </div>
    </div>
    
    <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                        {{ Form::submit(trans('payments.addBalance'), array('class' => 'btn btn-primary')) }}
                </div>
    </div>

    {{ Form::close() }}
</div>
