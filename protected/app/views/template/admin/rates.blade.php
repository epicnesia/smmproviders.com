<h3>{{trans('users.rates')}}</h3>
<div class="well">
	{{ Form::open(array('action' => array('RateController@update', $user_id),
	'method' => 'put',
	'class' => 'form-horizontal',
	'role' => 'form'
	)) }}
	<?php $id = isset($user_id) ? $user_id : Input::old('userId'); ?>
	{{ Form::hidden('userId', $id, array('class' => 'form-control', 'placeholder' => trans('users.rates'))) }}
	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				
				<table class="table table-striped table-hover table-bordered">
					<thead>
						<tr>
							<th colspan="4">Regular Services</th>
						</tr>
						<tr>
							<th>{{trans('users.id')}}</th>
							<th>{{trans('users.service_name')}}</th>
							<th>{{trans('users.normal_price')}}</th>
							<th>{{trans('users.custom_price')}}</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($services as $service)
						<?php $old_r = isset($rate[$service -> id]) ? $rate[$service -> id] : Input::old('rates['.$service->id.']'); ?>
						<tr>
							<td>{{$service->id}}</td>
							<td>{{$service->name}}</td>
							<td>{{$service->price}}</td>
							<td> {{ Form::text('rates['.$service->id.']', $old_r, array('class' => 'form-control', 'placeholder' => trans('users.rates'))) }} </td>

						</tr>
						@endforeach
					</tbody>
				</table>
				
				<!-- Auto Likes -->
				<table class="table table-striped table-hover table-bordered">
					<thead>
						<tr>
							<th colspan="11">Auto Likes Plans</th>
						</tr>
						<tr>
							<th rowspan="2">id</th>
		                    <th rowspan="2">{{ trans('services.Name') }}</th>
		                    <th rowspan="2">{{ trans('services.Count') }}</th>
		                    <th colspan="2">{{ trans('services.7_days') }}</th>
		                    <th colspan="2">{{ trans('services.30_days') }}</th>
		                    <th colspan="2">{{ trans('services.60_days') }}</th>
		                    <th colspan="2">{{ trans('services.90_days') }}</th>
						</tr>
						<tr>
		                	<th>{{ trans('services.normalPrice') }}</th>
		                	<th>{{ trans('services.customPrice') }}</th>
		                	<th>{{ trans('services.normalPrice') }}</th>
		                	<th>{{ trans('services.customPrice') }}</th>
		                	<th>{{ trans('services.normalPrice') }}</th>
		                	<th>{{ trans('services.customPrice') }}</th>
		                	<th>{{ trans('services.normalPrice') }}</th>
		                	<th>{{ trans('services.customPrice') }}</th>
		                </tr>
					</thead>
					<tbody>
						@foreach ($autoLikes as $alPlan)
						<?php 
							$old_alr[$alPlan->id][0] = isset($autoLikesRates[$alPlan -> id][0]) ? $autoLikesRates[$alPlan -> id][0] : Input::old('autoLikesRates['.$alPlan->id.'][0]'); 
							$old_alr[$alPlan->id][1] = isset($autoLikesRates[$alPlan -> id][1]) ? $autoLikesRates[$alPlan -> id][1] : Input::old('autoLikesRates['.$alPlan->id.'][1]');
							$old_alr[$alPlan->id][2] = isset($autoLikesRates[$alPlan -> id][2]) ? $autoLikesRates[$alPlan -> id][2] : Input::old('autoLikesRates['.$alPlan->id.'][2]');
							$old_alr[$alPlan->id][3] = isset($autoLikesRates[$alPlan -> id][3]) ? $autoLikesRates[$alPlan -> id][3] : Input::old('autoLikesRates['.$alPlan->id.'][3]');
						?>
						<tr>
							<td>{{$alPlan->id}}</td>
							<td>{{$alPlan->name}}</td>
							<td>{{$alPlan->count}}</td>
							<td>{{$alPlan->price_7_days}}</td>
							<td> {{ Form::text('autoLikesRates['.$alPlan->id.'][0]', $old_alr[$alPlan->id][0], array('class' => 'form-control', 'placeholder' => trans('users.rates'))) }} </td>
							<td>{{$alPlan->price_30_days}}</td>
							<td> {{ Form::text('autoLikesRates['.$alPlan->id.'][1]', $old_alr[$alPlan->id][1], array('class' => 'form-control', 'placeholder' => trans('users.rates'))) }} </td>
							<td>{{$alPlan->price_60_days}}</td>
							<td> {{ Form::text('autoLikesRates['.$alPlan->id.'][2]', $old_alr[$alPlan->id][2], array('class' => 'form-control', 'placeholder' => trans('users.rates'))) }} </td>
							<td>{{$alPlan->price_90_days}}</td>
							<td> {{ Form::text('autoLikesRates['.$alPlan->id.'][3]', $old_alr[$alPlan->id][3], array('class' => 'form-control', 'placeholder' => trans('users.rates'))) }} </td>
						</tr>
						@endforeach
					</tbody>
				</table>
				<!-- End Auto Likes -->
				
				<!-- Auto Views -->
				<table class="table table-striped table-hover table-bordered">
					<thead>
						<tr>
							<th colspan="11">Auto Views Plans</th>
						</tr>
						<tr>
							<th rowspan="2">id</th>
		                    <th rowspan="2">{{ trans('services.Name') }}</th>
		                    <th rowspan="2">{{ trans('services.Count') }}</th>
		                    <th colspan="2">{{ trans('services.7_days') }}</th>
		                    <th colspan="2">{{ trans('services.30_days') }}</th>
		                    <th colspan="2">{{ trans('services.60_days') }}</th>
		                    <th colspan="2">{{ trans('services.90_days') }}</th>
						</tr>
						<tr>
		                	<th>{{ trans('services.normalPrice') }}</th>
		                	<th>{{ trans('services.customPrice') }}</th>
		                	<th>{{ trans('services.normalPrice') }}</th>
		                	<th>{{ trans('services.customPrice') }}</th>
		                	<th>{{ trans('services.normalPrice') }}</th>
		                	<th>{{ trans('services.customPrice') }}</th>
		                	<th>{{ trans('services.normalPrice') }}</th>
		                	<th>{{ trans('services.customPrice') }}</th>
		                </tr>
					</thead>
					<tbody>
						@foreach ($autoViews as $avPlan)
						<?php 
							$old_avr[$avPlan->id][0] = isset($autoViewsRates[$avPlan -> id][0]) ? $autoViewsRates[$avPlan -> id][0] : Input::old('autoViewsRates['.$avPlan->id.'][0]'); 
							$old_avr[$avPlan->id][1] = isset($autoViewsRates[$avPlan -> id][1]) ? $autoViewsRates[$avPlan -> id][1] : Input::old('autoViewsRates['.$avPlan->id.'][1]');
							$old_avr[$avPlan->id][2] = isset($autoViewsRates[$avPlan -> id][2]) ? $autoViewsRates[$avPlan -> id][2] : Input::old('autoViewsRates['.$avPlan->id.'][2]');
							$old_avr[$avPlan->id][3] = isset($autoViewsRates[$avPlan -> id][3]) ? $autoViewsRates[$avPlan -> id][3] : Input::old('autoViewsRates['.$avPlan->id.'][3]');
						?>
						<tr>
							<td>{{$avPlan->id}}</td>
							<td>{{$avPlan->name}}</td>
							<td>{{$avPlan->count}}</td>
							<td>{{$avPlan->price_7_days}}</td>
							<td> {{ Form::text('autoViewsRates['.$avPlan->id.'][0]', $old_avr[$avPlan->id][0], array('class' => 'form-control', 'placeholder' => trans('users.rates'))) }} </td>
							<td>{{$avPlan->price_30_days}}</td>
							<td> {{ Form::text('autoViewsRates['.$avPlan->id.'][1]', $old_avr[$avPlan->id][1], array('class' => 'form-control', 'placeholder' => trans('users.rates'))) }} </td>
							<td>{{$avPlan->price_60_days}}</td>
							<td> {{ Form::text('autoViewsRates['.$avPlan->id.'][2]', $old_avr[$avPlan->id][2], array('class' => 'form-control', 'placeholder' => trans('users.rates'))) }} </td>
							<td>{{$avPlan->price_90_days}}</td>
							<td> {{ Form::text('autoViewsRates['.$avPlan->id.'][3]', $old_avr[$avPlan->id][3], array('class' => 'form-control', 'placeholder' => trans('users.rates'))) }} </td>
						</tr>
						@endforeach
					</tbody>
				</table>
				<!-- End Auto Views -->
				
			</div>
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-offset-10 col-sm-2">
			{{ link_to('admin/users', 'Back To Users', array('class' => 'btn btn-default')) }}
			{{ Form::submit('Save '.trans('users.rates'), array('class' => 'btn btn-primary')) }}
		</div>
	</div>

	{{ Form::close() }}
</div>