    
    <div class="well">
        {{ Form::open(array(
        'action' => array('UserController@ubah', $user->id),
        'class' => 'form-horizontal'
        )) }}

        <div class="form-group {{ $errors->has('oldPassword') ? 'has-error' : '' }}">
            {{ Form::label('oldPassword', trans('users.oldpassword_lbl'), array()) }}
            {{ Form::text('oldPassword',is_null($user->password_nohash)? Input::old('oldPassword'):$user->password_nohash, array('class' => 'form-control', 'placeholder' => trans('users.oldpassword_lbl'))) }}
        </div>

        <div class="form-group {{ $errors->has('newPassword') ? 'has-error' : '' }}">
            {{ Form::label('newPassword', trans('users.newpassword_lbl'), array()) }}
            {{ Form::text('newPassword',is_null($user->password_nohash)? Input::old('newUsername'):$user->password_nohash, array('class' => 'form-control', 'placeholder' => trans('users.newpassword_lbl'))) }}
        </div>

        <div class="form-group {{ $errors->has('newPassword_confirmation') ? 'has-error' : '' }}">
            {{ Form::label('newPassword_confirmation', trans('users.newcompassword_lbl'), array()) }}
            {{ Form::text('newPassword_confirmation',is_null($user->password_nohash)? Input::old('newPassword_confirmation'):$user->password_nohash, array('class' => 'form-control', 'placeholder' => trans('users.newcompassword_lbl'))) }}
        </div>
		<div class="form-group">
		<font color="#ff0000">
		{{ ($errors->has('oldPassword') ? $errors->first('oldPassword') : '') }}
        {{ ($errors->has('newPassword') ?  '<br />' . $errors->first('newPassword') : '') }}
        {{ ($errors->has('newPassword_confirmation') ? '<br />' . $errors->first('newPassword_confirmation') : '') }}        
        </font>
        </div>
        
        {{ Form::submit(trans('users.change_password'), array('class' => 'btn btn-primary'))}}

        {{ Form::close() }}
    </div>
    
<br />
<div class="well">
        {{ Form::open(array(
        'action' => array('UserController@ubahPaypalDetail', $user->id),
        'class' => 'form-horizontal'
        )) }}

        <div class="form-group {{ $errors->has('paypall_email') ? 'has-error' : '' }}">
            {{ Form::label('paypall_email', trans('users.paypall') . ' ' . trans('users.email'), array()) }}
            {{ Form::text('paypall_email',is_null($user->paypall_email)? Input::old('paypall_email'):$user->paypall_email, array('class' => 'form-control', 'placeholder' => trans('users.paypall') . ' ' . trans('users.email'))) }}
        </div>      
		<div class="form-group">
		<font color="#ff0000">
		{{ ($errors->has('paypall_email') ? $errors->first('paypall_email') : '') }}      
        </font>
        </div>
        
        {{ Form::submit(trans('users.change_paypal_detail'), array('class' => 'btn btn-primary'))}}

        {{ Form::close() }}
    </div>
