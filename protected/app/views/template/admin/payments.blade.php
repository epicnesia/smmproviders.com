<div class="well">
    <form action="" method="get" id="filter" name="frm_filter">
    	<div class="form-group">
        {{ Form::label('user_name', trans('users.user'), array('class' => 'col-sm-1 control-label')) }}
        <div class="col-sm-6">
            {{ Form::select('user_name', $user_names, Input::get('user_name'), array('class'=>'form-control', "onchange" => "document.frm_filter.submit();")) }}
        </div>
        </div>
    </form>
    <br />
    <br />
</div>
@if(count($payments)!=0)
<div class="row">
    <div class="col-md-12">
        <div class="">
            <table  class="table table-striped table-hover table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>{{trans('users.id')}}</th>
                        <th>{{trans('payments.transaction_id')}}</th>                        
                        <th>{{trans('users.user')}}</th>
                        <th>{{trans('payments.amount')}}</th>
                        <th>{{trans('payments.details')}}</th>
                        <th>{{trans('payments.date')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($payments as $payment)
                    <tr>
                        <td>
                        <input type="checkbox" name="payments[{{$payment['id']}}]" value='{{ $payment['id'] }}' />
                        </td>
                        <td>{{ $payment->id }}</td>
                        <td>{{ $payment->transaction_id }}</td>
                        <td>{{ $payment->one_user->username }}</td>
                        <td>{{ $payment->amount }}</td>
                        <td>{{ $payment->details }}</td>
                        <td>{{ $payment->created_at }}</td>
                    </tr>

                    @endforeach
                </tbody>
            </table>
            {{$payments->appends(array('user_name' => Input::has('user_name')?Input::get('user_name'):''))->links()}}
        </div>
    </div>
</div>
@endif
