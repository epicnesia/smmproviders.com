<h3>{{trans('services.addService')}}</h3>
<div class="well">
    {{ Form::open(array('action' => 'ServicesController@store', 'class' => 'form-horizontal', 'role' => 'form')) }}

    <div class="form-group {{ ($errors->has('s_name')) ? 'has-error' : '' }}">
        {{ Form::label('s_name', trans('services.serviceName'), array('class' => 'col-sm-2 control-label')) }}
        <div class="col-sm-10">
            {{ Form::text('s_name', Input::old('s_name'), array('class' => 'form-control', 'placeholder' => trans('services.serviceName'))) }}
        </div>
        <div class="col-sm-offset-2 col-sm-10">
                {{ ($errors->has('s_name') ? $errors->first('s_name') : '') }}
        </div>
    </div>
    
    <div class="form-group {{ ($errors->has('s_price')) ? 'has-error' : '' }}">
        {{ Form::label('s_price', trans('services.serviceSubtypesPrice/1000'), array('class' => 'col-sm-2 control-label')) }}
        <div class="col-sm-10">
            {{ Form::text('s_price', Input::old('s_price'), array('class' => 'form-control', 'placeholder' => trans('services.serviceSubtypesPrice'))) }}
        </div>
        <div class="col-sm-offset-2 col-sm-10">
                {{ ($errors->has('s_price') ? $errors->first('s_price') : '') }}
        </div>
    </div>
    
    <div class="form-group {{ ($errors->has('s_max_amount')) ? 'has-error' : '' }}">
        {{ Form::label('s_max_amount', trans('services.serviceMaxAmount'), array('class' => 'col-sm-2 control-label')) }}
        <div class="col-sm-10">
            {{ Form::text('s_max_amount', Input::old('s_max_amount'), array('class' => 'form-control', 'placeholder' => trans('services.serviceMaxAmount'))) }}
        </div>
        <div class="col-sm-offset-2 col-sm-10">
                {{ ($errors->has('s_max_amount') ? $errors->first('s_max_amount') : '') }}
        </div>
    </div>
    
    <div class="form-group {{ ($errors->has('s_cost')) ? 'has-error' : '' }}">
        {{ Form::label('s_cost', trans('services.Service Cost'), array('class' => 'col-sm-2 control-label')) }}
        <div class="col-sm-10">
            {{ Form::text('s_cost', Input::old('s_cost'), array('class' => 'form-control', 'placeholder' => trans('services.Service Cost'))) }}
        </div>
        <div class="col-sm-offset-2 col-sm-10">
                {{ ($errors->has('s_cost') ? $errors->first('s_cost') : '') }}
        </div>
    </div>
    
    <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                        {{ Form::submit(trans('services.createService'), array('class' => 'btn btn-primary')) }}
                </div>
    </div>

    {{ Form::close() }}
</div>
