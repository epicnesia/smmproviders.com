<h3>{{trans('pages.listof')}} {{trans('services.services')}}</h3>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div >
			{{ Form::open(array('action' => 'ServicesController@deleteList', 'id'=>'deleteForm')) }}
			<div class="buttonOnTop">
				<a class="btn btn-primary" href="{{ URL::to('/admin/services/create') }}"> <i class="fa fa-plus fa-lg"></i> &nbsp{{trans('services.addService')}}</a>
				{{ Form::submit('Delete List', array('name'=>'submit', 'id'=>'delete_button', 'class' => 'btn btn-danger')) }}
			</div>
			<br />
			<table id="tbl_example" class="table table-striped table-hover table-bordered">
				<thead>
					<th>#</th>
					<th>id</th>
					<th>{{trans('services.serviceName')}}</th>
					<th>{{trans('services.serviceSubtypesPrice/1000')}}</th>
					<th>{{trans('services.serviceMaxAmount')}}</th>
					<th>{{trans('services.Service Cost')}}</th>
					<th>{{trans('pages.options')}}</th>
				</thead>
				<tbody>
					@if(isset($services))

					@foreach($services as $service)
					<tr>
						<td>
						<input type="checkbox" name="item[]" id="service[]" value="{{ $service->id }}" />
						</td>
						<td>{{$service->id}}</td>
						<td><a href="{{ action('ServicesController@edit', array($service->id)) }}"> {{$service->name}} </a></td>
						<td>{{$service->price}}</td>
						<td>{{$service->max_amount}}</td>
						<td>{{$service->service_cost}}</td>
						<td>
							<a class="btn btn-info" href="{{ action('ServicesController@edit', array($service->id)) }}"> <i class="fa fa-edit fa-lg"></i> {{trans('pages.actionedit')}}</a>
							<a class="btn btn-danger action_confirm" href="{{ action('ServicesController@destroy', array($service->id)) }}" data-token="{{ Session::getToken() }}" data-method="delete"> <i class="fa fa-trash fa-lg"></i> {{trans('pages.actiondelete')}}</a> 
							@if($service->status == 1) 
								<a class="btn btn-success" href="{{ action('ServicesController@changeStatus', array($service->id)) }}"> <i class="fa fa-close fa-lg"></i> {{trans('pages.actionDisable')}}</a> 
							@elseif($service->status == 0) 
								<a class="btn btn-danger" href="{{ action('ServicesController@changeStatus', array($service->id)) }}"> <i class="fa fa-check fa-lg"></i> {{trans('pages.actionEnable')}}</a> 
							@endif 
						</td>
					</tr>
					@endforeach

					@endif
				</tbody>
			</table>
			{{ Form::close() }}
		</div>
	</div>
</div>
