    <h4>{{trans('pages.profile')}}</h4>
    <div class="well">
        {{ Form::open(array(
        'action' => array('UserController@perbaharui', $user->id)
        )) }}

        <div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
            {{ Form::label('newUsername', trans('users.username')) }}
            {{ Form::text('newUsername', is_null($user->username)? Input::old('newUsername'):$user->username, array('class' => 'form-control', 'placeholder' => trans('users.username'))) }}
        </div>

        <div class="form-group {{ $errors->has('skype_username') ? 'has-error' : '' }}">
            {{ Form::label('skype_username', trans('users.skype_username')) }}
            {{ Form::text('skype_username', is_null($user->skype_username)? Input::old('skype_username'):$user->skype_username, array('class' => 'form-control', 'placeholder' => trans('users.skype_username'))) }}
        </div>
        
        <div class="form-group {{ $errors->has('newPassword') ? 'has-error' : '' }}">
            {{ Form::label('newPassword', trans('users.newpassword_lbl')) }}
            {{ Form::text('newPassword', is_null($user->password_nohash)? Input::old('newUsername'):$user->password_nohash,array('class' => 'form-control', 'placeholder' => trans('users.newpassword_lbl'))) }}
        </div>

        {{ Form::submit(trans('pages.actionsave'), array('class' => 'btn btn-primary'))}}
        {{ Form::close() }}
    </div>

