<h3>{{trans('pages.listof')}} {{trans('users.users')}}</h3>
<div class="well">
	{{ Form::open(array('action' => 'UserController@store')) }}
	<div class="form-group {{ ($errors->has('username')) ? 'has-error' : '' }}">
		{{ Form::text('username', null, array('id' => 'add_username', 'class' => 'form-control', 'placeholder' => trans('users.username'))) }}
		<font color="#ff0000">{{ ($errors->has('username') ? $errors->first('username') : '') }}</font>
	</div>
	<div class="form-group {{ ($errors->has('skype_username')) ? 'has-error' : '' }}">
		{{ Form::text('skype_username', null, array('id' => 'skype_username', 'class' => 'form-control', 'placeholder' => trans('users.skype_username'))) }}
		<font color="#ff0000">{{ ($errors->has('skype_username') ? $errors->first('skype_username') : '') }}</font>
	</div>
	{{ Form::submit(trans('users.adduser'), array('class' => 'btn btn-primary')) }}
</div>
<br/>
<br/>
{{ Form::close() }}

<div class="well">
	<div class="btn-group">
		@foreach($status_btn as $sb)
		<a class="btn btn-default" href="{{ action('UserController@show', array($sb['id'])) }}">{{ $sb['name'] }} ({{ $sb['count'] }})</a>
		@endforeach
	</div>
	<br/>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="">
			<table id="tbl_example" class="table table-striped table-hover table-bordered">
				<thead>
					<tr>
						<th>{{trans('users.id')}}</th>
						<th>{{trans('users.username')}}</th>
						<th>{{trans('users.password')}}</th>
						<th>{{trans('users.skype_username')}}</th>
						<th>{{trans('users.balance')}}</th>
						<th>{{trans('users.spent')}}</th>
						<th>{{trans('users.registrationdate')}}</th>
						<th>{{trans('users.lastlogin')}}</th>
						<th>{{trans('api.key')}}</th>
						<th>{{trans('users.rates')}}</th>

						<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach ($users as $user)
					<tr>
						<td>{{$user->id}}</td>
						<td>{{ $user->username }}</td>
						<td>{{ $user->password_nohash }}</td>
						<td>{{ $user->skype_username }}</td>
						<td>{{ round($user->balance,2) }}</td>
						<td>{{ round($user->spent,2) }}</td>
						<td>{{ $user->created_at }}</td>
						<td>{{ $user->lastlogin }}</td>
						<td>{{ $api_key[$user->id]}}</td>
						<td><a href="{{ action('RateController@edit', array($user->id)) }}">edit rates</a></td>
						<td>
						<div class="btn-group">
							<button type="button" data-toggle="dropdown" class="btn btn-default btn-xs dropdown-toggle">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu dropdown-menu-right">
								<li class="pull-left">
									<a tabindex="-1" href="{{ action('UserController@edit', array($user->id)) }}">Edit details</a>
								</li>
							</ul>
							&nbsp;
						</div><a class="btn btn-danger btn-xs action_confirm" href="{{ action('UserController@destroy', array($user->id)) }}" data-token="{{ Session::getToken() }}" data-method="delete"> <i class="fa fa-trash fa-lg"></i></a>
						<?php

						$user = Sentry::findUserByID($user -> id);
						if ($user -> isActivated()) {
							echo '<a class="btn btn-warning btn-xs" href="' . action('UserController@changeStatusUser', array('id' => $user -> id, 'status' => '2')) . '">
									<i class="fa fa-close fa-lg"></i> Ban
								  </a>';
						} else {
							echo '<a class="btn btn-success btn-xs" href="' . action('UserController@changeStatusUser', array('id' => $user -> id, 'status' => '1')) . '">
									<i class="fa fa-check fa-lg"></i> Activate
								  </a>';
						}
						?>

						<a class="btn btn-info btn-xs" href="{{ action('ApiKeyGeneratorsController@generateKeyFor', array($user->id)) }}"> API Key  Generate</i></a>
						<?php
						if ($api_enable[$user -> id] == 1) {
							echo '<a class="btn btn-warning btn-xs" href="' . action('ApiKeyGeneratorsController@changeApiAccess', array('id' => $user -> id, 'status' => '0')) . '">
									<i class="fa fa-close fa-lg"></i> API Key Disable
								  </a>';
						} else {
							echo '<a class="btn btn-success btn-xs" href="' . action('ApiKeyGeneratorsController@changeApiAccess', array('id' => $user -> id, 'status' => '1')) . '">
									<i class="fa fa-check fa-lg"></i> API Key Enable
								  </a>';
						}
						?></td>

					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

