
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Users</h3>
    </div>
    <div class="panel-body">
        Users Statistics
    </div>

    <table id="tbl_example2" class="table table-striped table-hover table-bordered">
        <tbody>
        @foreach ($datas_users as $key => $data)
        <tr>
            <td>{{ $data['name'] }}</td>
            <td style="width: 20%;">{{ $data['value'] }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>

<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title">Orders</h3>
    </div>
    <div class="panel-body">
        Orders Statistics
    </div>

    <table id="tbl_example2" class="table table-striped table-hover table-bordered">
        <tbody>
        @foreach ($datas_orders as $key => $data)
        <tr>
            <td>{{ $data['name'] }}</td>
            <td style="width: 20%;">{{ $data['value'] }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>

<div class="panel panel-success">
    <div class="panel-heading">
        <h3 class="panel-title">Payments</h3>
    </div>
    <div class="panel-body">
        Payments Statistics
    </div>

    <table id="tbl_example2" class="table table-striped table-hover table-bordered">
        <tbody>
        @foreach ($datas_payments as $key => $data)
        <tr>
            <td>{{ $data['name'] }}</td>
            <td style="width: 20%;">{{ $data['value'] }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>


<div class="panel panel-warning">
    <div class="panel-heading">
        <h3 class="panel-title">Spendings</h3>
    </div>
    <div class="panel-body">
        Spendings Statistics
    </div>

    <table id="tbl_example2" class="table table-striped table-hover table-bordered">
        <tbody>
        @foreach ($datas_spent as $key => $data)
        <tr>
            <td>{{ $data['name'] }}</td>
            <td style="width: 20%;">{{ $data['value'] }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>


<div class="panel panel-danger">
    <div class="panel-heading">
        <h3 class="panel-title">Service Cost</h3>
    </div>
    <div class="panel-body">
        Service Cost Spending Statistics
    </div>

    <table id="tbl_example2" class="table table-striped table-hover table-bordered">
        <tbody>
        @foreach ($datas_service_cost as $key => $data)
        <tr>
            <td>{{ $data['name'] }}</td>
            <td style="width: 20%;">{{ $data['value'] }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
