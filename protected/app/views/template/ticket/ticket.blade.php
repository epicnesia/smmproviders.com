
@if(!Sentry::getUser()->hasAccess('admin'))
<div class="buttonOnTop">
    <a class="btn btn-primary" href="{{ URL::to('/ticket/create') }}"> <i class="fa fa-plus fa-lg"></i>{{trans('pages.create')}}&nbsp{{trans('pages.ticket')}}</a>    
</div>
<br />
@endif
<div class="row">
    <div class="col-md-12">
        <div >
            <table id="tbl_example" class="table table-striped table-hover table-bticketed">
                <thead>
                    <tr>
                        <th>{{trans('users.id')}}</th>
                        <th>{{trans('orders.title')}}</th> 
                        <th>{{trans('tiket.createdby')}}</th>                       
                        <th>{{trans('tiket.lastrepliedby')}}</th>
                        <th>{{trans('orders.status')}}</th>                        
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($tickets as $key => $ticket)
                    <tr>                        
                        <td>{{$ticket['id']}}</td>
                        <td><a href="{{ action('TicketController@show', $ticket['id']) }}">{{$ticket['title']}}</a></td>
                		<td>{{$ticket['created_by']}}</td>
                		<td>{{$ticket['lastrepliedby']}}</td>
                		<td>{{$ticket['status']}} <span class="label label-danger">{{ $ticket['unread']==1? 'New':'' }}</span></td>
                        <td><a href="{{ action('TicketController@show', $ticket['id']) }}">{{trans('tiket.reply')}}</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

