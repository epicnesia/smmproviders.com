<h2>Title: {{ $ticketku->title; }}</h2>
<h3>Status: {{ $status; }}</h3>
<div class="row">
    <table class="table table-striped table-hover table-bordered">
        <thead>
            <tr>
                <th>User</th>
                <th>Content</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($tickets as $key => $ticket)
            <tr>
                <td>{{$ticket['username']}}</td>
                <td>{{$ticket['content']}}</td>
            </tr>
            @endforeach
            <tr>
                <td>{{$username}}</td>
                <td>
                <div class="well">
                    {{ Form::open(array(
                    'action' => array('TicketController@update' , $ticketku->id) ,
                    'method' => 'put',
                    'class' => 'form-horizontal',
                    'role' => 'form'
                    )) }}
                    {{ Form::hidden('ticket_id', $ticketku->id) }}
                    {{ Form::hidden('judul', $ticketku->title) }}
                    {{ Form::hidden('user_id', $user_ticket_id) }}
                    <div class="form-group {{ ($errors->has('isi')) ? 'has-error' : '' }}" >
                        {{ Form::textarea('isi', Input::old('isi'), array('class'=>'form-control', 'placeholder'=>'Content', 'required', 'style'=>'height:50px')) }}
                        {{ ($errors->has('isi') ? $errors->first('isi') : '') }}
                    </div>
                    <div class="form-group">
                        {{ Form::submit(trans('tiket.reply'), array('class' => 'btn btn-primary')) }}
                    </div>
                    {{ Form::close() }}
                </div></td>
            </tr>
        </tbody>
    </table>
</div>
