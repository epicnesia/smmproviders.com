<h2>{{trans('pages.create')}} {{trans('pages.ticket') }}</h2>
<div class="well">
    {{ Form::open(array('action' => 'TicketController@store', 'class' => 'form-horizontal', 'role' => 'form')) }}
    
    <div class="form-group {{ ($errors->has('judul')) ? 'has-error' : '' }}">
        {{ Form::label('judul', trans('orders.title'), array('class' => 'col-sm-2 control-label')) }}
        <div class="col-sm-10">
            {{ Form::text('judul', Input::old('judul'), array('class'=>'form-control', 'id'=>'judul', 'placeholder'=>'Title', 'required')) }}
        </div>
        <div class="col-sm-offset-2 col-sm-10">
            {{ ($errors->has('judul') ? $errors->first('judul') : '') }}
        </div>
    </div>

    <div class="form-group {{ ($errors->has('isi')) ? 'has-error' : '' }}" >
        {{ Form::label('isi', trans('orders.content'), array('class' => 'col-sm-2 control-label')) }}
        <div class="col-sm-10">
            {{ Form::textarea('isi', Input::old('isi'), array('class'=>'form-control', 'placeholder'=>'Content', 'required', 'style'=>'height:50px')) }}
        </div>
        <div class="col-sm-offset-2 col-sm-10">
            {{ ($errors->has('isi') ? $errors->first('isi') : '') }}
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            {{ Form::submit(trans('pages.actionsave'), array('class' => 'btn btn-primary')) }}
        </div>
    </div>

    {{ Form::close() }}
</div>
