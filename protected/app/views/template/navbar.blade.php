<?php 
$tickets_count = 0;
if(Sentry::check()){
$user = Sentry::getUser();

if($user->hasAccess('admin')){          
    $tickets_count = TicketUnread::where('user_id', '=', 0)->count();
}else{          
    $tickets_count = TicketUnread::where('user_id', '=', $user->id)->count();
}
}
 ?>                
                        <div class="container-fluid">
                                <div class="navbar-header">
                                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                  </button>
                                  <a class="navbar-brand" href="{{ URL::to('/') }}">{{ trans('pages.usermanagement') }}</a>
                                </div>
                                <div class="collapse navbar-collapse">
                                  <ul class="nav navbar-nav">
                                        @if(Session::has('menus'))
                                        @foreach (Session::get('menus') as $key => $menu)
                                                @if($menu['status'] == 1)
                                                        <li>                                                                
                                                                <a href="{{ URL::to($menu['route']) }}">{{ $menu['name'] }}
                                                                    @if($menu['name']=='Ticket')
                                                                    <span class="label label-danger label-as-badge" >{{$tickets_count}}</span>
                                                                    @endif
                                                                </a>    
                                                        </li>
                                                @endif
                                        @endforeach
                                        @endif
                                  </ul>
                                  <ul class="nav navbar-nav navbar-right">
                                          @if(Sentry::check())
                                        <li>
                                                <a href="{{ URL::to('/logout') }}"> Logout</a>
                                        </li>
                                        @endif
                                  </ul>
                                </div><!--/.nav-collapse -->
                        </div>
                