<h2>{{trans('pages.edit')}} {{trans('pages.page') }} {{ $pagename }}</h2>
<div class="well">
    {{ Form::open(array('action' => 'PageController@store', 'class' => 'form-horizontal', 'role' => 'form')) }}

    <div class="form-group {{ ($errors->has('editor1')) ? 'has-error' : '' }}" >
        {{ Form::label('editor1', trans('orders.content'), array('class' => 'col-sm-2 control-label')) }}
        <div class="col-sm-10">
            <textarea id="editor1" name="editor1" style="width: 100%;height: 200px;" ><p style="font-size:20px">
			Maximal order for one page:
			<br />
			How to place new order:
			<br />
			How to fill the field Link depending on the type of service:
			<br />
			What is Partially completed status:
			</p></textarea>
        </div>
        <div class="col-sm-offset-2 col-sm-10">
            {{ ($errors->has('editor1') ? $errors->first('editor1') : '') }}
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            {{ Form::submit(trans('pages.actionsave'), array('class' => 'btn btn-primary')) }}
        </div>
    </div>

    {{ Form::close() }}
</div>
