{{ Form::open(array('action' => 'UserController@register', 'class' => 'form-horizontal col-lg-6')) }}  
<h3>User Registration</h3>          
    <div class="form-group {{ ($errors->has('username')) ? 'has-error' : '' }}">
        <div class="col-lg-12">
            {{ Form::text('username', Input::old('username'), array('class' => 'form-control', 'placeholder' => trans('users.username'), 'autofocus')) }}
            {{ ($errors->has('username') ? $errors->first('username') : '') }}
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-12">                        
            <input type="password" name="password" class="form-control" placeholder="{{trans('users.pword')}}" />
            <input type="hidden" name="password_confirmation" />            
            {{ ($errors->has('password') ?  $errors->first('password') : '') }}
        </div>
    </div>
    <div class="form-group {{ ($errors->has('email')) ? 'has-error' : '' }}">
        <div class="col-lg-12">
            {{ Form::text('email', Input::old('email'), array('class' => 'form-control', 'placeholder' => trans('users.email'))) }}
            {{ ($errors->has('email') ? $errors->first('email') : '') }}
        </div>
    </div>
    <div class="form-group {{ ($errors->has('country')) ? 'has-error' : '' }}">
        <div class="col-lg-12">
            {{ Form::text('country', Input::old('country'), array('class' => 'form-control', 'placeholder' => trans('users.country'))) }}
            {{ ($errors->has('country') ? $errors->first('country') : '') }}
        </div>
    </div>                
    <div class="form-group">
        <div class="col-lg-6">
            {{ Form::submit(trans('pages.signup'), array('class' => 'btn btn-primary'))}}
            {{-- <a class="btn btn-link" href="{{ route('forgotPasswordForm') }}">{{trans('users.forgot')}}?</a> --}}
        </div>
        <div class="col-lg-6">
            <label class="checkbox" style="margin-left:20px"> {{ Form::checkbox('rememberMe', 1, $rememberMe_old) }} {{trans('users.remember')}}? </label>
        </div>
    </div>
{{ Form::close() }}