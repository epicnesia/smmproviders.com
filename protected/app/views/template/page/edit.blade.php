<h2>{{trans('pages.edit')}} {{trans('pages.page') }} {{ isset($page)?$page->title:'' }}</h2>
<div class="well">
    
    {{ Form::open(array(
                    'action' => array('PageController@update' , $page->id) ,
                    'method' => 'put',
                    'class' => 'form-horizontal',
                    'role' => 'form'
                    )) }}
	<div class="form-group {{ ($errors->has('judul')) ? 'has-error' : '' }}">
        {{ Form::label('judul', trans('orders.title'), array('class' => 'col-sm-2 control-label')) }}
        <div class="col-sm-10">
            {{ Form::text('judul', isset($page)?$page->title:'' , array('class'=>'form-control', 'id'=>'judul', 'placeholder'=>'Title', 'required')) }}
        </div>
        <div class="col-sm-offset-2 col-sm-10">
            {{ ($errors->has('judul') ? $errors->first('judul') : '') }}
        </div>
    </div>
    <div class="form-group {{ ($errors->has('editor1')) ? 'has-error' : '' }}" >
        {{ Form::label('editor1', trans('orders.content'), array('class' => 'col-sm-2 control-label')) }}
        <div class="col-sm-10">
            <textarea id="editor1" name="editor1" style="width: 100%;height: 200px;" >{{ isset($page)?$page->content:'' }}</textarea>
        </div>
        <div class="col-sm-offset-2 col-sm-10">
            {{ ($errors->has('editor1') ? $errors->first('editor1') : '') }}
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            {{ Form::submit(trans('pages.actionsave'), array('class' => 'btn btn-primary')) }}
        </div>
    </div>

    {{ Form::close() }}
</div>
