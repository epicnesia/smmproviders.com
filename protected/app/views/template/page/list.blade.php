<div class="row">
    <div class="col-md-12">
        <div >
            <table id="tbl_example" class="table table-striped table-hover table-bpageed">
                <thead>
                    <tr>
                        <th>{{trans('users.id')}}</th>
                        <th>{{trans('orders.title')}}</th>                        
                        <th>{{trans('orders.status')}}</th>                        
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($pages as $page)
                    <tr>                        
                        <td>{{$page->id}}</td>
                        <td>{{$page->title}}</td>
                		<td>{{$status[$page->status]}}</td>                		
                        <td><a href="{{ action('PageController@edit', $page->id) }}">Edit</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

