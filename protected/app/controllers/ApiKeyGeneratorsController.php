<?php

class ApiKeyGeneratorsController extends \BaseController {

        /**
         * Constructor
         */
        public function __construct() {
            $this->beforeFilter('Sentry', array('except' => 'login'));
        }
        
        public function getApiKey($user_id) {
        
            $apis = array();
            $apis = Config::get('app.api_access');
            
            $user_key = '';            
            foreach ($apis as $key => $value) {
                if($user_id == $value){
                    $user_key = $key;
                    break;
                }
            }
            
            if($user_key == ''){
                if($api_gen = ApiKeyGenerator::where('user_id','=', $user_id)->first()){
                    $user_key = $api_gen->api_key;
                }
            }
            
            return  $user_key;
        }

        /**
         * Display a listing of the resource.
         * GET /apikeygenerators
         *
         * @return Response
         */
        public function index() {

                $user = Sentry::getUser();
                $users = User::all();
                foreach ($users as $key) {
                        $api_key[$key -> id] = $this->getApiKey($key->id);
                        $api_enable[$key -> id] = 1;
                        if($api_key[$key -> id]==''){
                           if($api = ApiKeyGenerator::where('user_id', '=', $key -> id) -> first()){
                                $api_enable[$key -> id] = $api->api_enable;   
                           }   
                        }                                                
                }

                $key = sha1(md5(time()));

                if ($user -> hasAccess('admin')) {
                        return View::make('views.admin.api_key_generator') -> with('users', $users) -> with('api_key', $api_key) -> with('api_enable', $api_enable);
                } else {

                        $api = ApiKeyGenerator::where('user_id', '=', $user -> id) -> first();

                        $api_key_string = $this->getApiKey($user->id); 
                        
                        return View::make('views.user.api_key_generator') -> with('user', $user) 
                        -> with('api_key', $api)
                        -> with('api_key_string', $api_key_string);

                }

        }

        /**
         * Show the form for creating a new resource.
         * GET /apikeygenerators/create
         *
         * @return Response
         */
        public function create() {
                //
        }

        /**
         * Store a newly created resource in storage.
         * POST /apikeygenerators
         *
         * @return Response
         */
        public function store() {
                //
        }

        /**
         * Display the specified resource.
         * GET /apikeygenerators/{id}
         *
         * @param  int  $id
         * @return Response
         */
        public function show($id) {
                //
        }

        /**
         * Show the form for editing the specified resource.
         * GET /apikeygenerators/{id}/edit
         *
         * @param  int  $id
         * @return Response
         */
        public function edit($id) {
                //
        }

        /**
         * Update the specified resource in storage.
         * PUT /apikeygenerators/{id}
         *
         * @param  int  $id
         * @return Response
         */
        public function update($id) {
                //
        }

        /**
         * Remove the specified resource from storage.
         * DELETE /apikeygenerators/{id}
         *
         * @param  int  $id
         * @return Response
         */
        public function destroy($id) {
                //
        }

        /**
         * User's API Key Generator
         *
         * @return Response
         *
         */
        public function generateKey() {

                try {
                        DB::beginTransaction();

                        $user = Sentry::getUser();
                        $api_key = ApiKeyGenerator::where('user_id', '=', $user -> id) -> first();
                        $key = sha1(md5(time()));

                        $check_key = ApiKeyGenerator::where('api_key', '=', $key) -> count();

                        if ($check_key > 0) {
                                $key = sha1(md5(time() . rand(0, 9999)));
                        }

                        if (is_null($api_key)) {

                                $proc = ApiKeyGenerator::create(array('user_id' => $user -> id, 'api_key' => $key));

                        } else {
                                $proc = ApiKeyGenerator::where('user_id', '=', $user -> id) -> update(array('api_key' => $key));
                        }

                        if (!$proc) {
                                DB::rollback;
                                throw new Exception(trans('api.systemError'));
                        } else {
                                DB::commit();
                                Session::flash('success', trans('api.generateKeySuccess'));
                                return Redirect::to('api/key');
                        }

                } catch (Exception $e) {
                        DB::rollback();

                        $result['message'] = '(' . $e -> getLine() . ') ' . $e -> getMessage();
                        Session::flash('error', $result['message']);
                        return Redirect::to('api/key');
                }

        }

        /**
         * Admin's API Key Generator
         * Generating API Key for user
         *
         * @param  int  $id
         * @return Response
         *
         */
        public function generateKeyFor($id) {

                try {
                        DB::beginTransaction();

                        $user = Sentry::getUser();

                        if (!$user -> hasAccess('admin')) {
                                DB::rollback;
                                throw new Exception(trans('api.notAllowedUser'));
                        }

                        $api_key = ApiKeyGenerator::where('user_id', '=', $id) -> first();
                        $key = sha1(md5(time()));

                        $check_key = ApiKeyGenerator::where('api_key', '=', $key) -> count();

                        if ($check_key > 0) {
                                $key = sha1(md5(time() . rand(0, 9999)));
                        }

                        if (is_null($api_key)) {

                                $proc = ApiKeyGenerator::create(array('user_id' => $id, 'api_key' => $key));

                        } else {
                                $proc = ApiKeyGenerator::where('user_id', '=', $id) -> update(array('api_key' => $key));
                        }

                        if (!$proc) {
                                DB::rollback;
                                throw new Exception(trans('api.systemError'));
                        } else {
                                DB::commit();
                                Session::flash('success', trans('api.generateKeySuccess'));
                                return Redirect::to('admin/users');
                        }

                } catch (Exception $e) {
                        DB::rollback();

                        $result['message'] = '(' . $e -> getLine() . ') ' . $e -> getMessage();
                        Session::flash('error', $result['message']);
                        return Redirect::to('admin/users');
                }
        }

        /**
         * Admin's API Key Generator
         * Change API Access (enabling or disabling) of user by ID
         *
         * @param  int  $id
         * @param int $status
         * @return Response
         *
         */
        public function changeApiAccess($id, $status) {

                if($api_key = ApiKeyGenerator::where('user_id','=',$id)->first()){
                    $api_key->api_enable = $status;
                    $api_key->save();    
                }

                return Redirect::to('admin/users');

        }

}
