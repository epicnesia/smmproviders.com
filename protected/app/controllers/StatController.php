<?php
use Jenssegers\Date;

class StatController extends \BaseController
{
	
	public function __construct() {
		 $this->beforeFilter('Sentry', array('except' => 'login'));
	}

    /**
     * Display a listing of the resource.
     * GET /stat
     *
     * @return Response
     */
    public function index()
    {
        if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }
		
        if (!Sentry::getUser()->hasAccess('admin')) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }

        //misal : sekarang : rabu, 21 januari jam 13:00
        //interval today (1 hari ini) : dari 21 januari jam 00:00 sampe 21 januari jam 23:59
        $todayStart = \Jenssegers\Date\Date::today();
        $todayEnd = \Jenssegers\Date\Date::tomorrow()->sub('1 second');

        //interval this week (1 minggu ini) dari 19 januari jam 00:00 sampai 25 januari 23:59
        $weekStart = \Jenssegers\Date\Date::parse('monday this week');
        $weekEnd = \Jenssegers\Date\Date::parse('monday next week')->sub('1 second');

        //interval this month (1 bulan ini) dari 1 januari jam 00:00 sampai 31 januari 23:59 (tanggal 31, mengacu pada bulan, misal april maka sampai 30 april 23:59)
        $monthStart = \Jenssegers\Date\Date::parse('midnight first day of this month');
        $monthEnd = \Jenssegers\Date\Date::parse('midnight first day of next month')->sub('1 second');

        //total users: select count users
        $user_total = User::count();

        //new users today: select count users where created_at (dalam waktu 1 hari ini)
        $user_new_today = User::whereRaw("created_at between '" . $todayStart . "' and '" . $todayEnd . "'")->count();

        //new users this week: select count users where created_at (dalam waktu 1 minggu ini)
        $user_new_this_week = User::whereRaw("created_at between '" . $weekStart . "' and '" . $weekEnd . "'")->count();

        //total payments today: select sum ammount from payment where created_at (dalam waktu 1 hari ini)
        $payments_total_today = Payment::whereRaw("created_at between '" . $todayStart . "' and '" . $todayEnd . "'")->sum('amount');

        //total payment this week: select sum ammount from payment where created_at (dalam waktu 1 minggu ini)
        $payments_total_this_week = Payment::whereRaw("created_at between '" . $weekStart . "' and '" . $weekEnd . "'")->sum('amount');

        //total payments this month: select sum ammount from payment where created_at (dalam waktu 1 month ini)
        $payments_total_this_month = Payment::whereRaw("created_at between '" . $monthStart . "' and '" . $monthEnd . "'")->sum('amount');

        //total payments: select sum ammount from payment
        $payments_total = Payment::sum('amount');

        //total money spent today: select sum charge from orders where created_at (dalam waktu 1 hari ini)
        $spent_total_today = Order::whereRaw("created_at between '" . $todayStart . "' and '" . $todayEnd . "'")->sum('charge');

        //total money spent this week: select sum charge from orders where created_at (dalam waktu 1 minggu ini)
        $spent_total_this_week = Order::whereRaw("created_at between '" . $weekStart . "' and '" . $weekEnd . "'")->sum('charge');

        //total money spent this month: select sum charge from orders where created_at (dalam waktu 1 bulan ini)
        $spent_total_this_month = Order::whereRaw("created_at between '" . $monthStart . "' and '" . $monthEnd . "'")->sum('charge');

        //total money spent: select sum charge from orders
        $spent_total = Order::sum('charge');

        //total orders today: select count orders where created_at (dalam waktu 1 hari ini)
        $order_total_today = Order::whereRaw("created_at between '" . $todayStart . "' and '" . $todayEnd . "'")->count();

        //total orders this week: select count orders where created_at (dalam waktu 1 minggu ini)
        $order_total_this_week = Order::whereRaw("created_at between '" . $weekStart . "' and '" . $weekEnd . "'")->count();

        //total orders this month: select count orders where created_at (dalam waktu 1 bulan ini)
        $order_total_this_month = Order::whereRaw("created_at between '" . $monthStart . "' and '" . $monthEnd . "'")->count();

        //total orders: select count orders
        $order_total = Order::count();

        //total service cost spent today: select sum charge from orders where created_at (dalam waktu 1 hari ini)
        $cost_total_today = Order::whereRaw("created_at between '" . $todayStart . "' and '" . $todayEnd . "'")->sum('service_cost');

        //total service cost spent this week: select sum charge from orders where created_at (dalam waktu 1 minggu ini)
        $cost_total_this_week = Order::whereRaw("created_at between '" . $weekStart . "' and '" . $weekEnd . "'")->sum('service_cost');

        //total service cost spent this month: select sum charge from orders where created_at (dalam waktu 1 bulan ini)
        $cost_total_this_month = Order::whereRaw("created_at between '" . $monthStart . "' and '" . $monthEnd . "'")->sum('service_cost');

        //total service cost spent: select sum charge from orders
        $cost_total = Order::sum('service_cost');

        // array
        $arr_data = array();
        $arr_data['orders']['order_total_today'] = array(
            'id'    => 12,
            'name'  => 'Total orders today',
            'value' => $order_total_today
        );
        $arr_data['orders']['order_total_this_week'] = array(
            'id'    => 13,
            'name'  => 'Total orders this week',
            'value' => $order_total_this_week
        );

        $arr_data['orders']['order_total_this_month'] = array(
            'id'    => 14,
            'name'  => 'Total orders this month',
            'value' => $order_total_this_month
        );
        $arr_data['orders']['order_total'] = array(
            'id'    => 15,
            'name'  => 'Total orders',
            'value' => $order_total
        );


        $arr_data['spent']['spent_total_today'] = array(
            'id'    => 8,
            'name'  => 'Total money spent today',
            'value' => $spent_total_today
        );
        $arr_data['spent']['spent_total_this_week'] = array(
            'id'    => 9,
            'name'  => 'Total money spent this week',
            'value' => $spent_total_this_week
        );
        $arr_data['spent']['spent_total_this_month'] = array(
            'id'    => 10,
            'name'  => 'Total money spent this month',
            'value' => $spent_total_this_month
        );
        $arr_data['spent']['spent_total'] = array(
            'id'    => 11,
            'name'  => 'Total money spent',
            'value' => $spent_total
        );


        $arr_data['users']['user_new_today'] = array(
            'id'    => 2,
            'name'  => 'New users today',
            'value' => $user_new_today
        );
        $arr_data['users']['user_new_this_week'] = array(
            'id'    => 3,
            'name'  => 'New users this week',
            'value' => $user_new_this_week
        );
        $arr_data['users']['user_total'] = array(
            'id'    => 1,
            'name'  => 'Total users',
            'value' => $user_total
        );


        $arr_data['payments']['payments_total_today'] = array(
            'id'    => 4,
            'name'  => 'Total payments today',
            'value' => $payments_total_today
        );

        $arr_data['payments']['payments_total_this_week'] = array(
            'id'    => 5,
            'name'  => 'Total payment this week',
            'value' => $payments_total_this_week
        );

        $arr_data['payments']['payments_total_this_month'] = array(
            'id'    => 6,
            'name'  => 'Total payment this month',
            'value' => $payments_total_this_month
        );

        $arr_data['payments']['payments_total'] = array(
            'id'    => 7,
            'name'  => 'Total payment',
            'value' => $payments_total
        );


        $arr_data['cost']['cost_total_today'] = array(
            'id'    => 16,
            'name'  => 'Total service cost spent today',
            'value' => $cost_total_today
        );
        $arr_data['cost']['cost_total_this_week'] = array(
            'id'    => 17,
            'name'  => 'Total service cost spent this week',
            'value' => $cost_total_this_week
        );
        $arr_data['cost']['cost_total_this_month'] = array(
            'id'    => 18,
            'name'  => 'Total service cost spent this month',
            'value' => $cost_total_this_month
        );
        $arr_data['cost']['cost_total'] = array(
            'id'    => 19,
            'name'  => 'Total service cost spent',
            'value' => $cost_total
        );


        // view
        return View::make('views.admin.stat')
            ->with('datas_users', $arr_data['users'])
            ->with('datas_spent', $arr_data['spent'])
            ->with('datas_payments', $arr_data['payments'])
            ->with('datas_orders', $arr_data['orders'])
            ->with('datas_service_cost', $arr_data['cost']);
    }

	public function userStat()
    {
        if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }
		
		$user = Sentry::getUser();
		
		$totalOrder 		= Order::where('seller_id', '=', $user->id)->count();
		$totalPending		= Order::where('seller_id', '=', $user->id)->where('status', '=', 0)->count();
		$totalProcessing	= Order::where('seller_id', '=', $user->id)->where('status', '=', 1)->count();
		$totalCompleted		= Order::where('seller_id', '=', $user->id)->where('status', '=', 2)->count();
		
		return View::make('views.user.statistic')
					->with('user', $user)
					->with('totalOrder', $totalOrder)
					->with('totalPending', $totalPending)
					->with('totalProcessing', $totalProcessing)
					->with('totalCompleted', $totalCompleted);
	}

    /**
     * Show the form for creating a new resource.
     * GET /stat/create
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * POST /stat
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     * GET /stat/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /stat/{id}/edit
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * PUT /stat/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /stat/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}