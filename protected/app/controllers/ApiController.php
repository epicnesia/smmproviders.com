<?php

class ApiController extends \BaseController {

    public function __construct() {
       
    }
    /**
     * Display a listing of the resource.
     * GET /api
     *
     * @return Response
     */
    public function index() {
        $result = array('result' => 'error', 'id' => 0, 'message' => 'Invalid user api key');

        if (!Input::has('key')) {
            $result['message'] = 'Invalid user api key';
            return json_encode($result);
        }

        $key_client = Input::get('key');

        $apis = array();
        $apis = Config::get('app.api_access');
       
        foreach ($apis as $key => $value) {
            if ($key == $key_client) {
                
                if(!$user = User::find($value)){
                    $result['message'] = 'User api not found';
                    return json_encode($result);
                }
                
                if(!$user->activated){
                    $result['message'] = 'User api is not active';
                    return json_encode($result);
                }   
                             
                $result = array(
                    'result' => 'success',
                    'message' => 'api key is valid',
                    'id' => $value,
                );
                                
                return json_encode($result);
            }
        }
        
        if($api_gen = ApiKeyGenerator::where('api_enable','=',1) -> where('api_key','=', $key_client)->first()){
            
            $result = array(
                    'result' => 'success',
                    'message' => 'api key is valid',
                    'id' => $api_gen->user_id,
                );
                
            return json_encode($result);
        }
        
        $result['message'] = 'User api not found';
        return json_encode($result);
    }
    
    /**
     * Store a newly created resource in storage.
     * POST /api
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     * GET /api/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function show($access) {
        return json_encode(Sentry::findAllUsersWithAccess(array($access)));
    }
    
    /**
     * Display the specified resource.
     * GET /api/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function showapi() {
        if(!Sentry::check()){
            return Redirect::to('/');
        }
               
        $apis = array();
        $apis = Config::get('app.api_access');
        
        $user_key = '';
        $user_id = Sentry::getUser()->id;
        foreach ($apis as $key => $value) {
            if($user_id == $value){
                $user_key = $key;
                break;
            }
        }
        
        if($user_key == ''){
            if($api_gen = ApiKeyGenerator::where('api_enable','=',1) -> where('user_id','=', $user_id)->first()){
                $user_key = $api_gen->api_key;
            }
        }
        
        return View::make('views.user.apikey')
        ->with('user_key', $user_key);
    }
    
    /**
     * Display the specified resource.
     * GET /api/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function showexample() {
        return View::make('views.user.example');
    }
    

    /**
     * Show the form for editing the specified resource.
     * GET /api/{id}/edit
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     * PUT /api/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /api/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

}
