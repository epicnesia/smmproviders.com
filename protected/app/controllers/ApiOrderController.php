<?php

class ApiOrderController extends \BaseController
{

    private function getId($key_client)
    {
        if (!isset($key_client)) return 0;

        $apis = array();
        $apis = Config::get('app.api_access');


        foreach ($apis as $key => $value) {
            if ($key == $key_client) {
                if (!$user = User::find($value)) {
                    return 0;
                }

                if (!$user->activated) {
                    return 0;
                }

                return $value;
            }
        }

        if ($api_gen = ApiKeyGenerator::where('api_enable', '=', 1)->where('api_key', '=', $key_client)->first()) {
            return $api_gen->user_id;
        }

        return 0;
    }

    /**
     * Display a listing of the resource.
     * GET /apiorder
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     * GET /apiorder/create
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * POST /apiorder
     *
     * @return Response
     */
    public function store()
    {
        $result = array('result' => 'error', 'id' => 0, 'message' => 'none');

        try {
            file_put_contents('/tmp/reseller-api.log', json_encode(Input::except('custom_list')) . PHP_EOL, FILE_APPEND);
            DB::beginTransaction();

            //cek api key
            if (!$user_id_form_api_key = $this->getId(Input::get('api_key'))) {
                DB::rollback();
                $result['message'] = 'New Order cannot be saved';

                return json_encode($result);
            }

            $service = Services::find(Input::get('services'));

            if (!$service) {
                throw new Exception('Service with id ' . Input::get('services') . ' Not Exist on our system');
            }

            if ($service->status != 1) {
                throw new Exception('Service ' . $service->name . ' disabled');
            }

            $pos = strpos($service->name, 'Comments');

            if ($pos == true) {
                $validator = Validator::make(
                    Input::all(), array(
                        'services' => 'required|integer|min:1',
                        'link'     => 'required|min:4',
                        'comments' => 'required|min:1'
                    )
                );

                $qty = count(explode("\n", trim(Input::get('comments'))));
                if (!$qty) {

                    DB::rollback();
                    $result['message'] = 'Please Insert Comments';

                    return json_encode($result);
                }
            } elseif (($service->name == 'Instagram Auto Likes') || ($service->name == 'Instagram Views')) {
                $validator = Validator::make(
                    Input::all(), array(
                        'services' => 'required|integer|min:1',
                        'link'     => 'required|min:4',
                        'quantity' => 'required|numeric|min:1'
                    )
                );

                $qty = Input::get('quantity');
            } elseif ($service->name == 'Instagram Mentions Custom List') {
                $validator = Validator::make(
                    Input::all(), array(
                        'services' => 'required|integer|min:1',
                        'link'     => 'required|min:4',
                        'custom_list'     => 'required|min:1'
                    )
                );
				$qty = 0;
				$c_list = '';
					
				$lists = explode("\n", Input::get('custom_list'));
				$uLists= array_unique($lists);
				$validList = array();
				foreach ($uLists as $value) {
					if($value && !strpos($value, " ")){
						$validList[$qty] = $value;
						$qty++;
					}
				}
				
				$c_list = trim(implode(PHP_EOL, $validList));
					
	         	if ($qty == 0) {
	            	DB::rollback();
	             	$result['message'] = 'Please Insert Custom List!';

                	return json_encode($result);
	       		}

            } elseif ($service->name == 'Instagram User Mentions') {
                $validator = Validator::make(
                    Input::all(), array(
                        'services' => 'required|integer|min:1',
                        'instagram_username' => 'required|min:4',
                        'quantity' => 'required|numeric|min:1'
                    )
                );

                $qty = Input::get('quantity');

            } else {
                $validator = Validator::make(
                    Input::all(), array(
                        'services' => 'required|integer|min:1',
                        'link'     => 'required|min:4',
                        'quantity' => 'required|numeric|min:1'
                    )
                );

                $qty = Input::get('quantity');
            }

            if ($validator->fails()) {

                DB::rollback();
                $result['message'] = 'All input form must be required!!!';

                return json_encode($result);
            }

            $data = array();
            $data = Input::all();
            $service_id = $data['services'];

            if (!$user_data = User::find($user_id_form_api_key)) {
                DB::rollback();
                $result['message'] = 'User not found';

                return json_encode($result);
            }

            $price = $service->price;
            $rate = Rate::where('user_id', '=', $user_data->id)->where('service_id', '=', $service_id)->first();
            if (count($rate) > 0) {
                $price = $rate->rates;
            }

			$max_amount = $service->max_amount;
			if($qty > $max_amount) {
				throw new Exception('Sorry, Your quantity exceeds the maximum amount of orders!!!');
			}

            $order_prs = 1000 / $qty;
            $charge = $price / $order_prs;

            if ($user_data->balance < $charge OR is_null($user_data->balance)) {
                DB::rollback();
                $result['message'] = 'Sorry, Admin Balance Not Enough';

                return json_encode($result);
            }

            $typesArray1 = array('Instagram Auto Likes');
            if (in_array($service->name, $typesArray1)) {

                if (!self::isInactiveOrder($service_id, $data['link'])) {
                    DB::rollback();
                    $result['message'] = 'Sorry, the order with same service and instagram username  already exist and still active on smmproviders.com';

                    return json_encode($result);
                }
            }

            $typesArray2 = array('Instagram Mentions', 'Instagram User Mentions');
            if (in_array($service->name, $typesArray2)) {

                $hashtags_links = count(explode("\n", trim($data['hashtags'])));
                if($hashtags_links < 2){
                    DB::rollback();
                    $result['message'] = ': Sorry, hashtags must be 2 or more!';

                    return json_encode($result);
                }

            }

            $url = Config::get('app.ipanel_add');

            $curlData = array();
            $curlData['Middle East Instagram Likes'] = array(
                'instagram_link' => $data['link'],
                'likes_to_get'   => $qty,
                'private_key' => Config::get('app.private_key')
            );
			
			$curlData['Instagram Views'] = array(
                'instagram_link' => $data['link'],
                'views_to_get'   => $qty,
                'private_key' => Config::get('app.private_key')
            );
			
			$pos = strpos($service->name, 'Comments');
			$pos1 = strpos($service->name, 'Mentions');
            if ($pos == true) {
            	$curlData['Middle East Instagram Comments'] = array(
	                'instagram_link' => $data['link'],
	                'comments'       => $data['comments'],
                    'private_key' => Config::get('app.private_key')
	            );
				
				$curlData['Instagram High Quality Comments'] = array(
	                'instagram_link' => $data['link'],
	                'comments'       => $data['comments'],
                    'private_key' => Config::get('app.private_key')
	            );
			}	

            $curlData['Middle East Instagram Followers'] = array(
                'name'  => $this->getInstaUsername($data['link']),
                'limit' => $qty,
                'private_key' => Config::get('app.private_key')
            );

            $curlData['Instagram High Quality Likes'] = array(
                'instagram_link' => $data['link'],
                'likes_to_get'   => $qty,
                'private_key' => Config::get('app.private_key')
            );

            $curlData['Instagram High Quality Followers'] = array(
                'name'  => $this->getInstaUsername($data['link']),
                'limit' => $qty,
                'private_key' => Config::get('app.private_key')
            );

            $newDate = strtotime('+30 days', time());
            $curlData['Instagram Auto Likes'] = array(
                'name'  => $this->getInstaUsername($data['link']),
                'date'  => $newDate,
                'limit' => $qty,
                'private_key' => Config::get('app.private_key')
            );
			
			if($service->id == 7){
				$curlData['Instagram Mentions'] = array(
	                'instagram_link' => $data['link'],
	                'usernames'      => (isset($data['usernames']) && $data['usernames']) ? $data['usernames'] : '',
	                'hashtags'       => (isset($data['hashtags']) && $data['hashtags']) ? $data['hashtags'] : '',
	                'limit'          => $qty,
	                'comments'       => (isset($data['comments']) && $data['comments']) ? $data['comments'] : '',
                    'private_key' => Config::get('app.private_key')
	            );	
			}elseif($service->id == 8){
				$curlData['Instagram Mentions Custom List'] = array(
	                'instagram_link' => $data['link'],
	                'custom_list'    => (isset($c_list) && $c_list) ? $c_list : '',
	                'limit'          => $qty,
	                'comments'       => (isset($data['comments']) && $data['comments']) ? $data['comments'] : '',
                    'private_key' => Config::get('app.private_key')
	            );
			}elseif($service->id == 10){
				$curlData['Instagram User Mentions'] = array(
	                'instagram_username' => $data['instagram_username'],
	                'usernames'      => (isset($data['usernames']) && $data['usernames']) ? $data['usernames'] : '',
	                'hashtags'       => (isset($data['hashtags']) && $data['hashtags']) ? $data['hashtags'] : '',
	                'limit'          => $qty,
                    'private_key' => Config::get('app.private_key')
	            );
			}

            $user_data->balance = $user_data->balance - $charge;
            $user_data->spent = $user_data->spent + $charge;

            $user_data->save();

            $order = new Order();
            $order->service_type_id = $service_id;
            $order->seller_id = $user_data->id;
            $order->link = $this->validatingLink($data['link']);
            $order->charge = $charge;
            $order->quantity = $qty;
            if ($service->name == 'Instagram Auto Likes') {
                $order->expired_date = gmdate("Y-m-d H:i:s", $newDate);
            }

            if (($order->save()) && $user_data->save()) {
                    
                if($pos == true){
                            
                    $answer = new Answer();
                    $answer->order_id = $order->id;
                    $answer->question = "Comments";
                    $answer->answer = $data['comments'];
                    
                    if(!$answer->save()) throw new Exception("Error Processing Request, please try again!", 1);
                    
                } elseif ($pos1 == true && (isset($data['comments']) && $data['comments'])) {
                	
                    $answer = new Answer();
                    $answer->order_id = $order->id;
                    $answer->question = "Mention Additional Comments";
                    $answer->answer = $data['comments'];
                    
                    if(!$answer->save()) throw new Exception("Error Processing Request, please try again!", 1);
                }
				
				$curl = new \Reseller\Api\Curl();

	            $http_get = '';
	            $http_get = $curl->simple_post($url[$service->name], $curlData[$service->name]);
	            if (strlen(trim($http_get)) == 0) {
	                DB::rollback();
	                $result['message'] = 'New Order cannot be saved, Please try again';
	
	                return json_encode($result);
	            }
	
	            $result_api = json_decode($http_get);
	
                file_put_contents('/tmp/reseller-api.log', json_encode($result_api) . PHP_EOL, FILE_APPEND);
	
	            if (!isset($result_api) OR $result_api->result == 'error') {
	                DB::rollback();
	                $result['message'] = ((isset($result_api->msg) &&  $result_api->msg) ? $result_api->msg : 'New Order cannot be saved, Please try again');
					//$result['result'] ="errorC1";
	                return json_encode($result);
	            }
	
	            $result_id = isset($result_api) ? $result_api->id : 0;
	
	            if (!$result_id) {
	                DB::rollback();
	                $result['message'] = 'New Order cannot be saved, Please try again';
	
	                return json_encode($result);
	            }
	
	            $valid_api_id = is_array($result_id) ? $result_id : array($result_id);
				
				$order->api_id = serialize($valid_api_id);
				
				$order->save();

                DB::commit();

                $result = array(
                    'result'  => 'success',
                    'message' => 'Order success',
                    'id'      => $order->id,
                );

                return json_encode($result);
            }

        } catch (Exception $e) {

            DB::rollback();

                file_put_contents('/tmp/reseller-api.log', $e->getMessage() . PHP_EOL, FILE_APPEND);
                file_put_contents('/tmp/reseller-api.log', $e->getTraceAsString() . PHP_EOL, FILE_APPEND);


            $result['message'] = $e->getMessage();
            file_put_contents('/tmp/reseller-api.log', $e->getMessage() . PHP_EOL, FILE_APPEND);
            file_put_contents('/tmp/reseller-api.log', $e->getTraceAsString() . PHP_EOL, FILE_APPEND);
            file_put_contents('/tmp/reseller-api.log', json_encode(Input::except('custom_list')) . PHP_EOL, FILE_APPEND);
            if (isset($result_api))
                file_put_contents('/tmp/reseller-api.log', json_encode($result_api) . PHP_EOL, FILE_APPEND);
            if (isset($http_get))
                file_put_contents('/tmp/reseller-api.log', $http_get . PHP_EOL, FILE_APPEND);


            return json_encode($result);
        }
    }

    public static function isInactiveOrder($service_id, $link)
    {
        if (!isset($service_id) && !isset($link)) {
            return false;
        }

        $order = Order::where('service_type_id', '=', $service_id)
            ->where('link', '=', $link)
            ->orderBy('id', 'desc')//Order yang terakhir yang di cek, karena kemungkinan order sebelumnya sudah selesai
            ->first();

        if (is_null($order)) {
            return true;
        }

        if (time() > strtotime($order->expired_date)) {
            return true;
        }

        return false;
    }

    function getInstaUsername($str)
    {
            
        if(strpos($str, "instagram.com")){
                 
                $url = substr($str, 4, 1) == "s" ? "https://instagram.com/" : "http://instagram.com/";
                         
                $name = trim(str_replace("/", "", (substr($str, strlen($url)))));
        }else{
                $name = $str;
        }

        return $name;

    }
    
    function validatingLink($str)
    {
        if(strpos($str, "instagram.com")){
                $name = $str;
        }else{
                $name = "https://instagram.com/".$str;
        }
        return $name;
    }


    /**
     * Display the specified resource.
     * GET /apiorder/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $result = array('result' => 'error', 'id' => 0, 'message' => 'none');
        try {

            //cek api key
            if (!$user_id_form_api_key = $this->getId(Input::get('api_key'))) {
                $result['message'] = 'api key invalid';

                return json_encode($result);
            }

            $order = Order::find($id);
            if (!$order)
                throw new Exception('Order with id ' . $id . ' Not Found');

            if ($order->seller_id != $user_id_form_api_key)
                throw new Exception('Order with id ' . $id . ' is not your order');

            $result = array(
                'result'  => 'success',
                'message' => 'Order Found',
                'data'    => $order,
            );

            return json_encode($result);
        } catch (Exception $e) {

            $result['message'] = $e->getMessage();

            return json_encode($result);
        }
    }

    /**
     * Display the specified resource.
     * GET /apiorder/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function status()
    {
        $result = array('result' => 'error', 'status' => 8, 'start' => 0, 'remains' => 0, 'charge' => 0, 'message' => 'Order Not Found');

        try {

            //cek api key
            if (!$user_id_form_api_key = $this->getId(Input::get('api_key'))) {
                $result['message'] = 'api key invalid';

                return json_encode($result);
            }

            $id = Input::get('id');

            $order = Order::find($id);
            if (!$order)
                throw new Exception('Order with id ' . $id . ' Not Found');

            if ($order->seller_id != $user_id_form_api_key)
                throw new Exception('Order with id ' . $id . ' is not your order');

            $result = array(
                'result'  => 'success',
                'status'  => $order->status,
                'start'   => is_null($order->start_count) ? 0 : $order->start_count,
                'remains' => $order->remains,
                'charge'  => $order->charge,
                'message' => 'Order Found',
                'desc'     => $order->desc,
            );

            return json_encode($result);
        } catch (Exception $e) {

            $result['message'] = $e->getMessage();

            return json_encode($result);
        }
    }

    /**
     * Show the form for editing the specified resource.
     * GET /apiorder/{id}/edit
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * PUT /apiorder/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /apiorder/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}