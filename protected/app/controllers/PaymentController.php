<?php

class PaymentController extends \BaseController
{
    protected $pagging;

    public function __construct()
    {
        $this->beforeFilter('Sentry', array('except' => 'successPay'));
        $this->pagging = 10;
    }

    public function check()
    {
        $payments = Payment::first();

        if (is_null($payments)) {
            return false;
        }
        if (count($payments) == 0) {
            return false;
        }

        return true;
    }

    protected function getAllData($user_id = 0)
    {
        if (!$this->check()) {
            return array();
        }
        $payments = null;
        if ($user_id == 0) {
            $payments = Payment::all();
        } else {
            $payments = Payment::where('user_id', '=', $user_id)->get();
        }
        $payments_1 = array();
        $status_id = 0;
        foreach ($payments as $payment) {
            $user = User::where('id', '=', $payment->user_id)->first();
            $user_name = '';
            if (!is_null($user)) {
                $user_name = $user->username;
            }
            $payments_1[$payment->id] = array(
                'id'             => $payment->id,
                'transaction_id' => $payment->transaction_id,
                'username'       => $user_name,
                'balance'        => $payment->balance,
                'amount'         => round($payment->amount, 2),
                'details'        => $payment->details,
                'date'           => $payment->created_at
            );
        }


        return $payments_1;
    }

    protected function getAllDataPaging($user_id = 0)
    {
        if (!$this->check()) {
            return null;
        }

        $Payments_1 = null;

        if ($user_id == 0) {
            $Payments_1 = Payment::with('one_user')->orderBy('id', 'desc')->paginate($this->pagging);
        } else {
            $user = User::find($user_id);
            $Payments_1 = $user->payments()->with('one_user')->orderBy('id', 'desc')->paginate($this->pagging);
        }

        return $Payments_1;
    }

    protected function getUserNames()
    {
        if (!$this->check()) {
            return array();
        }

        $o_users = User::orderBy('username', 'asc')
            ->get();

        $users[''] = '-- ALL --';
        foreach ($o_users as $t) {
            $users[$t['username']] = $t['username'];
        }

        return $users;
    }

    /**
     * Display a listing of the resource.
     * GET /payment
     *
     * @return Response
     */
    public function index()
    {
        $users = $this->getUserNames();

        $payments = array();
        if (Input::has('user_name')) {
            $user = User::where('username', '=', Input::get('user_name'))->first();

            $payments = $this->getAllDataPaging($user->id);
        } else {
            $payments = $this->getAllDataPaging();
        }

        return View::make('views.admin.payments')->with('payments', $payments)
            ->with('user_names', $users);
    }

    /**
     * Show the form for creating a new resource.
     * GET /payment/create
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Omnipay on user add balance
     * @return mixed
     */
    public function buyBalance()
    {
        if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        $validator = Validator::make(
            Input::all(), array(
                'amount' => 'required|numeric|min:10',
            )
        );

        if ($validator->fails()) {
            Session::flash('error', 'Amount is required, it must be numeric least 10!');

            return Redirect::to('/user/balance')->withErrors($validator->messages())->withInput();
        }

        $data = Input::all();

        $user = Sentry::getUser();

        $grandTotal = $data['amount'];
           //"2.97" harus ada titik;
        $amount = $grandTotal;
        $amount = round($amount,2);
        $item_name = $amount . " smmproviders.com Balances";    //"For 100 Likes";
        $quantity = "1";


        //**//



//        $gatewayConfig = Config::get('ignited/laravel-omnipay::default');
//        dd($gatewayConfig);
//        $gateway = Omnipay::create('PayPal_Express');
//        $gateway->setUsername(Config::get('laravel-omnipay.gateways.' . $gatewayConfig . '.options.username'));
//        $gateway->setPassword(Config::get('laravel-omnipay.gateways.' . $gatewayConfig . '.options.password'));
//        $gateway->setSignature(Config::get('laravel-omnipay.gateways.' . $gatewayConfig . '.options.signature'));
//        $gateway->setTestMode(Config::get('laravel-omnipay.gateways.' . $gatewayConfig . '.options.testMode'));

        $params = [
            'amount'     => $amount,
            'currency'   => 'USD',
            'returnUrl'  => Config::get('app.url') . '/user/balance/added',
            'cancelUrl'  => Config::get('app.url') . '/user/balance',
            'noShipping' => true,
            'items'      => array(
                array(
                    'name'        => $item_name,
                    'price'       => $amount,
                    'description' => $item_name,
                    'quantity'    => $quantity
                ),
            ),
        ];

        \Illuminate\Support\Facades\Session::put('paypal_params', $params);
        \Illuminate\Support\Facades\Session::save();

        $response = Omnipay::purchase($params)->send();

        if ($response->isSuccessful()) {

            $paymentku = Payment::create(
                array(
                    'transaction_id' => $response->getTransactionId(),
                    'user_id'        => $user->id,
                    'balance'        => $user->balance + $amount,
                    'amount'         => $amount,
                    'details'        => $item_name
                )
            );

            $user->increment('balance', $amount);

            Session::flash('success', $amount . ' Seller Balances Added to you account');

            return Redirect::to('/user/balance');

        } elseif ($response->isRedirect()) {
            // redirect to offsite payment gateway
            return $response->getRedirectResponse();
        } else {
            Session::flash('error', $response->getMessage());

            // payment failed: display message to customer
            return Redirect::to('/user/balance');
        }
    }

    public function balanceAdded()
    {
        if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        $user = Sentry::getUser();

        $params = \Illuminate\Support\Facades\Session::get('paypal_params');
        \Illuminate\Support\Facades\Session::forget('paypal_params');
        \Illuminate\Support\Facades\Session::save();

        if (!$params) {
            Session::flash('error', 'Your Transaction is invalid, please try again');

            return Redirect::to('/user/balance');
        }

        $response = Omnipay::completePurchase($params)->send();
        $paypalResponse = $response->getData(); // this is the raw response object

        if (isset($paypalResponse['PAYMENTINFO_0_ACK']) && $paypalResponse['PAYMENTINFO_0_ACK'] === 'Success') {

            $balanceToAdd = $params['items'][0]['price'];

            $paymentku = Payment::create(
                array(
                    'transaction_id' => $paypalResponse['PAYMENTINFO_0_TRANSACTIONID'],
                    'user_id'        => $user->id,
                    'balance'        => $user->balance + $balanceToAdd,
                    'amount'         => $balanceToAdd,
                    'details'        => $params['items'][0]['description']
                )
            );

            $user->increment('balance', $balanceToAdd);


            Session::flash('success', $balanceToAdd . ' Balance Seller Added to your account');

            return Redirect::to('/user/balance');

        } else {
            Session::flash('error', 'Your Transaction is invalid, please try again');

            return Redirect::to('/user/balance');

        }
    }


    /**
     * @return mixedunused
     */
    public function successPay()
    {

        $data = Input::all();

        $paypal = new \Reseller\Paypal\PaypalClass();
        $method = Request::method();
        if (isset($data['txn_id']) && $paypal->validate_ipn() && $paypal->payment_success) {
            file_put_contents('/tmp/data', json_encode($data) . PHP_EOL . PHP_EOL, FILE_APPEND);
            file_put_contents('/tmp/data', json_encode($method) . PHP_EOL . PHP_EOL, FILE_APPEND);
            file_put_contents('/tmp/data', json_encode($_SERVER['HTTP_USER_AGENT']) . PHP_EOL . PHP_EOL, FILE_APPEND);

            $payment_find = Payment::where('transaction_id', '=', $data['txn_id'])->first();
            if (!is_null($payment_find)) {
                Session::flash('error', 'Transaction ID already exists');

                if (stripos($_SERVER['HTTP_USER_AGENT'], 'paypal ipn') !== false) {
                    die();
                } else
                    return Redirect::to('/');
            }

            $user_id = $data['item_number'];
            $bal = 0;

            $userku = User::find($user_id);
            if ($userku)
                $bal = $userku->balance;
//            $payment_old = Payment::where('user_id', '=', $user->id)->orderBy('id', 'desc')->first();
//            if (!is_null($payment_old)) {
//                $bal = $payment_old->balance;
//            }

            $paymentku = Payment::create(
                array(
                    'transaction_id' => $data['txn_id'],
                    'user_id'        => $user_id,
                    'balance'        => $bal + $data['mc_gross'],
                    'amount'         => $data['mc_gross'],
                    'details'        => $data['item_name']
                )
            );


            if ($userku) {
                $userku->balance = $bal + $data['mc_gross'];
                $userku->save();
            }


            Session::flash('success', 'Balance Added');


            if (stripos($_SERVER['HTTP_USER_AGENT'], 'paypal ipn') !== false) {
                die();
            } else
                return Redirect::to('/');
        }

        Session::flash('error', 'Transaction ID Not Found');

        return Redirect::to('/');

    }

    public function storePay()
    {
        //DISABLE FUNC
        if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        $validator = Validator::make(
            Input::all(), array(
                'amount' => 'required|numeric|min:10',
            )
        );

        if ($validator->fails()) {
            Session::flash('error', 'Amount is required, it must be numeric least 10!');

            return Redirect::to('/user/balance')->withErrors($validator->messages())->withInput();
        }

        $data = Input::all();
        $paymentku = Payment::orderBy('id', 'desc')->first();
        $payment_id = 1;
        if (!is_null($paymentku)) {
            $payment_id = $paymentku->id + 1;
        }

        $user = Sentry::getUser();

        $grandTotal = $data['amount'];    //"2.97" harus ada titik;
        $item_name = "Add Balance Seller";    //"For 100 Likes";
        $recordID = $payment_id;
        $quantity = "1";


        $grandTotal = $data['amount'];    //"2.97" harus ada titik;
        $item_name = "Add Balance Seller";    //"For 100 Likes";
        $recordID = $payment_id;
        $quantity = "1";

        $paypal = new \Reseller\Paypal\PaypalClass();

//        var_dump($paypal->form_action);
//        var_dump($paypal->paypalurl);
//        dd();

        //optionally disable page caching by browsers
        $paypal->headers_nocache(); //should be called before any output

        //set the price
        $paypal->price = $grandTotal;


        //OR one-time payment
        $paypal->enable_payment();
        //change currency code
        $paypal->add('currency_code', 'USD');

        $paypall_email = Config::get('paypal.default_email');
        if (Config::get('paypal.use_sandbox')) {
            $paypal->add('payment_method', 'credit_card');
            $paypall_email = Config::get('paypal.default_email_sandbox');
        }

        $paypall_email_temp = '';
        $users = Sentry::findAllUsersWithAccess(array('admin'));
        if (isset($users)) {
            foreach ($users as $key => $value) {
                if (isset($value['paypall_email'])) {
                    $is_email = str_contains(e($value['paypall_email']), '@') ? true : false;
                    $paypall_email_temp = $is_email ? trim($value['paypall_email']) : '';
                    break;
                }
            }
        }

        if (($paypall_email_temp != '') || (!empty($paypall_email_temp))) {
            $paypall_email = $paypall_email_temp;
        }

        $paypal->ipn = Config::get('app.url') . '/user/balance/success';

        //your paypal email address
        $paypal->add('business', $paypall_email);

        $paypal->add('add', 1);
        $paypal->add('item_name', $item_name);
        $paypal->add('item_number', $user->id);
        $paypal->add('quantity', $quantity);
        $paypal->add('cancel_return', Config::get('app.url') . '/user/balance?msg=trxn_failed');
        $paypal->add('success', Config::get('app.url') . '/user/balance/success');
        $paypal->add('shopping_url', Config::get('app.url') . '/user/balance/success');
        $paypal->add('notify_url', Config::get('app.url') . '/user/balance/success');
        $paypal->add('return', Config::get('app.url') . '/user/balance/success');
        $paypal->add('cbt', trans('payments.click_complete'));
        $paypal->add('test_ipn', Config::get('paypal.use_sandbox') ? 1 : 0);
        $paypal->output_form();


    }

    /**
     * Store a newly created resource in storage.
     * POST /payment
     *
     * @return Response
     */
    public function store()
    {
        if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }
        /*
        if (!Sentry::getUser() -> hasAccess('admin')) {
            return Redirect::to('/') -> withErrors(trans('users.ploginf'));
        }
        $validator = Validator::make( Input::all(), array(
                            'username'      =>'required|min:4',
                            'amount'               =>'required|min:1',
                            )
                    );

        if($validator->fails()){
            Session::flash('error', 'Input must be required and it is numeric!');
            return Redirect::to('/admin/payments')->withErrors($validator->messages())->withInput();
        }

        $data = Input::all();
        $user = User::where('username','=',$data['username'])->first();

        if(is_null($user)){
            Session::flash('error', 'User not found!');
            return Redirect::to('/admin/payments')->withInput();
        }

        $payment = new Payment();
        $payment->transaction_id = strtoupper($this->generateRandomString(17));
        $payment->user_id = $user->id;
        $payment->amount = $data['amount'];
        if(isset($data['details'])){
            $payment->details = $data['details'];
        }

        if ($payment->save()) {

            // Success!

            Session::flash('success', 'Success');
            return Redirect::to('/admin/payments');

        } else {
         */
        Session::flash('error', 'Error!');

        return Redirect::to('/admin/payments')->withInput();
        /*
        }
         */
    }

    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    /**
     * Display the specified resource.
     * GET /payment/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /payment/{id}/edit
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * PUT /payment/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /payment/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Balance Adder index
     */
    public function balanceAdder()
    {

        if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }

        if (!Sentry::getUser()->hasAccess('admin')) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        $users = User::all();

        return View::make('views.admin.balanceAdder.main')->with('users', $users);

    }

    /**
     * Add Balance Form
     */
    public function addBalance($id)
    {

        if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }

        if (!Sentry::getUser()->hasAccess('admin')) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        $users = User::find($id);

        return View::make('views.admin.balanceAdder.create')->with('users', $users);

    }

    /**
     * Add Balance Store
     */
    public function storeBalance()
    {

        if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }

        if (!Sentry::getUser()->hasAccess('admin')) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        $data = array();
        $data = Input::all();
        $id = $data['id'];
        $users = User::find($id);
        $by = Sentry::getUser()->id;
        $amount = $data['b_amount'];

        $validator = Validator::make(Input::all(), array('id' => 'required', 'b_amount' => 'required'));

        if ($validator->fails()) {
            Session::flash('error', 'Amount of balance that will added required');

            return Redirect::action('PaymentController@addBalance', array($id))->withErrors($validator)->withInput();
        } else {
            $currentBalance = $users->balance + $amount;
            $users->balance = $currentBalance;
            $users->save();

            BalanceAdder::create(
                array(
                    'user_id' => $users->id,
                    'balance' => $currentBalance,
                    'amount'  => $amount,
                    'by'      => $by
                )
            );

            Session::flash('success', 'Balance has added succesfully!');

            return Redirect::to('admin/balanceAdder/history');
        }

    }

    /**
     * Balance Adder History index
     */
    public function balanceHistory()
    {

        if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }

        if (!Sentry::getUser()->hasAccess('admin')) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        //$history = array();
        $historyAll = BalanceAdder::orderBy('updated_at', 'desc')->orderBy('id', 'desc')->with('one_user')->paginate($this->pagging);

        /*
        if(!is_null($historyAll)){
                foreach ($historyAll as $h) {
                        $history[$h->id]['id'] = $h->id;
                        $history[$h->id]['user'] = User::where('id','=', $h->user_id)->pluck('username');
                        $history[$h->id]['balance'] = $h->balance;
                        $history[$h->id]['amount'] = $h->amount;
                        $history[$h->id]['created'] = $h->created_at;
                        $history[$h->id]['updated'] = $h->updated_at;
                        if (Sentry::getUser() -> hasAccess('su_admin')) {
                                $history[$h->id]['edit_btn'] = '<a class="btn btn-primary btn-xs action_confirm" href="'.action('PaymentController@editBalance', array($h['id'])).'">
                                                                        <i class="fa fa-edit fa-lg"></i></a>';
                                $history[$h->id]['cancel_btn'] = '<a class="btn btn-danger btn-xs action_confirm" href="'.action('PaymentController@cancelBalance', array($h['id'])).'" data-token="' . Session::getToken() . '" data-method="delete">
                                                                            <i class="fa fa-trash fa-lg"></i></a>';
                        }else{
                                $history[$h->id]['edit_btn'] = '';
                                $history[$h->id]['cancel_btn'] = '';
                        }
                }
        }
         */

        return View::make('views.admin.balanceAdder.history')->with('histories', $historyAll);

    }


    /**
     * Edit Balance
     */
    public function editBalance($id)
    {

        if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }

        if (!Sentry::getUser()->hasAccess('su_admin')) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        $balance = BalanceAdder::find($id);
        $users = User::find($balance->user_id);

        return View::make('views.admin.balanceAdder.edit')->with('balance', $balance)->with('users', $users);

    }

    public function cancelBalance($id)
    {

        //TODO: use try catch exception for better logic
        //TODO: Use DB begin transaction, commit, rollback if error
        if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }

        if (!Sentry::getUser()->hasAccess('su_admin')) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }


        $balance_id = $id;
        $balance = BalanceAdder::find($balance_id);
        if (!$balance) {
            Session::flash('error', 'History Not Found');

            return Redirect::action('PaymentController@balanceHistory');
        }
        $user_id = $balance->user_id;
        $users = User::find($user_id);

        $oldAmount = $balance->amount;
        $newBalance = $users->balance - $oldAmount;

        if ($newBalance < 0) {
            Session::flash('error', 'Balance User Become Less than 0');

            return Redirect::action('PaymentController@balanceHistory');
        }

        $users->balance = $newBalance;
        $users->save();


        if ($balance->delete()) {
            Session::flash('success', 'Balance has been restored!');

            return Redirect::to('admin/balanceAdder/history');
        }


    }

    /**
     * Update Balance
     */
    public function updateBalance()
    {

        if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }

        if (!Sentry::getUser()->hasAccess('su_admin')) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        $data = array();
        $data = Input::all();
        $user_id = $data['user_id'];
        $users = User::find($user_id);
        $balance_id = $data['balance_id'];
        $balance = BalanceAdder::find($balance_id);
        $by = Sentry::getUser()->id;
        $amount = $data['b_amount'];

        $validator = Validator::make(Input::all(), array('user_id' => 'required', 'balance_id' => 'required', 'b_amount' => 'required'));

        if ($validator->fails()) {
            Session::flash('error', 'Amount of balance that will added required');

            return Redirect::action('PaymentController@editBalance', array($balance_id))->withErrors($validator)->withInput();
        } else {
            $oldAmount = $balance->amount;
            $difference = $oldAmount > $amount ? $oldAmount - $amount : $amount - $oldAmount;
            $newBalance = $oldAmount > $amount ? $users->balance - $difference : $users->balance + $difference;

            if ($newBalance < 0) {
                Session::flash('error', 'Balance User Become Less than 0');

                return Redirect::action('PaymentController@balanceHistory');
            }

            $users->balance = $newBalance;
            $users->save();

            $balance->balance = $newBalance;
            $balance->amount = $amount;
            $balance->by = $by;

            if ($balance->save()) {
                Session::flash('success', 'Balance has updated succesfully!');

                return Redirect::to('admin/balanceAdder/history');
            } else {
                Session::flash('error', 'Something wrong, cannot update it, please try again!');

                return Redirect::to('admin/balanceAdder/history');
            }


        }

    }

}
