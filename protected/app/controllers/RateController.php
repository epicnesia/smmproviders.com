<?php

class RateController extends \BaseController {

	public function __construct() {
		 $this->beforeFilter('Sentry', array('except' => 'login'));
	}

	/**
	 * Display a listing of the resource.
	 * GET /rate
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /rate/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /rate
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /rate/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /rate/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		if(!Sentry::check()){
       		return Redirect::to('/')
           			->withErrors( trans('users.ploginf') ); 
     	}
                
  		if (!Sentry::getUser() -> hasAccess('admin')) {
        	return Redirect::to('/') -> withErrors(trans('users.noaccess'));
      	}
                
   		$services 	= Services::whereNull('is_subscription')->get();
       	$rates 		= array();
     	foreach($services as $service){
     		
        	$rate 	= Rate::where('user_id','=',$id)->where('service_id','=',$service->id)->first();
           	
           	$rates[$service->id] = !is_null($rate) ? $rate->rates : 0;
			
     	}
		
		$autoLikes		= Services::where('name', '=', 'Auto Likes')->whereNotNull('is_subscription')->first();
		$autoLikesRates	= array();
		foreach($autoLikes->plans as $alPlan){
			
			$planRate	= ServicePlanRates::where('user_id','=',$id)->where('plan_id','=',$alPlan->id)->first();
			
			$autoLikesRates[$alPlan->id][0] = !is_null($planRate) ? $planRate->rate_7_days : 0;
			$autoLikesRates[$alPlan->id][1] = !is_null($planRate) ? $planRate->rate_30_days : 0;
			$autoLikesRates[$alPlan->id][2] = !is_null($planRate) ? $planRate->rate_60_days : 0;
			$autoLikesRates[$alPlan->id][3] = !is_null($planRate) ? $planRate->rate_90_days : 0;
			
		}
		
		$autoViews		= Services::where('name', '=', 'Auto Views')->whereNotNull('is_subscription')->first();
		$autoViewsRates	= array();
		foreach($autoViews->plans as $avPlan){
			
			$planRate	= ServicePlanRates::where('user_id','=',$id)->where('plan_id','=',$avPlan->id)->first();
			
			$autoViewsRates[$avPlan->id][0] = !is_null($planRate) ? $planRate->rate_7_days : 0;
			$autoViewsRates[$avPlan->id][1] = !is_null($planRate) ? $planRate->rate_30_days : 0;
			$autoViewsRates[$avPlan->id][2] = !is_null($planRate) ? $planRate->rate_60_days : 0;
			$autoViewsRates[$avPlan->id][3] = !is_null($planRate) ? $planRate->rate_90_days : 0;
			
		}
                
		return View::make('views.admin.rates')
                      ->with('user_id', $id)
                      ->with('rate', $rates)
                      ->with('services', $services)
                      ->with('autoLikesRates', $autoLikesRates)
                      ->with('autoLikes', $autoLikes->plans)
                      ->with('autoViewsRates', $autoViewsRates)
                      ->with('autoViews', $autoViews->plans);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /rate/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		try {
			if(!Sentry::check()){
				return Redirect::to('/')
	            	->withErrors( trans('users.ploginf') ); 
	 		}
                
	  		if (!Sentry::getUser() -> hasAccess('admin')) {
	        	return Redirect::to('/') -> withErrors(trans('users.noaccess'));
	      	}
	                
			$data = array();
			$data = Input::all(); 
			
			DB::beginTransaction();
	        
			// Process the regular services first
			foreach ($data['rates'] as $key => $val) {
					
				$rate = Rate::where('user_id', '=', $id)
							->where('service_id','=',$key)
		                    ->first();      
		                        
				if(count($rate) > 0){
					if($val > 0){
		               	$rate->rates = $val;
		             	$rate->save();
		          	}else{
		            	$rate->delete();
		          	}
		       	}else{
		           	if($val > 0){
		              	Rate::create(array('user_id'=> $id,
		                            	   'service_id'=> $key,
		                            	   'rates'=> $val
		                                   )
									);
		        	}
		       	}
					
			}
			
			// Auto likes plans process
			foreach ($data['autoLikesRates'] as $key => $val) {
					
				$rate = ServicePlanRates::where('user_id', '=', $id)
							->where('plan_id','=',$key)
		                    ->first();      
		                        
				if(count($rate) > 0){
					if($val[0] > 0 || $val[1] > 0 || $val[2] > 0 || $val[3] > 0){
		            	$rate->rate_7_days = $val[0];
						$rate->rate_30_days = $val[1];
						$rate->rate_60_days = $val[2];
						$rate->rate_90_days = $val[3];
		           		$rate->save();
		     		}else{
		          		$rate->delete();
		       		}
		       	}else{
		           	if($val[0] > 0 || $val[1] > 0 || $val[2] > 0 || $val[3] > 0){
		              	ServicePlanRates::create(array('user_id'=> $id,
					                            	   'plan_id'=> $key,
					                            	   'rate_7_days'=> $val[0],
					                            	   'rate_30_days'=> $val[1],
					                            	   'rate_60_days'=> $val[2],
					                            	   'rate_90_days'=> $val[3],
		                                   			  )
												);
		        	}
		       	}
					
			}

			// Auto views plans process
			foreach ($data['autoViewsRates'] as $key => $val) {
					
				$rate = ServicePlanRates::where('user_id', '=', $id)
							->where('plan_id','=',$key)
		                    ->first();      
		                        
				if(count($rate) > 0){
					if($val[0] > 0 || $val[1] > 0 || $val[2] > 0 || $val[3] > 0){
		            	$rate->rate_7_days = $val[0];
						$rate->rate_30_days = $val[1];
						$rate->rate_60_days = $val[2];
						$rate->rate_90_days = $val[3];
		           		$rate->save();
		     		}else{
		          		$rate->delete();
		       		}
		       	}else{
		           	if($val[0] > 0 || $val[1] > 0 || $val[2] > 0 || $val[3] > 0){
		              	ServicePlanRates::create(array('user_id'=> $id,
					                            	   'plan_id'=> $key,
					                            	   'rate_7_days'=> $val[0],
					                            	   'rate_30_days'=> $val[1],
					                            	   'rate_60_days'=> $val[2],
					                            	   'rate_90_days'=> $val[3],
		                                   			  )
												);
		        	}
		       	}
					
			}
			
			DB::commit();
	                
			Session::flash('success', 'Rate edited successfully');
			return Redirect::to('admin/users');
			
		} catch (Exception $e) {
            DB::rollback();

            Session::flash('error','('. $e->getLine() .') '. $e->getMessage());

            return Redirect::to('admin/users')->withErrors($e->getMessage());
        }
		
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /rate/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}