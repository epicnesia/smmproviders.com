<?php

class PageController extends \BaseController {
	protected $lbl_status;
        
    public function __construct() {
    		$this->beforeFilter('Sentry', array('except' => 'login'));
            $this ->lbl_status = array();
            $this ->lbl_status[0] = 'Hidden';
            $this ->lbl_status[1] = 'Published';            
    }
	/**
	 * Display a listing of the resource.
	 * GET /page
	 *
	 * @return Response
	 */
	public function index()
	{
		if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }
		
		$pages = Page::all(); 
		return View::make('views.page.list')
				->with('pages',$pages)				
				->with('status',$this->lbl_status);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /page/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /page
	 *
	 * @return Response
	 */
	public function store()
	{
		if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }
		
		
	}

	/**
	 * Display the specified resource.
	 * GET /page/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }
		
		$page = Page::find($id); 
		return View::make('views.page.page')
			->with('page',$page);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /page/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }
		
		$page = Page::find($id); 
		return View::make('views.page.edit')->with('page',$page);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /page/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }
		
		$user = Sentry::getUser();
		
		$validator = Validator::make(Input::all(), array(						
						'editor1' 	=> 'required|min:5'					
						));
						
		if($validator->fails()){
			Session::flash('error', 'All is required!');            
			return Redirect::to('/page/' . $id . '/edit')->withErrors($validator->messages())->withInput();
		}						
		
		$page = Page::find($id); 
		if(is_null($page)){
			Session::flash('error', 'Page is not found!');            
			return Redirect::to('/');
		}
		
		$page->title = Input::get('judul');
		$page->content = Input::get('editor1');
		if($page->save()){
			Session::flash('success', Input::get('judul') . ' has created succesfully!');
        	return Redirect::to('/');	
		}		
		
		Session::flash('error', 'Page is not saved!');            
		return Redirect::to('/');
		
        
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /page/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}