<?php

class TicketController extends \BaseController {
	protected $lbl_status;
        
    public function __construct() {
    		$this->beforeFilter('Sentry', array('except' => 'login'));
            $this ->lbl_status = array();
            $this ->lbl_status[0] = 'Open';
            $this ->lbl_status[1] = 'Closed';
    }
		
	public function check()
    {
        $user = Sentry::getUser();
        $tickets = null;
        if($user->hasAccess('admin')){
                $tickets = Ticket::first();
        }else{
                $tickets = Ticket::where('user_id', '=', $user->id)->first();
        }
        if (is_null($tickets)) {
            return false;
        }
        if (count($tickets) == 0) {
            return false;
        }

        return true;
    }

    protected function getAllData($ticket_id = 0)
    {
        if (!$this->check()) {
            return array();
        }

        $user = Sentry::getUser();
        $tickets = null;
		if($ticket_id==0){
	        if($user->hasAccess('admin')){        	
	        	$tickets = Ticket::where('reply_to', '=', 0)->get();
	        }else{        	
	        	$tickets = Ticket::where('user_id', '=', $user->id)->where('reply_to', '=', 0)->get();
	        }
		}else{
        		$tickets = Ticket::where('reply_to','=', $ticket_id)
							->orWhere('id','=', $ticket_id)
							->orderBy('id','asc')
							->get();
        }	
                        
        if (is_null($tickets)) {
            return array();
        }
		
        $tickets_1 = array();
        $status_id = 0;
        foreach ($tickets as $ticket) {           
			
			if($ticket->reply_to != 0){
				$seller = User::find($ticket->reply_from);
			}else{
				$seller = User::find($ticket->user_id);	
			}
            
            try {
                $lastrepliedby = User::find(Ticket::where('user_id', '=', $ticket->user_id)->where('reply_to', '!=', 0)->orderBy('created_at', 'desc')->pluck('reply_from'));                
            } catch (Exception $e) {
                $lastrepliedby = null;
            }
            
             
            $tickets_unread = null;
            $is_tickets_unread = 1;            
                        
            if($user->hasAccess('admin')){          
                $tickets_unread = TicketUnread::where('user_id', '=', 0)->where('ticket_id', '=', $ticket->id)->first();                
            }else{          
                $tickets_unread = TicketUnread::where('user_id', '=', $user->id)->where('ticket_id', '=', $ticket->id)->first();
            }
            
            if (is_null($tickets_unread)) {
                $is_tickets_unread = 0;
            }
            
 
            
            $created_by = User::find($ticket->user_id);

            $tickets_1[$ticket->id] = array(
                'id'                   => $ticket->id,      
                'title'                   => $ticket->title,               
                'username'          => $seller ? $seller->username : '',
                'content'      => $ticket->content,
                'status'         => $this ->lbl_status[$ticket->status],
                'lastrepliedby'         => $lastrepliedby?$lastrepliedby->username:'',
                'created_by'         => $created_by?$created_by->username:'',
                'date' =>  $ticket->created_at,
                'unread' =>  $is_tickets_unread
            );
        }


        return $tickets_1;
    }

	/**
	 * Display a listing of the resource.
	 * GET /ticket
	 *
	 * @return Response
	 */
	public function index()
	{
		if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }
				
		$tickets = $this->getAllData();
		 
		return View::make('views.ticket.ticket')->with('tickets', $tickets);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /ticket/create
	 *
	 * @return Response
	 */
	public function create()
	{
		if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }
		
		return View::make('views.ticket.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /ticket
	 *
	 * @return Response
	 */
	public function store()
	{
	    try{
	        
	    DB::beginTransaction();
        
		if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }
		
		$user = Sentry::getUser();
		
		$validator = Validator::make(Input::all(), array(
						'judul' => 'required|min:4',
						'isi' 	=> 'required|min:5'					
						));
						
		if($validator->fails()){
			Session::flash('error', 'All is required!');            
			return Redirect::to('/ticket/create')->withErrors($validator->messages())->withInput();
		}						
		
		$ticket = Ticket::create(array(
        'title'=>Input::get('judul'),
        'content'=>Input::get('isi'),
        'user_id'=> $user->id,
        'is_read' => 0
        ));
        
        TicketUnread::create(array(        
        'ticket_id'=> $ticket->id
        ));
        
        DB::commit();
        
        Session::flash('success', Input::get('judul') . ' has created succesfully!');
        return Redirect::to('/ticket');
        
        }catch(Exception $e){
            DB::rollback();
            $err = $e->getLine() . ' ' . $e->getMessage();
            Session::flash('error', $err);
            return Redirect::to('/ticket')->withInput()->withErrors($err);    
        }
	}


	/**
	 * Display the specified resource.
	 * GET /ticket/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }
		
		$user = Sentry::getUser();
		
		$userku = User::find($user->id);
		$tickets = $this->getAllData($id);
		$ticketku = Ticket::find($id);
        if($ticketku){
            if($user->hasAccess('admin')){
    		    $ticket_unread = TicketUnread::where('ticket_id','=',$id)->first();
                if($ticket_unread){$ticket_unread->delete();}
            }else{
                $ticket_unread = TicketUnread::where('ticket_id','=',$id)->where('user_id','=',$user->id)->first();
                if($ticket_unread){$ticket_unread->delete();}
            }
        }
		$user_ticket = User::find($ticketku->user_id);
		
		$status = $this->lbl_status[$ticketku->status]; 
		
		return View::make('views.ticket.reply')
					->with('username', $userku->username)
					->with('user_ticket_id', $user_ticket->id)					
					->with('tickets', $tickets)
					->with('ticketku', $ticketku)
					->with('status', $status);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /ticket/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /ticket/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }
		
		$user = Sentry::getUser();
		
		$validator = Validator::make(Input::all(), array(						
						'isi' 	=> 'required'					
						));
						
		if($validator->fails()){
			Session::flash('error', 'All is required!');            
			return Redirect::to('/ticket/'. $id .' /show')->withErrors($validator->messages())->withInput();
		}						
		
		Ticket::create(array(
        'title'=>'Re:' . Input::get('judul'),
        'content'=>Input::get('isi'),
        'user_id'=> Input::get('user_id'),
        'reply_to'=> $id,
        'reply_from'=> $user->id
        ));
        
        if($user->hasAccess('admin')){
            TicketUnread::create(array(        
            'ticket_id'=> $id,
            'user_id'=> Input::get('user_id')
            ));               
         } else{
           TicketUnread::create(array(        
            'ticket_id'=> $id,
            'user_id'=> 0
            ));
         }
         
        Session::flash('success', Input::get('judul') . ' has created succesfully!');
        return Redirect::to('/ticket');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /ticket/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}