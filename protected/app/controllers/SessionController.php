<?php

use Authority\Repo\Session\SessionInterface;
use Authority\Service\Form\Login\LoginForm;
use Cartalyst\Sentry\Users\Eloquent\User;
use Authority\Service\Validation\ValidableInterface;

class SessionController extends BaseController {

	/**
	 * Member Vars
	 */
	protected $session;
	protected $loginForm;

	/**
	 * Constructor
	 */
	public function __construct(SessionInterface $session, LoginForm $loginForm) {
		$this -> session = $session;
		$this -> loginForm = $loginForm;
	}

	/**
	 * Show the login form
	 */
	public function create() {
		$email = Session::get('email');
		$password = Session::get('password');	
		$rememberMe = is_null(Session::get('rememberMe'))?false:true;
		return View::make('views/login')
			->with('email_old', $email)
			->with('password_old', $password)
			->with('rememberMe_old', $rememberMe);
	}

	public function store() {
		// Form Processing
		$data = Input::all();

		$is_email = str_contains(e($data['email']), '@') ? true : false;

		$user = new User();

		if ($is_email) {
			// validate the info, create rules for the inputs
			$rules = array('email' => 'required|email', // make sure the email is an actual email
			'password' => 'required|alphaNum|min:6' // password can only be alphanumeric and has to be greater than 3 characters
			);

			// run the validation rules on the inputs from the form
			$validator = Validator::make($data, $rules);

			// if the validator fails, redirect back to the form
			if ($validator -> fails()) {
				$result['message'] = 'Error';
				Session::flash('error', $result['message']);
				return Redirect::to('/') -> withInput() -> withErrors($validator);
			}

			$user -> setLoginAttributeName('email');
			$credentials = array('email' => e($data['email']), 'password' => e($data['password']));
		} else {
			// validate the info, create rules for the inputs
			$rules = array('email' => 'required|min:4', // make sure the email is an actual email
			'password' => 'required|alphaNum|min:6' // password can only be alphanumeric and has to be greater than 3 characters
			);

			// run the validation rules on the inputs from the form
			$validator = Validator::make($data, $rules);

			// if the validator fails, redirect back to the form
			if ($validator -> fails()) {
				$result['message'] = 'Error';
				Session::flash('error', $result['message']);
				return Redirect::to('/') -> withInput() -> withErrors($validator);
			}

			$user -> setLoginAttributeName('username');
			$credentials = array('username' => e($data['email']), 'password' => e($data['password']));
		}
		
		try {
			$remMe = isset($data['rememberMe']) ? true :false;
			if ($remMe ? Sentry::authenticateAndRemember($credentials) :Sentry::authenticate($credentials, false)) {
				
				if(Sentry::check()){
					$user_s =Sentry::getUser(); 
					$userku = User::find($user_s ->id);
					$userku->lastlogin =  date("Y-m-d H:i:s");
					$userku->save(); 
				}
				 if($remMe){
				 	Session::set('email', e($data['email']));
					Session::set('password', e($data['password']));
					Session::set('rememberMe', true);
				 }else{
				 	Session::set('email', null);
					Session::set('password', null);
					Session::set('rememberMe', null);
				 }
				
				// Success!				
				return Redirect::to('/main');

			} else {
				$result['message'] = 'error';
				Session::flash('error', $result['message']);
				return Redirect::to('/') -> withInput() -> withErrors($this -> loginForm -> errors());

			}
		} catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
			$result['message'] = 'Login field is required.';
			Session::flash('error', $result['message']);
			return Redirect::to('/') -> withInput() -> withErrors($this -> loginForm -> errors());
		} catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
			$result['message'] = 'Password field is required.';
			Session::flash('error', $result['message']);
			return Redirect::to('/') -> withInput() -> withErrors($this -> loginForm -> errors());
		} catch (Cartalyst\Sentry\Users\WrongPasswordException $e) {
			$result['message'] = 'Wrong password, try again.';
			$result['message'] = 'Invalid username or password';
			Session::flash('error', $result['message']);
			//return Redirect::to('/') -> withInput() -> withErrors($this -> loginForm -> errors());
			return Redirect::to('/') -> withInput() -> withErrors($result['message']);
		} catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
			$result['message'] = 'User was not found.';
			$result['message'] = 'Invalid username or password';
			Session::flash('error', $result['message']);
			//return Redirect::to('/') -> withInput() -> withErrors($this -> loginForm -> errors());
			return Redirect::to('/') -> withInput() -> withErrors($result['message']);
		} catch (Cartalyst\Sentry\Users\UserNotActivatedException $e) {
			$result['message'] = 'User is not activated.';
			Session::flash('error', $result['message']);
			return Redirect::to('/') -> withInput() -> withErrors($this -> loginForm -> errors());
		}catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
		{		    
			$result['message'] = 'User is banned.';
			Session::flash('error', $result['message']);
			return Redirect::to('/') -> withInput() -> withErrors($this -> loginForm -> errors());
		}

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy() {
		$this -> session -> destroy();
		Session::flush();
		Event::fire('user.logout');
		return Redirect::to('/');
	}

}
