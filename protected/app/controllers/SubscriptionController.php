<?php
use \Reseller\Api;

class SubscriptionController extends \BaseController {
	
    protected $pagging;

    public function __construct()
    {
        $this->beforeFilter('Sentry', array('except' => 'login'));
        $this->pagging = 10;
    }

    public function check()
    {
        $user = Sentry::getUser();
        $orders = null;
        if ($user->hasAccess('admin')) {
            $orders = Order::first();
        } else {
            $orders = Order::where('seller_id', '=', $user->id)->first();
        }
        if (is_null($orders)) {
            return false;
        }
        if (count($orders) == 0) {
            return false;
        }

        return true;
    }

	/**
	 * Display a listing of the resource.
	 * GET /subscription
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /subscription/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /subscription/add_plan
	 *
	 * @return Response
	 */
	public function addPlan($id)
	{
		if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }
                
  		if (!Sentry::getUser() -> hasAccess('admin')) {
        	return Redirect::to('/') -> withErrors(trans('users.noaccess'));
      	}
		
		$service = Services::find($id);
			
		if(!$service->is_subscription) {
			return Redirect::to('/') -> withErrors(trans('users.noaccess'));
		}
		
		return View::make('views.subscription.add_plans')
						->with('service', $service)
	            		->with('active_count', $this->getCountData($service->id))
	            		->with('expired_count', $this->getCountData($service->id, false));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /subscription/store_plan
	 *
	 * @return Response
	 */
	public function storePlan()
	{
		if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }
                
  		if (!Sentry::getUser() -> hasAccess('admin')) {
        	return Redirect::to('/') -> withErrors(trans('users.noaccess'));
      	}
			
		$service 		= Services::find(Input::get('service_id'));
		$red_service	= str_replace(" ", "_", strtolower($service->name));
		$red_error		= 'subscription/'.$service->id.'/add_plan';
		
		try {
			
			if(!$service->is_subscription) throw new Exception("Error Processing Request");
			
            $validator = Validator::make(
     			Input::all(), array(
              		'service_id' 	=> 'required|integer|exists:services,id',
              		'name'			=> 'required|min:1',
              		'count'			=> 'required|min:1',
              		'price_7_days'	=> 'required|min:1',
              		'cost_7_days'	=> 'required|min:1',
              		'price_30_days'	=> 'required|min:1',
              		'cost_30_days'	=> 'required|min:1',
              		'price_60_days'	=> 'required|min:1',
              		'cost_60_days'	=> 'required|min:1',
              		'price_90_days'	=> 'required|min:1',
              		'cost_90_days'	=> 'required|min:1',
              	)
        	);
			
			if ($validator->fails()) {
                DB::rollback();
                Session::flash('error', 'All input form must be required!!!');

                return Redirect::to($red_error)->withErrors($validator)->withInput();
            }

            DB::beginTransaction();
			
			$proc = ServicePlans::create(Input::all());
			
			DB::commit();
			
			Session::flash('success', 'Success!');
			return Redirect::to($red_service);
			
			
		} catch (Exception $e) {
        	Log::error($e);
            DB::rollback();

            Session::flash('error','('. $e->getLine() .') '. $e->getMessage());

            return Redirect::to($red_error)->withErrors($e->getMessage())->withInput();
        }
		
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /subscription/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function editPlan($id)
	{
	 	if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }
                
  		if (!Sentry::getUser() -> hasAccess('admin')) {
        	return Redirect::to('/') -> withErrors(trans('users.noaccess'));
      	}
		
		$plan 			= ServicePlans::find($id);
		$red_service	= str_replace(" ", "_", strtolower($plan->service->name));
		
		return View::make('views.subscription.edit_plan')
					->with('plan', $plan)
					->with('service', $plan->service)
	            	->with('active_count', $this->getCountData($plan->service->id))
	            	->with('expired_count', $this->getCountData($plan->service->id, false));
	}
	
	/**
	 * Update the specified resource in storage.
	 * PUT /subscription/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function updatePlan($id)
	{
		
		if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }
                
  		if (!Sentry::getUser() -> hasAccess('admin')) {
        	return Redirect::to('/') -> withErrors(trans('users.noaccess'));
      	}
		
		$plan 			= ServicePlans::find($id);
		$red_service	= str_replace(" ", "_", strtolower($plan->service->name));
		$red_error		= 'subscription/'.$plan->id.'/edit_plan';
		
		try {
			
			$validator = Validator::make(
     			Input::all(), array(
              		'service_id' 	=> 'required|integer|exists:services,id',
              		'name'			=> 'required|min:1',
              		'count'			=> 'required|min:1',
              		'price_7_days'	=> 'required|min:1',
              		'cost_7_days'	=> 'required|min:1',
              		'price_30_days'	=> 'required|min:1',
              		'cost_30_days'	=> 'required|min:1',
              		'price_60_days'	=> 'required|min:1',
              		'cost_60_days'	=> 'required|min:1',
              		'price_90_days'	=> 'required|min:1',
              		'cost_90_days'	=> 'required|min:1',
              	)
        	);
			
			if ($validator->fails()) {
                DB::rollback();
                Session::flash('error', 'All input form must be required!!!');

                return Redirect::to($red_error)->withErrors($validator)->withInput();
            }

            DB::beginTransaction();
			
			$data = Input::all();
			
			$plan->name 			= $data['name'];
			$plan->count 			= $data['count'];
			$plan->price_7_days 	= $data['price_7_days'];
			$plan->cost_7_days 		= $data['cost_7_days'];
			$plan->price_30_days 	= $data['price_30_days'];
			$plan->cost_30_days 	= $data['cost_30_days'];
			$plan->price_60_days 	= $data['price_60_days'];
			$plan->cost_60_days 	= $data['cost_60_days'];
			$plan->price_90_days 	= $data['price_90_days'];
			$plan->cost_90_days 	= $data['cost_90_days'];
			
			$plan->save();
			
			DB::commit();
			
			Session::flash('success', 'Success!');
			return Redirect::to($red_service);
			
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();
			Session::flash('error','('. $e->getLine() .') '. $e->getMessage());
            return Redirect::to($red_error)->withErrors($e->getMessage())->withInput();
        }
		
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /auto_likes
	 *
	 * @return Response
	 */
	public function auto_likes()
	{
		if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }

        $service 	= Services::where('name', '=', 'Auto Likes')
        						->where('is_subscription', '=', '1')->first();
							
		$plans		= $service->plans()->paginate(10);
		$allPlans	= array('0'=> '-- Select Plan --');
		foreach($service->plans as $plan){
			$allPlans[$plan->id] = $plan->name;
		}
							
		if(Sentry::getUser()->hasAccess('admin')){
			return View::make('views.subscription.plans')
	            		->with('service', $service)
	            		->with('plans', $plans)
	            		->with('active_count', $this->getCountData($service->id))
	            		->with('expired_count', $this->getCountData($service->id, false));
		}else{
			return View::make('views.subscription.create')
	            		->with('service', $service)
	            		->with('plans', $allPlans)
	            		->with('active_count', $this->getCountData($service->id))
	            		->with('expired_count', $this->getCountData($service->id, false));
		}

        
	}
	
	/**
	 * Show list of specific resource.
	 * GET /auto_likes/active
	 *
	 * @return Response
	 */
	public function activeAutoLikes()
	{
		if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }
		
		$search  = Input::has('s') ? self::getSearchParam(Input::get('s')) : 'id IS NOT NULL';

        $service = Services::where('name', '=', 'Auto Likes')
        					->where('is_subscription', '=', '1')->first();
		
        $statusku = Status::all();
		
        $status_r = array();
        foreach ($statusku as $s) {
            $status_r[$s->id] = $s->name;
        }

        return View::make('views.subscription.list')
					->with('service', $service)
					->with('orders', $this->getData($service->id, $search))
            		->with('status_arr', $status_r)
            		->with('active_count', $this->getCountData($service->id))
            		->with('expired_count', $this->getCountData($service->id, false));
	}
	
	/**
	 * Show list of specific resource.
	 * GET /auto_likes/active
	 *
	 * @return Response
	 */
	public function expiredAutoLikes()
	{
		if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }
		
		$search  = Input::has('s') ? self::getSearchParam(Input::get('s')) : 'id IS NOT NULL';

        $service = Services::where('name', '=', 'Auto Likes')
        					->where('is_subscription', '=', '1')->first();
		
        $statusku = Status::all();
		
        $status_r = array();
        foreach ($statusku as $s) {
            $status_r[$s->id] = $s->name;
        }

        return View::make('views.subscription.list')
					->with('service', $service)
					->with('orders', $this->getData($service->id, $search, false))
            		->with('status_arr', $status_r)
            		->with('active_count', $this->getCountData($service->id))
            		->with('expired_count', $this->getCountData($service->id, false));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /auto_views
	 *
	 * @return Response
	 */
	public function auto_views()
	{
		if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }

        $service 	= Services::where('name', '=', 'Auto Views')
        						->where('is_subscription', '=', '1')->first();
							
		$plans		= $service->plans()->paginate(10);
		$allPlans	= array('0'=> '-- Select Plan --');
		foreach($service->plans as $plan){
			$allPlans[$plan->id] = $plan->name;
		}
							
		if(Sentry::getUser()->hasAccess('admin')){
			return View::make('views.subscription.plans')
	            		->with('service', $service)
	            		->with('plans', $plans)
	            		->with('active_count', $this->getCountData($service->id))
	            		->with('expired_count', $this->getCountData($service->id, false));
		}else{
			return View::make('views.subscription.create')
	            		->with('service', $service)
	            		->with('plans', $allPlans)
	            		->with('active_count', $this->getCountData($service->id))
	            		->with('expired_count', $this->getCountData($service->id, false));
		}
	}
	
	/**
	 * Show list of specific resource.
	 * GET /auto_views/active
	 *
	 * @return Response
	 */
	public function activeAutoViews()
	{
		if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }
		
		$search  = Input::has('s') ? self::getSearchParam(Input::get('s')) : 'id IS NOT NULL';

        $service = Services::where('name', '=', 'Auto Views')
        					->where('is_subscription', '=', '1')->first();
		
        $statusku = Status::all();
		
        $status_r = array();
        foreach ($statusku as $s) {
            $status_r[$s->id] = $s->name;
        }

        return View::make('views.subscription.list')
					->with('service', $service)
					->with('orders', $this->getData($service->id, $search))
            		->with('status_arr', $status_r)
            		->with('active_count', $this->getCountData($service->id))
            		->with('expired_count', $this->getCountData($service->id, false));
	}
	
	/**
	 * Show list of specific resource.
	 * GET /auto_likes/active
	 *
	 * @return Response
	 */
	public function expiredAutoViews()
	{
		if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }
		
		$search  = Input::has('s') ? self::getSearchParam(Input::get('s')) : 'id IS NOT NULL';

        $service = Services::where('name', '=', 'Auto Views')
        					->where('is_subscription', '=', '1')->first();
		
        $statusku = Status::all();
		
        $status_r = array();
        foreach ($statusku as $s) {
            $status_r[$s->id] = $s->name;
        }

        return View::make('views.subscription.list')
					->with('service', $service)
					->with('orders', $this->getData($service->id, $search, false))
            		->with('status_arr', $status_r)
            		->with('active_count', $this->getCountData($service->id))
            		->with('expired_count', $this->getCountData($service->id, false));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /subscription
	 *
	 * @return Response
	 */
	public function store()
	{
		if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        try {
			
            $user 			= Sentry::getUser();
			$service 		= Services::find(Input::get('services'));
			$red_service	= str_replace(" ", "_", strtolower($service->name));
			
            $validator = Validator::make(
     			Input::all(), array(
              		'services' 		=> 'required|integer|min:1',
              		'plan'			=> 'required|integer|min:1',
              		'daysPrice'		=> 'required|integer|min:1',
                  	'username'     	=> 'required',
              	)
        	);

            if ($validator->fails()) {
                DB::rollback();
                Session::flash('error', 'All input form must be required!!!');

                return Redirect::to($red_service)->withErrors($validator)->withInput();
            }

            DB::beginTransaction();

            $data 		= array();
            $data 		= Input::all();
            $service_id = $data['services'];
			$plan		= ServicePlans::find($data['plan']);
			$username	= OrderController::getInstaUsername($data['username']);

            $price 			= 0;
			$newDate		= '';
			$serviceCost 	= 0;
			if($data['daysPrice'] == 1){
				$price 		= $plan->price_7_days;
				$serviceCost= $plan->cost_7_days;
				$newDate 	= strtotime('+7 days', time());
			} elseif($data['daysPrice'] == 2){
				$price 		= $plan->price_30_days;
				$serviceCost= $plan->cost_30_days;
				$newDate 	= strtotime('+30 days', time());
			} elseif($data['daysPrice'] == 3){
				$price 		= $plan->price_60_days;
				$serviceCost= $plan->cost_60_days;
				$newDate 	= strtotime('+60 days', time());
			} elseif($data['daysPrice'] == 4){
				$price 		= $plan->price_90_days;
				$serviceCost= $plan->cost_90_days;
				$newDate 	= strtotime('+90 days', time());
			}
			
            $rate = ServicePlanRates::where('user_id', '=', $user->id)->where('plan_id', '=', $plan->id)->first();
            if (count($rate) > 0) {
            	if($data['daysPrice'] == 1){
            		if($rate->rate_7_days > 0)
						$price = $rate->rate_7_days;
				} elseif($data['daysPrice'] == 2){
					if($rate->rate_30_days > 0)
						$price = $rate->rate_30_days;
				} elseif($data['daysPrice'] == 3){
					if($rate->rate_60_days > 0)
						$price = $rate->rate_60_days;
				} elseif($data['daysPrice'] == 4){
					if($rate->rate_90_days > 0)
						$price = $rate->rate_90_days;
				}
            }

            $user_data = User::find($user->id);
            if ($user_data->balance < $price OR is_null($user_data->balance)) {
                DB::rollback();
                Session::flash('error', 'Sorry, Your balance is not enough!!!');

                return Redirect::to($red_service)->withErrors($validator)->withInput();
            }

            if (!OrderController::isInactiveOrder($service_id, $username)) {
            	DB::rollback();
              	$result['message'] = ': Sorry, the order with same service and instagram username ' . $username . ' already exist and still active, Please try again later or contact our support';
             	Session::flash('error', $result['message']);

             	return Redirect::to($red_service)->withErrors($result['message'])->withInput();
            }

            $url = Config::get('app.ipanel_add');

            $curlData = '';
			$curlData['Auto Likes'] = array(
                'name'    => $username,
                'contact' => '#smmproviders_#'.$user->id,
                'date'    => $newDate,
                'limit'   => $plan->count,
                'private_key' => Config::get('app.private_key')
            );
			
            $curlData['Auto Views'] = array(
                'name'    => $username,
                'contact' => '#smmproviders_#'.$user->id,
                'date'    => $newDate,
                'limit'   => $plan->count,
                'private_key' => Config::get('app.private_key')
            );

            $user_data->balance = $user_data->balance - $price;
            $user_data->spent 	= $user_data->spent + $price;

            $user_data->save();

            $order 					= new Order();
            $order->service_type_id = $service_id;
            $order->service_plan_id = $plan->id;
            $order->seller_id 		= $user->id;
            $order->link 			= $username;
            $order->charge 			= $price;
            $order->quantity 		= $plan->count;
            $order->phone_number 	= '#smmproviders_#'.$user->id;
            $order->expired_date 	= gmdate("Y-m-d H:i:s", $newDate);
			$order->is_subscription	= 1;
            $order->service_cost 	= $serviceCost;
            
            if ($order->save() && $user_data->save()) {
				
				$curl = new \Reseller\Api\Curl();

	            $http_get = '';
	            $http_get = $curl->simple_post($url[$service->name], $curlData[$service->name], array('SSL_VERIFYPEER' => 0, 'SSL_VERIFYHOST' => 0, 'TIMEOUT' => 60));
	            if (strlen(trim($http_get)) == 0) {
	                DB::rollback();
	                $result['message'] = trans('orders.noresultapi');
	                Session::flash('error', $result['message']);
					Log::error($result['message']);
	
	                return Redirect::to($red_service)->withErrors($result['message'])->withInput();
	            }
	
	            $result_api = json_decode($http_get);
	
	            if (!isset($result_api) OR $result_api->result == 'error') {
	                DB::rollback();
	                $result['message'] = ((isset($result_api->msg) &&  $result_api->msg) ? $result_api->msg : trans('orders.noresultapi'));
	                Session::flash('error', $result['message']);
					Log::error($result['message']);
	
	                return Redirect::to($red_service)->withErrors($result['message'])->withInput();
	            }
	
	            $result_id = isset($result_api) ? $result_api->id : 0;
	
	            if (!$result_id) {
	                DB::rollback();
	                $result['message'] = trans('orders.noresultapi');
	                Session::flash('error', $result['message']);
					Log::error($result['message']);
	
	                return Redirect::to($red_service)->withErrors($result['message'])->withInput();
	            }
				
	            $valid_api_id = is_array($result_id) ? $result_id : array($result_id);
				
				$order->api_id = serialize($valid_api_id);
				
				$order->save();

                DB::commit();

                Session::flash('success', 'Success!');

                return Redirect::to($red_service);

            }

        } catch (Exception $e) {
        	Log::error($e);
            DB::rollback();

            Session::flash('error','('. $e->getLine() .') '. $e->getMessage());

            return Redirect::to($red_service)->withErrors($e->getMessage())->withInput();
        }
	}

	/**
	 * Display the specified resource.
	 * GET /subscription/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /subscription/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		
		if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }
		
		$order 			= Order::find($id);
		$red_service	= str_replace(" ", "_", strtolower($order->service->name)).'/active';
		
		try {
				
			if (Sentry::getUser()->id != $order->seller->id) throw new Exception(trans('users.noaccess'));
			
			return View::make('views.subscription.edit')->with('order', $order)
														->with('price', $this->getPlanPrices($order->plan->id));
			
		} catch (Exception $e) {
			Log::error($e);
			Session::flash('error','('. $e->getLine() .') '. $e->getMessage());
            return Redirect::to($red_service)->withErrors($e->getMessage())->withInput();
        }
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /subscription/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }
		
		$order 			= Order::find($id);
		$red_service	= str_replace(" ", "_", strtolower($order->service->name)).'/active';
		
		try {
				
			if (Sentry::getUser()->id != $order->seller->id) throw new Exception(trans('users.noaccess'));
			
			$validator = Validator::make(
     			Input::all(), array(
              		'daysPrice'		=> 'required|integer|min:1',
              	)
        	);
			
            $qty = $order->quantity;

            if ($validator->fails()) {
                DB::rollback();
                Session::flash('error', 'All input form must be required!!!');

                return Redirect::to('subscription/'.$id.'/edit')->withErrors($validator)->withInput();
            }
			
			$data = array();
			$data = Input::all();
            $user = Sentry::getUser();
			
			$price 			= 0;
			$newDate		= '';
			$serviceCost	= 0;
			if($data['daysPrice'] == 1){
				$price 		= $order->plan->price_7_days;
				$serviceCost= $order->plan->cost_7_days;
				$newDate 	= strtotime('+7 days', strtotime($order->expired_date));
			} elseif($data['daysPrice'] == 2){
				$price 		= $order->plan->price_30_days;
				$serviceCost= $order->plan->cost_30_days;
				$newDate 	= strtotime('+30 days', strtotime($order->expired_date));
			} elseif($data['daysPrice'] == 3){
				$price 		= $order->plan->price_60_days;
				$serviceCost= $order->plan->cost_60_days;
				$newDate 	= strtotime('+60 days', strtotime($order->expired_date));
			} elseif($data['daysPrice'] == 4){
				$price 		= $order->plan->price_90_days;
				$serviceCost= $order->plan->cost_90_days;
				$newDate 	= strtotime('+90 days', strtotime($order->expired_date));
			}
			
            $rate = ServicePlanRates::where('user_id', '=', $user->id)->where('plan_id', '=', $order->plan->id)->first();
            if (count($rate) > 0) {
            	if($data['daysPrice'] == 1){
            		if($rate->rate_7_days > 0)
						$price = $rate->rate_7_days;
				} elseif($data['daysPrice'] == 2){
					if($rate->rate_30_days > 0)
						$price = $rate->rate_30_days;
				} elseif($data['daysPrice'] == 3){
					if($rate->rate_60_days > 0)
						$price = $rate->rate_60_days;
				} elseif($data['daysPrice'] == 4){
					if($rate->rate_90_days > 0)
						$price = $rate->rate_90_days;
				}
            }

            $user_data = User::find($user->id);
            if ($user_data->balance < $price OR is_null($user_data->balance)) {
                DB::rollback();
                Session::flash('error', 'Sorry, Your balance is not enough!!!');

                return Redirect::to('subscription/'.$id.'/edit')->withErrors($validator)->withInput();
            }

            if (OrderController::isInactiveOrder($order->service->id, $order->link)) {
            	DB::rollback();
              	$result['message'] = ': Sorry, the order already expired, Please try again later or contact our support';
             	Session::flash('error', $result['message']);

             	return Redirect::to('subscription/'.$id.'/edit')->withErrors($result['message'])->withInput();
            }

            $url = Config::get('app.ipanel_update');

            $curlData = '';
			$curlData['Auto Likes'] = array(
                'name'    => $order->link,
                'contact' => $order->phone_number,
                'date'    => $newDate,
                'limit'   => $qty,
                'private_key' => Config::get('app.private_key')
            );
			
            $curlData['Auto Views'] = array(
                'name'    => $order->link,
                'contact' => $order->phone_number,
                'date'    => $newDate,
                'limit'   => $qty,
                'private_key' => Config::get('app.private_key')
            );
			
			// Update user data
            $user_data->balance 	= $user_data->balance - $price;
            $user_data->spent 		= $user_data->spent + $price;
			
			// Update order data
			$order->charge			+= $price;
			$order->service_cost	+= $serviceCost;
			$order->expired_date 	= gmdate("Y-m-d H:i:s", $newDate);

            if ($order->save() && $user_data->save()) {
				
				$curl = new \Reseller\Api\Curl();

	            $http_get = '';
	            $http_get = $curl->simple_post($url[$order->service->name], $curlData[$order->service->name], array('SSL_VERIFYPEER' => 0, 'SSL_VERIFYHOST' => 0, 'TIMEOUT' => 60));
	            if (strlen(trim($http_get)) == 0) {
	                DB::rollback();
	                $result['message'] = trans('orders.noresultapi');
	                Session::flash('error', $result['message']);
					Log::error($result['message']);
	
	                return Redirect::to($red_service)->withErrors($result['message'])->withInput();
	            }
	
	            $result_api = json_decode($http_get);
	
	            if (!isset($result_api) OR $result_api->result == 'error') {
	                DB::rollback();
	                $result['message'] = ((isset($result_api->msg) &&  $result_api->msg) ? $result_api->msg : trans('orders.noresultapi'));
	                Session::flash('error', $result['message']);
					Log::error($result['message']);
	
	                return Redirect::to($red_service)->withErrors($result['message'])->withInput();
	            }
	
	            $result_id = isset($result_api) ? $result_api->id : 0;
	
	            if (!$result_id) {
	                DB::rollback();
	                $result['message'] = trans('orders.noresultapi');
	                Session::flash('error', $result['message']);
					Log::error($result['message']);
	
	                return Redirect::to($red_service)->withErrors($result['message'])->withInput();
	            }
				
	            $valid_api_id = is_array($result_id) ? $result_id : array($result_id);
				
				$order->api_id = serialize($valid_api_id);
				
				$order->save();

                DB::commit();

                Session::flash('success', 'Success!');

                return Redirect::to($red_service);

            }
			
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();
			Session::flash('error','('. $e->getLine() .') '. $e->getMessage());
            return Redirect::to($red_service)->withErrors($e->getMessage())->withInput();
        }
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /subscription/update_setting
	 *
	 * @return Response
	 */
	public function updateSetting()
	{
		if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }
                
  		if (!Sentry::getUser() -> hasAccess('admin')) {
        	return Redirect::to('/') -> withErrors(trans('users.noaccess'));
      	}
			
		$service 		= Services::find(Input::get('service_id'));
		$red_service	= str_replace(" ", "_", strtolower($service->name));

        try {
        	
			DB::beginTransaction();
			
            $validator = Validator::make(
     			Input::all(), array(
              		'service_id'	=> 'required|integer|min:1',
              		'max_amount'	=> 'required|integer',
              		'price_7_days'	=> 'required',
              		'price_30_days'	=> 'required',
              		'price_60_days'	=> 'required',
              		'price_90_days'	=> 'required',
              		'cost_7_days'	=> 'required',
              		'cost_30_days'	=> 'required',
              		'cost_60_days'	=> 'required',
              		'cost_90_days'	=> 'required',
              	)
        	);
			
			if ($validator->fails()) {
                DB::rollback();
                Session::flash('error', 'All input form must be required!!!');

                return Redirect::to($red_service)->withErrors($validator)->withInput();
            }
			
			$service->max_amount	= Input::get('max_amount');
			$service->price_7_days	= Input::get('price_7_days');
			$service->price_30_days	= Input::get('price_30_days');
			$service->price_60_days	= Input::get('price_60_days');
			$service->price_90_days	= Input::get('price_90_days');
			$service->cost_7_days	= Input::get('cost_7_days');
			$service->cost_30_days	= Input::get('cost_30_days');
			$service->cost_60_days	= Input::get('cost_60_days');
			$service->cost_90_days	= Input::get('cost_90_days');
			$service->save();
			
			DB::commit();
			Session::flash('success', 'Service updated succesfully!');

            return Redirect::to($red_service);

        } catch (Exception $e) {
        	Log::error($e);
            DB::rollback();

            Session::flash('error','('. $e->getLine() .') '. $e->getMessage());

            return Redirect::to($red_service)->withErrors($e->getMessage())->withInput();
        }
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /subscription/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if (!Sentry::check()) {
			return Redirect::to('/') -> withErrors(trans('users.ploginf'));
		}

		if (!Sentry::getUser() -> hasAccess('admin')) {
			return Redirect::to('/') -> withErrors(trans('users.ploginf'));
		}

		$data = ServicePlans::find($id);

		if ($data -> delete()) {
			Session::flash('success', trans('services.Plan') . ' has deleted succesfully!');
			$red_service	= str_replace(" ", "_", strtolower($data->service->name));
			return Redirect::to($red_service);
		} else {
			Session::flash('error', 'Something wrong, please try again!');
			$red_service	= str_replace(" ", "_", strtolower($data->service->name));
			return Redirect::to($red_service);
		}
	}
	
	/**
	 * Delete selected data from database
	 *
	 * */
	public function deleteList() {

		if (!Sentry::check()) {
			return Redirect::to('/') -> withErrors(trans('users.ploginf'));
		}

		if (!Sentry::getUser() -> hasAccess('admin')) {
			return Redirect::to('/') -> withErrors(trans('users.ploginf'));
		}

		$submit 	= Input::get('submit');
		$item 		= Input::get('item');
		
		$service	= Services::find(Input::get('service_id'));
		$red_service	= str_replace(" ", "_", strtolower($service->name));

		if ($submit && is_array($item)) {

			foreach ($item as $val) {
				$data = ServicePlans::find($val);
				if($data){
					$data -> delete();
				}
			}

			Session::flash('success', ': ' . trans('services.Plan') . ' has deleted succesfully!');
			return Redirect::to($red_service);

		} else {
			Session::flash('error', ': ' . trans('services.Plan') . ' not selected!');
			return Redirect::to($red_service);
		}

	}

	/**
	 * Change status services
	 *
	 * */
	public function changeStatus($id) {

		$plan = ServicePlans::find($id);

		if ($plan -> status == '1') {
			$plan -> status = 0;
		} else {
			$plan -> status = 1;
		}
		
		$plan->save();
		
		$red_service	= str_replace(" ", "_", strtolower($plan->service->name));
		return Redirect::to($red_service);

	}
	
	protected function getData($id, $search, $active = true)
	{
		
		$now = Date::now();
		
		if (!$this->check()) {
            return array();
        }
		
		$op = '<';
		if($active) $op = '>';
		
		
		$user = Sentry::getUser();
        $orders = null;
        if ($user->hasAccess('admin')) {
            $orders = Order::where('service_type_id', '=', $id)->whereNotNull('is_subscription')->where('expired_date', $op, $now)
            				->where(function ($query) use ($search) {
		                        $query->whereRaw($search);
		                    })->orderBy('created_at', 'desc')->paginate($this->pagging);
        } else {
            $orders = Order::where('service_type_id', '=', $id)->where('seller_id', '=', $user->id)->whereNotNull('is_subscription')
            				->where('expired_date', $op, $now)->where(function ($query) use ($search) {
		                        $query->whereRaw($search);
		                    })->orderBy('created_at', 'desc')->paginate($this->pagging);
        }

		return $orders;
		
	}

	protected function getCountData($id, $active = true)
	{
		
		$now 	= Date::now();
        $count 	= 0;
		
		$op = '<';
		if($active) $op = '>';
		
		$user 	= Sentry::getUser();
        if ($user->hasAccess('admin')) {
            $count = Order::where('service_type_id', '=', $id)->whereNotNull('is_subscription')
            				->where('expired_date', $op, $now)->orderBy('created_at', 'desc')->count();
        } else {
            $count = Order::where('service_type_id', '=', $id)->where('seller_id', '=', $user->id)->whereNotNull('is_subscription')
            				->where('expired_date', $op, $now)->orderBy('created_at', 'desc')->count();
        }

		return $count;
		
	}
	
	protected function getPrices($id)
	{
		
		$result		= '';
		$value		= '';
		$service	= Services::find($id);
		$rates		= Rate::where('user_id', '=', Sentry::getUser()->id)->where('service_id', '=', $service->id)->first();
		
		if($service->price_7_days > 0 || $service->price_30_days > 0 || $service->price_60_days > 0 || $service->price_90_days > 0){
			$result .= '<select id="daysPriceSelect" class="form-control" name="daysPrice">';
			
			if ($service->price_30_days > 0) {
            	$price_30_days	= !is_null($rates) && $rates->price_30_days > 0 ? $rates->price_30_days : $service->price_30_days;
               	$result			.= '<option value="2">'.trans('orders.30Days').' ( '.$price_30_days.' )</option>';
				$value			.= Form::hidden('vDaysPrice2', $price_30_days);
          	}
			
			if ($service->price_60_days > 0) {
            	$price_60_days = !is_null($rates) && $rates->price_60_days > 0 ? $rates->price_60_days : $service->price_60_days;
               	$result        .= '<option value="3">'.trans('orders.60Days').' ( '.$price_60_days.' )</option>';
				$value			.= Form::hidden('vDaysPrice3', $price_60_days);
          	}
			
			if ($service->price_90_days > 0) {
            	$price_90_days = !is_null($rates) && $rates->price_90_days > 0 ? $rates->price_90_days : $service->price_90_days;
               	$result        .= '<option value="4">'.trans('orders.90Days').' ( '.$price_90_days.' )</option>';
				$value			.= Form::hidden('vDaysPrice4', $price_90_days);
          	}
			
			if ($service->price_7_days > 0) {
            	$price_7_days = !is_null($rates) && $rates->price_7_days > 0 ? $rates->price_7_days : $service->price_7_days;
               	$result        .= '<option value="1">'.trans('orders.7Days').' ( '.$price_7_days.' )</option>';
				$value			.= Form::hidden('vDaysPrice1', $price_7_days);
          	}
			
			$result .= '</select>';
		}

		$result .= $value;
		
		return $result;
		
	}

	public function getPlanPrices($id = null)
	{
		
		$planId 	= is_null($id) ? Input::get('id') : $id;
		
		$result		= '';
		$value		= '';
		$plan		= ServicePlans::find($planId);
		$rates		= ServicePlanRates::where('user_id', '=', Sentry::getUser()->id)->where('plan_id', '=', $plan->id)->first();
		
		if($plan->price_7_days > 0 || $plan->price_30_days > 0 || $plan->price_60_days > 0 || $plan->price_90_days > 0){
			$result .= '<select class="form-control" name="daysPrice">';
			
			if ($plan->price_30_days > 0) {
            	$price_30_days	= !is_null($rates) && $rates->rate_30_days > 0 ? $rates->rate_30_days : $plan->price_30_days;
               	$result			.= '<option value="2">'.trans('orders.30Days').' ( '.$price_30_days.' )</option>';
				$value			.= Form::hidden('arrDaysPrice[2]', trans('orders.30Days').' ( '.$price_30_days.' )');
          	}
			
			if ($plan->price_60_days > 0) {
            	$price_60_days 	= !is_null($rates) && $rates->rate_60_days > 0 ? $rates->rate_60_days : $plan->price_60_days;
               	$result        	.= '<option value="3">'.trans('orders.60Days').' ( '.$price_60_days.' )</option>';
				$value			.= Form::hidden('arrDaysPrice[3]', trans('orders.60Days').' ( '.$price_60_days.' )');
          	}
			
			if ($plan->price_90_days > 0) {
            	$price_90_days 	= !is_null($rates) && $rates->rate_90_days > 0 ? $rates->rate_90_days : $plan->price_90_days;
               	$result        	.= '<option value="4">'.trans('orders.90Days').' ( '.$price_90_days.' )</option>';
				$value			.= Form::hidden('arrDaysPrice[4]', trans('orders.90Days').' ( '.$price_90_days.' )');
          	}
			
			if ($plan->price_7_days > 0) {
            	$price_7_days 	= !is_null($rates) && $rates->rate_7_days > 0 ? $rates->rate_7_days : $plan->price_7_days;
               	$result        	.= '<option value="1">'.trans('orders.7Days').' ( '.$price_7_days.' )</option>';
				$value			.= Form::hidden('arrDaysPrice[1]', trans('orders.7Days').' ( '.$price_7_days.' )');
          	}
			
			$result .= '</select>';
			
		}
		
		return $result.$value;
		
	}
	
	public static function getSearchParam($str)
    {

        $all    = explode(" ", $str);
        $result = '';

        for ($i = 0; $i < count($all); $i++) {

            $result .= "id LIKE '%".$all[$i]."%' OR link LIKE '%".$all[$i]."%' OR phone_number LIKE '%".$all[$i]."%' ";
            if ($i + 1 < count($all)) {
                $result .= "OR ";
            }

        }

        return $result;

    }

}
