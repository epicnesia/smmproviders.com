<?php

class ServicesController extends \BaseController {

	public function __construct() {
		$this -> beforeFilter('Sentry', array('except' => 'login'));
	}

	/**
	 * Display a listing of the resource.
	 * GET /services
	 *
	 * @return Response
	 */
	public function index() {

		if (!Sentry::check()) {
			return Redirect::to('/') -> withErrors(trans('users.ploginf'));
		}

		if (!Sentry::getUser() -> hasAccess('admin')) {
			return Redirect::to('/') -> withErrors(trans('users.ploginf'));
		}

		$services = Services::whereNull('is_subscription') -> get();

		return View::make('views.admin.services.services') -> with('services', $services);
	}
	
	public function serviceList() {
			
		if (!Sentry::check()) {
			return Redirect::to('/') -> withErrors(trans('users.ploginf'));
		}
		
		$user = Sentry::getUser();

		$services 		= Services::whereNull('is_subscription')->get();
		$autoLikes 		= Services::whereNotNull('is_subscription')->where('name', '=', 'Auto Likes')->first();
		$autoLikesPrice	= array();
		foreach ($autoLikes->plans as $autoLikesPlan) {
			
			$rate = ServicePlanRates::where('user_id','=', $user->id)->where('plan_id','=',$autoLikesPlan->id)->first();
			
			$autoLikesPrice[$autoLikesPlan->id]['price_7_days']  = $rate && $rate->rate_7_days > 0 ? $rate->rate_7_days : $autoLikesPlan->price_7_days;
			$autoLikesPrice[$autoLikesPlan->id]['price_30_days'] = $rate && $rate->rate_30_days > 0 ? $rate->rate_30_days : $autoLikesPlan->price_30_days;
			$autoLikesPrice[$autoLikesPlan->id]['price_60_days'] = $rate && $rate->rate_60_days > 0 ? $rate->rate_60_days : $autoLikesPlan->price_60_days;
			$autoLikesPrice[$autoLikesPlan->id]['price_90_days'] = $rate && $rate->rate_90_days > 0 ? $rate->rate_90_days : $autoLikesPlan->price_90_days;
			
		}
		
		$autoViews 		= Services::whereNotNull('is_subscription')->where('name', '=', 'Auto Views')->first();
		$autoViewsPrice	= array();
		foreach ($autoViews->plans as $autoViewsPlan) {
			
			$rate = ServicePlanRates::where('user_id','=', $user->id)->where('plan_id','=',$autoViewsPlan->id)->first();
			
			$autoViewsPrice[$autoViewsPlan->id]['price_7_days']  = $rate && $rate->rate_7_days > 0 ? $rate->rate_7_days : $autoViewsPlan->price_7_days;
			$autoViewsPrice[$autoViewsPlan->id]['price_30_days'] = $rate && $rate->rate_30_days > 0 ? $rate->rate_30_days : $autoViewsPlan->price_30_days;
			$autoViewsPrice[$autoViewsPlan->id]['price_60_days'] = $rate && $rate->rate_60_days > 0 ? $rate->rate_60_days : $autoViewsPlan->price_60_days;
			$autoViewsPrice[$autoViewsPlan->id]['price_90_days'] = $rate && $rate->rate_90_days > 0 ? $rate->rate_90_days : $autoViewsPlan->price_90_days;
			
		}

		return View::make('views.user.services_list')-> with('services', $services)
													 -> with('autoLikes', $autoLikes->plans)
													 -> with('autoLikesPrice', $autoLikesPrice)
													 -> with('autoViews', $autoViews->plans)
													 -> with('autoViewsPrice', $autoViewsPrice);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /services/create
	 *
	 * @return Response
	 */
	public function create() {
		if (!Sentry::check()) {
			return Redirect::to('/') -> withErrors(trans('users.ploginf'));
		}

		if (!Sentry::getUser() -> hasAccess('admin')) {
			return Redirect::to('/') -> withErrors(trans('users.ploginf'));
		}

		return View::make('views.admin.services.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /services
	 *
	 * @return Response
	 */
	public function store() {
		
		if (!Sentry::check()) {
			return Redirect::to('/') -> withErrors(trans('users.ploginf'));
		}
	
		if (!Sentry::getUser() -> hasAccess('admin')) {
			return Redirect::to('/') -> withErrors(trans('users.ploginf'));
		}
		
		try {
	
			$validator = Validator::make(Input::all(), array(
												's_name' => 'required', 
												's_price' => 'required', 
												's_max_amount' => 'required', 
												's_name' => 'required'
											  )
										);
	
			if ($validator -> fails()) {
				Session::flash('error', 'All input required');
				return Redirect::to('admin/services/create') -> withErrors($validator) -> withInput();
			} else {
				
				DB::beginTransaction();
				
				Services::create(array('name' => Input::get('s_name'),
									   'price' => Input::get('s_price'),
									   'max_amount' => Input::get('s_max_amount'),
									   'service_cost' => Input::get('s_cost'),
									   )
								);
				
				DB::commit();
				
				Session::flash('success', Input::get('s_name') . ' has created succesfully!');
				return Redirect::to('admin/services');
			}
			
		} catch (Exception $e) {
            DB::rollback();

            Session::flash('error','('. $e->getLine() .') '. $e->getMessage());

            return Redirect::to($red_service)->withErrors($e->getMessage())->withInput();
        }
	}

	/**
	 * Display the specified resource.
	 * GET /services/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /services/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		if (!Sentry::check()) {
			return Redirect::to('/') -> withErrors(trans('users.ploginf'));
		}

		if (!Sentry::getUser() -> hasAccess('admin')) {
			return Redirect::to('/') -> withErrors(trans('users.ploginf'));
		}

		return View::make('views.admin.services.edit') -> with('service', Services::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /services/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {
		if (!Sentry::check()) {
			return Redirect::to('/') -> withErrors(trans('users.ploginf'));
		}

		if (!Sentry::getUser() -> hasAccess('admin')) {
			return Redirect::to('/') -> withErrors(trans('users.ploginf'));
		}
		
		try {

			$data = Services::find($id);
	
			$validator = Validator::make(Input::all(), 
									array('s_name' => 'required', 
										  's_price' => 'required', 
										  's_max_amount' => 'required|numeric', 
										  's_cost' => 'required'
										 ));
	
			if ($validator -> fails()) {
				Session::flash('error', 'All input required');
				return Redirect::to('admin/services/' . $id . '/edit') -> withErrors($validator) -> withInput();
			} else {
				
				DB::beginTransaction();
				
				$data -> name 			= Input::get('s_name');
				$data -> price 			= Input::get('s_price');
				$data -> max_amount 	= Input::get('s_max_amount');
				$data -> service_cost	= Input::get('s_cost');
				if ($data -> save()) {
					DB::commit();
					Session::flash('success', Input::get('s_name') . ' has updated succesfully!');
					return Redirect::action('ServicesController@index');
				} else {
					DB::rollback();
					Session::flash('error', 'Something wrong, please try again!');
					return Redirect::action('ServicesController@index');
				}
			}

		} catch (Exception $e) {
            DB::rollback();

            Session::flash('error','('. $e->getLine() .') '. $e->getMessage());

            return Redirect::to($red_service)->withErrors($e->getMessage())->withInput();
        }
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /services/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		if (!Sentry::check()) {
			return Redirect::to('/') -> withErrors(trans('users.ploginf'));
		}

		if (!Sentry::getUser() -> hasAccess('admin')) {
			return Redirect::to('/') -> withErrors(trans('users.ploginf'));
		}

		$data = Services::find($id);

		if ($data -> delete()) {
			Session::flash('success', trans('services.services') . ' has deleted succesfully!');
			return Redirect::action('ServicesController@index');
		} else {
			Session::flash('error', 'Something wrong, please try again!');
			return Redirect::action('ServicesController@index');
		}

	}

	/**
	 * Delete selected data from database
	 *
	 * */
	public function deleteList() {

		if (!Sentry::check()) {
			return Redirect::to('/') -> withErrors(trans('users.ploginf'));
		}

		if (!Sentry::getUser() -> hasAccess('admin')) {
			return Redirect::to('/') -> withErrors(trans('users.ploginf'));
		}

		$submit 	= Input::get('submit');
		$item 		= Input::get('item');

		if ($submit && is_array($item)) {

			foreach ($item as $val) {
				$data = Services::find($val);
				$data -> delete();
			}

			Session::flash('success', ': ' . trans('services.services') . ' has deleted succesfully!');
			return Redirect::action('ServicesController@index');

		} else {
			Session::flash('error', ': ' . trans('services.services') . ' not selected!');
			return Redirect::action('ServicesController@index');
		}

	}

	/**
	 * Change status services
	 *
	 * */
	public function changeStatus($id) {

		$service = Services::find($id);

		if ($service -> status == '1') {
			$service -> status = 0;
			$service -> save();
		} else {
			$service -> status = 1;
			$service -> save();
		}
		
		if($service->is_subscription){
			$red_service	= str_replace(" ", "_", strtolower($service->name));
			return Redirect::to($red_service);
		}else{
			return Redirect::to('admin/services');
		}

	}

}
