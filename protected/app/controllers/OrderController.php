<?php
use \Reseller\Api;

class OrderController extends \BaseController
{

    protected $lbl_status;
    protected $pagging;

    public function __construct()
    {
        $this->beforeFilter('Sentry', array('except' => 'login'));
        $this->lbl_status = array();
        $this->lbl_status[0] = 'label-default';
        $this->lbl_status[3] = 'label-success';
        $this->pagging = 10;
    }

    public function check()
    {
        $user = Sentry::getUser();
        $orders = null;
        if ($user->hasAccess('admin')) {
            $orders = Order::first();
        } else {
            $orders = Order::where('seller_id', '=', $user->id)->first();
        }
        if (is_null($orders)) {
            return false;
        }
        if (count($orders) == 0) {
            return false;
        }

        return true;
    }

    protected function getAllData()
    {
        if (!$this->check()) {
            return array();
        }

        $user = Sentry::getUser();
        $orders = null;
        if ($user->hasAccess('admin')) {
            $orders = Order::whereNull('is_subscription')->get();
        } else {
            $orders = Order::where('seller_id', '=', $user->id)->whereNull('is_subscription')->get();
        }

        if (is_null($orders)) {
            return array();
        }
        $orders_1 = array();
        $status_id = 0;
        foreach ($orders as $order) {
            $serviceku = Services::where('id', '=', $order->service_type_id)->first();
            if (is_null($order->status)) {
                $status_id = 0;
            } else {
                $status_id = $order->status;
            }
            $status_name = 'Pending';
            $statusku = Status::where('id', '=', $status_id)->first();
            if (!is_null($statusku)) {
                $status_name = $statusku->name;
            }

            $seller = User::find($order->seller_id);

            $orders_1[$order->id] = array(
                'id'                => $order->id,
                're'                => $order->re,
                'username'          => $seller ? $seller->username : '',
                'link'              => $order->link,
                'charge'            => round($order->charge, 2),
                'start_count'       => $order->start_count,
                'quantity'          => $order->quantity,
                'service_type_name' => $serviceku->name,
                'status'            => $status_name,
                'remains'           => $order->remains,
                'date'              => $order->created_at,
                'desc'              => $order->desc,
            );
        }

        return $orders_1;
    }

    protected function getAllDataPaging($status_id = -1)
    {
        if (!$this->check()) {
            return array();
        }

        $user = User::find(Sentry::getUser()->id);

        $Orders_1 = null;
        if ($status_id == -1) {
            if (Sentry::getUser()->hasAccess('admin')) {
                if (Input::has('q')) {
                	if (Input::has('o')) {	
	                    switch (Input::get('o')) {
							default:	
							case '1':
								$Orders_1 = Order::with('one_service', 'one_user')->whereHas(
			                        'one_user', function ($q) {
			                            $q->where('username', 'like', '%' . Input::get('q') . '%');
			                        }
			                    )->whereNull('is_subscription')->orderBy('id', 'desc');
								break;
							case '2':
								$Orders_1 = Order::with('one_service', 'one_user')
								->where('id', '=', Input::get('q') )
								->whereNull('is_subscription')
								->orderBy('id', 'desc');
								break;
							case '3':
								$Orders_1 = Order::with('one_service', 'one_user')
								->where('link', 'like', '%' . Input::get('q') . '%')
								->whereNull('is_subscription')
								->orderBy('id', 'desc');
								break;	
						}
					}else{
						$Orders_1 = Order::with('one_service', 'one_user')->whereHas(
	                        'one_user', function ($q) {
	                            $q->where('username', 'like', '%' . Input::get('q') . '%');
	                        }
	                    )->whereNull('is_subscription')->orderBy('id', 'desc');
					}
                } else {
                    $Orders_1 = Order::with('one_service', 'one_user')->whereNull('is_subscription')->orderBy('id', 'desc');
                }

            } else {
                $Orders_1 = $user->orders()->with('one_service', 'one_user')->whereNull('is_subscription')->orderBy('id', 'desc');

            }
        } else {
            if (Sentry::getUser()->hasAccess('admin')) {
                if (Input::has('q')) {
                    $Orders_1 = Order::with('one_service', 'one_user')->whereHas(
                        'one_user', function ($q) {
                            $q->where('username', 'like', '%' . Input::get('q') . '%');
                        }
                    )->where('status', '=', $status_id)->whereNull('is_subscription')->orderBy('id', 'desc');
                } else {
                    $Orders_1 = Order::with('one_service', 'one_user')->where('status', '=', $status_id)
                        ->whereNull('is_subscription')->orderBy('id', 'desc');
                }

            } else {

                $Orders_1 = $user->orders()
                    ->where('status', '=', $status_id)->with('one_service', 'one_user')
					->whereNull('is_subscription')
                    ->orderBy('id', 'desc');

            }
        }
		
//		$Orders_1 = $Orders_1->select(array('orders.id','orders.link','orders.charge','orders.start_count',
//		'orders.quantity','orders.remains','orders.created_at','orders.status','orders.desc'));
        $return = $Orders_1->paginate($this->pagging);

//        if (Sentry::getUser()->hasAccess('admin')) {
////            dd(DB::getQueryLog());
//        }

        return $return;
    }

    protected function getStatusBtn()
    {

        $result = array();

        if (!$this->check()) {
            return array();
        }

        $user = Sentry::getUser();
        $orders = null;
        $count = 0;
        $result[-1]['id'] = -1;
        $result[-1]['name'] = 'All';

        if ($user->hasAccess('admin')) {
//            $orders = Order::all();
            $result[-1]['count'] = Order::whereNull('is_subscription')->count();
        } else {
//            $orders = Order::where('seller_id', '=', $user->id)->get();
            $result[-1]['count'] = Order::where('seller_id', '=', $user->id)->whereNull('is_subscription')->count();
        }

        if (!$result[-1]['count']) {
            return array();
        }


        $status = Status::all();
        foreach ($status as $s) {
            $result[$s->id]['id'] = $s->id;
            $result[$s->id]['name'] = $s->name;
            if ($user->hasAccess('admin')) {
                $result[$s->id]['count'] = Order::where('status', '=', $s->id)->whereNull('is_subscription')->count();
            } else {
                $result[$s->id]['count'] = Order::where('seller_id', '=', $user->id)->where('status', '=', $s->id)->whereNull('is_subscription')->count();
            }
        }

        return $result;
    }

    protected function getData($id)
    {
        if (!$this->check()) {
            return array();
        }

        return Order::find($id);
    }

    /**
     * Display a listing of the resource.
     * GET /oder
     *
     * @return Response
     */
    public function index()
    {
        $orders = $this->getAllDataPaging();
        $status_btn = $this->getStatusBtn();
        $statusku = Status::all();
		
		$option_search = array('1'=>'User','2'=>'ID','3'=>'Link');
		
        $status_r = array();
        foreach ($statusku as $s) {
            $status_r[$s->id] = $s->name;
        }

        return View::make('views.order.main')
            ->with('orders', $orders)
			->with('option_search', $option_search)
            ->with('statuses', $statusku)
            ->with('status_btn', $status_btn)
            ->with('status_arr', $status_r)
            ->with('status_on', '-1');
    }

    /**
     * Display a listing of the resource.
     * GET /oder
     *
     * @return Response
     */
    public function history()
    {
        $orders = $this->getAllDataPaging();
        $statusku = Status::all();
        $status_r = array();
        foreach ($statusku as $s) {
            $status_r[$s->id] = $s->name;
        }
        $status_btn = $this->getStatusBtn();

        return View::make('views.order.history')
            ->with('orders', $orders)
            ->with('status_arr', $status_r)
            ->with('status_btn', $status_btn)
            ->with('status_on', -1);
    }

    /**
     * Display a listing of the resource.
     * GET /oder
     *
     * @return Response
     */
    public function historyShow($id)
    {
        $orders = $this->getAllDataPaging($id);
        $statusku = Status::all();
        $status_r = array();
        foreach ($statusku as $s) {
            $status_r[$s->id] = $s->name;
        }
        $status_btn = $this->getStatusBtn();

        return View::make('views.order.history')
            ->with('orders', $orders)
            ->with('status_arr', $status_r)
            ->with('status_btn', $status_btn)
            ->with('status_on', $id);
    }


    /**
     * Show the form for creating a new resource.
     * GET /oder/create
     *
     * @return Response
     */
    public function create()
    {
        if (!Sentry::check()) {
            return Redirect::to('/')
                ->withErrors(trans('users.ploginf'));
        }

        $o_services = Services::where('status','=','1')->whereNull('is_subscription')->get();
        $services[0] = '-- Please choose option --';
        foreach ($o_services as $t) {
            $services[$t->id] = $t->name;
        }

        return View::make('views.order.create')
            ->with('services', $services);
    }

    public static function isInactiveOrder($service_id, $link)
    {
        if (!isset($service_id) && !isset($link)) {
            return false;
        }

        $order = Order::where('service_type_id', '=', $service_id)
            ->where('link', '=', $link)
            ->orderBy('id', 'desc')//Order yang terakhir yang di cek, karena kemungkinan order sebelumnya sudah selesai
            ->first();

        if (is_null($order)) {
            return true;
        }

        if (time() > strtotime($order->expired_date)) {
            return true;
        }

        return false;
    }


    /**
     * Store a newly created resource in storage.
     * POST /oder
     *
     * @return Response
     */
    public function store()
    {
        if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        $oldService = 0;
        try {

            DB::beginTransaction();

            $service 	= Services::find(Input::get('services'));
            $pos 		= strpos($service->name, 'Comments');
            $pos1 		= strpos($service->name, 'Mentions');

            if ($pos == true) {
                $validator = Validator::make(
                    Input::all(), array(
                        'services' => 'required|integer|min:1',
                        'link'     => 'required|min:4',
                        'comments' => 'required|min:1'
                    )
                );
                $except = Input::except('quantity', 'phone');
                $oldService = 1;
                $qty = count(explode("\n", trim(Input::get('comments'))));
                if (!$qty) {
                    DB::rollback();
                    Session::flash('error', 'Please Insert Comments');
                    Session::flash('service', $oldService);

                    return Redirect::to('/order/create')->withInput($except);
                }


            } elseif ($service->name == 'Instagram Auto Likes') {
                $validator = Validator::make(
                    Input::all(), array(
                        'services' => 'required|integer|min:1',
                        'link'     => 'required|min:4',
                        'quantity' => 'required|numeric|min:1',
                        'phone'    => 'required|min:6'
                    )
                );
                $except = Input::except('comments');
                $oldService = 2;
                $qty = Input::get('quantity');

            } elseif ($service->name == 'Instagram Mentions Custom List') {
                $validator = Validator::make(
                    Input::all(), array(
                        'services' => 'required|integer|min:1',
                        'link'     => 'required|min:4',
                        'custom_list'     => 'required_without:custom_listFile|min:1',
                        'custom_listFile' => 'required_without:custom_list'
                    )
                );
				
                $except = Input::except('comments', 'phone', 'quantity');
                $oldService = 4;
				$qty = 0;
				$c_list = '';
				
				$file = Input::file('custom_listFile');
				
				if(Input::hasFile('custom_listFile')){
					
					if(Input::get('custom_list')) throw new Exception("You don't need to upload a file if custom list form already filled");
					
					if($file->isValid()){
						
							$ext = $file->getClientOriginalExtension();
							$size = $file->getSize();
							
							if($ext != 'txt' && $size <= 0) throw new Exception("Not allowed file!");
							
							$lists = explode("\n", file_get_contents($file->getRealPath()));
							$uLists= array_unique($lists);
							$validList = array();
							
							foreach ($uLists as $value) {
								if($value && !strpos($value, " ")){
									$validList[$qty] = $value;
									$qty++;
								}
							}
							
							$c_list = trim(implode(PHP_EOL, $validList));
							
						} else {
							DB::rollback();
							
	                    	Session::flash('error', 'Invalid file!');
	                    	return Redirect::to('/order/create')->withInput($except);
						}
					
				}else{
					$lists = explode("\n", Input::get('custom_list'));
					$uLists= array_unique($lists);
					$validList = array();
					foreach ($uLists as $value) {
						if($value && !strpos($value, " ")){
							$validList[$qty] = $value;
							$qty++;
						}
					}
					
					$c_list = trim(implode(PHP_EOL, $validList));
					
	                if ($qty == 0) {
	                    DB::rollback();
	                    Session::flash('error', 'Please Insert Custom List');
	                    Session::flash('service', $oldService);
	
	                    return Redirect::to('/order/create')->withInput($except);
	                }
				}

            } else {
                $validator = Validator::make(
                    Input::all(), array(
                        'services' => 'required|integer|min:1',
                        'link'     => 'required|min:4',
                        'quantity' => 'required|numeric|min:1'
                    )
                );
                $except = Input::except('comments', 'phone');
				if($service->name == 'Instagram Mentions'){
					$oldService = 3;
				} elseif ($service->name == 'Instagram User Mentions') {
					$oldService = 5;
				} else {
					$oldService = 0;
				}
                $qty = Input::get('quantity');
            }

            if ($validator->fails()) {
                DB::rollback();
                Session::flash('error', 'All input form must be required!!!');
                Session::flash('service', $oldService);

                return Redirect::to('/order/create')->withErrors($validator)->withInput($except);
            }

            $data = array();
            $data = Input::all();
            $service_id = $data['services'];
            $user = Sentry::getUser();
			
			$max_amount = $service->max_amount;
			if($qty > $max_amount){
				DB::rollback();
                Session::flash('error', 'Sorry, Your quantity exceeds the maximum amount of orders!!!');
                Session::flash('service', $oldService);

                return Redirect::to('/order/create')->withErrors($validator)->withInput($except);
			}

            $price = $service->price;
            $rate = Rate::where('user_id', '=', $user->id)->where('service_id', '=', $service_id)->first();
            if (count($rate) > 0) {
                $price = $rate->rates;
            }

            $order_prs 	= 1000 / $qty;
            $charge 	= $price / $order_prs;
			$cost		= $service->service_cost > 0 ? $service->service_cost / $order_prs : 0;

            $user_data = User::find($user->id);
            if ($user_data->balance < $charge OR is_null($user_data->balance)) {
                DB::rollback();
                Session::flash('error', 'Sorry, Your balance is not enough!!!');
                Session::flash('service', $oldService);

                return Redirect::to('/order/create')->withErrors($validator)->withInput($except);
            }

            $typesArray1 = array('Instagram Auto Likes');
            if (in_array($service->name, $typesArray1)) {

                if (!self::isInactiveOrder($service->id, $data['link'])) {
                    DB::rollback();
                    $result['message'] = ': Sorry, the order with same service and instagram username ' . self::getInstaUsername($data['link']) . ' already exist and still active, Please try again later or contact our support';
                    Session::flash('error', $result['message']);
                    Session::flash('service', $oldService);

                    return Redirect::to('/order/create')->withErrors($result['message'])->withInput();
                }

            }

            $typesArray2 = array('Instagram Mentions', 'Instagram User Mentions');
            if (in_array($service->name, $typesArray2)) {

                $hashtags_links = count(explode("\n", trim($data['mHashtags'])));
                if($hashtags_links < 2){
                    DB::rollback();
                    $result['message'] = ': Sorry, hashtags must be 2 or more!';
                    Session::flash('error', $result['message']);
                    Session::flash('service', $oldService);

                    return Redirect::to('/order/create')->withErrors($result['message'])->withInput();
                }

            }

            $url = Config::get('app.ipanel_add');

            $curlData = array();
            $curlData['Middle East Instagram Likes'] = array(
                'instagram_link' => trim($data['link']),
                'likes_to_get'   => trim($qty),
                'private_key' => Config::get('app.private_key')
            );

            $curlData['Middle East Instagram Comments'] = array(
                'instagram_link' => trim($data['link']),
                'comments'       => trim($data['comments']),
                'private_key' => Config::get('app.private_key')
            );

            $curlData['Middle East Instagram Followers'] = array(
                'name'  => trim(self::getInstaUsername($data['link'])),
                'limit' => trim($qty),
                'private_key' => Config::get('app.private_key')
            );

            $curlData['Instagram High Quality Likes'] = array(
                'instagram_link' => trim($data['link']),
                'likes_to_get'   => trim($qty),
                'private_key' => Config::get('app.private_key')
            );

            $curlData['Instagram High Quality Comments'] = array(
                'instagram_link' => trim($data['link']),
                'comments'       => trim($data['comments']),
                'private_key' => Config::get('app.private_key')
            );

            $curlData['Instagram High Quality Followers'] = array(
                'name'  => trim(self::getInstaUsername($data['link'])),
                'limit' => trim($qty),
                'private_key' => Config::get('app.private_key')
            );

            $newDate = strtotime('+30 days', time());
            $curlData['Instagram Auto Likes'] = array(
                'name'    => trim(self::getInstaUsername($data['link'])),
                'contact' => trim($data['phone']),
                'date'    => $newDate,
                'limit'   => trim($qty),
                'private_key' => Config::get('app.private_key')
            );

            $curlData['Instagram Mentions'] = array(
                'instagram_link' => trim($data['link']),
                'usernames'      => (isset($data['mUsernames']) && $data['mUsernames']) ? trim($data['mUsernames']) : '',
                'hashtags'       => (isset($data['mHashtags']) && $data['mHashtags']) ? trim($data['mHashtags']) : '',
                'limit'          => trim($qty),
                'comments'		 => (isset($data['mComments']) && $data['mComments']) ? trim($data['mComments']) : '',
                'private_key' => Config::get('app.private_key')
            );

            $curlData['Instagram Mentions Custom List'] = array(
                'instagram_link' => trim($data['link']),
                'custom_list'    => (isset($c_list) && $c_list) ? trim($c_list) : '',
                'limit'          => trim($qty),
                'comments'		 => (isset($data['mComments']) && $data['mComments']) ? trim($data['mComments']) : '',
                'private_key' 	 => Config::get('app.private_key')
            );
			
			$curlData['Instagram Views'] = array(
                'instagram_link' => trim($data['link']),
                'views_to_get'   => trim($qty),
                'private_key' => Config::get('app.private_key')
            );

            $curlData['Instagram User Mentions'] = array(
                'instagram_username' => trim($data['link']),
                'usernames'      => (isset($data['mUsernames']) && $data['mUsernames']) ? trim($data['mUsernames']) : '',
                'hashtags'       => (isset($data['mHashtags']) && $data['mHashtags']) ? trim($data['mHashtags']) : '',
                'limit'          => trim($qty),
                'private_key' => Config::get('app.private_key')
            );

            $user_data->balance = $user_data->balance - $charge;
            $user_data->spent = $user_data->spent + $charge;

            $user_data->save();

            $order = new Order();
            $order->service_type_id = $service_id;
            $order->seller_id = $user->id;
            $order->link = self::validatingLink($data['link']);
            $order->charge = $charge;
            $order->quantity = $qty;
            $order->phone_number = $data['phone'];
            $order->service_cost = $cost;
            if ($service->name == 'Instagram Auto Likes') {
                $order->expired_date = gmdate("Y-m-d H:i:s", $newDate);
            }
            
            if ($order->save() && $user_data->save()) {
            
                if($pos == true){
                            
                    $answer = new Answer();
                    $answer->order_id = $order->id;
                    $answer->question = "Comments";
                    $answer->answer = $data['comments'];
                    
                    if(!$answer->save()) throw new Exception("Error Processing Request, please try again!", 1);
                    
                } elseif ($pos1 == true && (isset($data['mComments']) && $data['mComments'])) {
                	
                    $answer = new Answer();
                    $answer->order_id = $order->id;
                    $answer->question = "Mention Additional Comments";
                    $answer->answer = $data['mComments'];
                    
                    if(!$answer->save()) throw new Exception("Error Processing Request, please try again!", 1);
                }
				
				$curl = new \Reseller\Api\Curl();

	            $http_get = '';
	            $http_get = $curl->simple_post($url[$service->name], $curlData[$service->name], array('SSL_VERIFYPEER' => 0, 'SSL_VERIFYHOST' => 0, 'TIMEOUT' => 60));
	            if (strlen(trim($http_get)) == 0) {
	                DB::rollback();
	                $result['message'] = trans('orders.noresultapi');
	                Session::flash('error', $result['message']);
	                Session::flash('service', $oldService);
	
	                return Redirect::to('/order/create' . Input::get('id'))->withErrors($result['message'])->withInput();
	            }
	
	            $result_api = json_decode($http_get);
	
	            if (!isset($result_api) OR $result_api->result == 'error') {
	                DB::rollback();
	                $result['message'] = ((isset($result_api->msg) &&  $result_api->msg) ? $result_api->msg : trans('orders.noresultapi'));
	                Session::flash('error', $result['message']);
	                Session::flash('service', $oldService);
	
	                return Redirect::to('/order/create' . Input::get('id'))->withErrors($result['message'])->withInput();
	            }
	
	            $result_id = isset($result_api) ? $result_api->id : 0;
	
	            if (!$result_id) {
	                DB::rollback();
	                $result['message'] = trans('orders.noresultapi');
	                Session::flash('error', $result['message']);
	                Session::flash('service', $oldService);
	
	                return Redirect::to('/order/create' . Input::get('id'))->withErrors($result['message'])->withInput();
	            }
				
	            $valid_api_id = is_array($result_id) ? $result_id : array($result_id);
				
				$order->api_id = serialize($valid_api_id);
				
				$order->save();

                DB::commit();

                Session::flash('success', 'Success!');

                return Redirect::to('/user/order/history');

            }

        } catch (Exception $e) {
            DB::rollback();

            Session::flash('error','('. $e->getLine() .') '. $e->getMessage());
            Session::flash('service', $oldService);

            return Redirect::to('/order/create')->withErrors($e->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     * GET /oder/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        if (!$this->check()) {
            return array();
        }
        /*
        $user = Sentry::getUser();
        $orders = null;
        if ($user -> hasAccess('admin')) {
            $orders = Order::where('status', '=', $id) -> get();
        } else {
            $orders = Order::where('seller_id', '=', $user -> id) -> where('status', '=', $id) -> get();
        }

        if (is_null($orders)) {
            return array();
        }
        $orders_1 = array();
        $status_id = 0;
        foreach ($orders as $order) {
            $serviceku = Services::where('id', '=', $order -> service_type_id) -> first();
            if (is_null($order -> status)) {
                $status_id = 0;
            } else {
                $status_id = $order -> status;
            }
            $statusku = Status::where('id', '=', $status_id) -> first();
            $seller = User::find($order -> seller_id);

                    $orders_1[$order->id] = array(
                        'id'                   => $order->id,
                        're'           => $order->re,
                        'username'          => $seller->username,
                        'link'      => $order->link,
                        'charge'   => round($order->charge,2),
                        'start_count'                => $order->start_count,
                        'quantity'          => $order->quantity,
                        'service_type_name'    => $serviceku->name,
                        'status'         => $statusku->name,
                        'remains' =>  $order->remains,
                        'date' =>  $order->created_at
                    );
                }
         */

        $orders_1 = $this->getAllDataPaging($id);
        $status = Status::all();
        $status_r = array();
        foreach ($status as $s) {
            $status_r[$s->id] = $s->name;
        }
        $status_btn = $this->getStatusBtn();
		$option_search = array('1'=>'User','2'=>'ID','3'=>'Link');
        return View::make('views.order.main')
            ->with('orders', $orders_1)
			->with('option_search', $option_search)
            ->with('status_btn', $status_btn)
            ->with('statuses', $status)
            ->with('status_arr', $status_r)
            ->with('status_on', $id);

    }

    /**
     * Show the form for editing the specified resource.
     * GET /oder/{id}/edit
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * PUT /oder/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /oder/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Change Order Status
     *
     */
    public function changeStatus($id, $status)
    {


        $order = Order::find($id);
        //refundud
        if ($status == 3) {
            $toRefund = $order->charge;

            $user = User::find($order->seller_id);
            if ($user) {
                $user->balance += $toRefund;
                $spent = $user->spent - $toRefund;
                if ($spent > 0)
                    $user->spent = $spent;
                $user->save();
            }
        }


        $order->status = $status;

        if ($order->save()) {
            return Redirect::to('order');
        } else {
            Session::flash('error', 'Error');

            return Redirect::to('order');
        }

    }

    /**
     * Cancel order
     *
     */
    public function cancelOrder($id)
    {

        if (!Sentry::getUser()->hasAccess('su_admin')) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        $order = Order::find($id);
        $user = User::find($order->seller_id);

        $user->balance = $user->balance + $order->charge;
        $user->spent = $user->spent - $order->charge;

        try {
            $user->save();
            $order->delete();

            return Redirect::to('order');
        } catch (Exception $e) {
            Session::flash('error', $e->messages());

            return Redirect::to('order');
        }

    }

    /**
     * Set Order Start Count
     *
     */
    public function setStartCount()
    {
        $submit = Input::get('submit');
        $id = Input::get('id');
        $startCount = Input::get('startCount');

        if ($submit && is_numeric($startCount)) {
            $order = Order::find($id);
            $order->start_count = $startCount;
            if ($order->save()) {
                Session::flash('success', 'Success');

                return Redirect::to('order');
            } else {
                Session::flash('error', 'Error');

                return Redirect::to('order');
            }
        } else {
            Session::flash('error', 'Error');

            return Redirect::to('order');
        }

    }

    /**
     * Check The Type of selected service
     *
     */
    public function checkServiceType()
    {
        $result = 0;
        $submit = Input::get('submit');
        $id = Input::get('id');

        if ($submit) {

            $service = Services::find($id);
            if (strpos($service->name, 'Comments')) {
                $result = 1;
            } elseif ($service->name == 'Instagram Auto Likes') {
                $result = 2;
            } elseif ($service->name == 'Instagram Mentions') {
                $result = 3;
            } elseif ($service->name == 'Instagram Mentions Custom List') {
                $result = 4;
            } elseif ($service->name == 'Instagram User Mentions') {
                $result = 5;
            }


        }

        echo $result;

    }

    public static function getInstaUsername($str)
    {

        if(strpos($str, "instagram.com")){
                 
                $url = substr($str, 4, 1) == "s" ? "https://www.instagram.com/" : "http://www.instagram.com/";
                         
                $name = trim(str_replace("/", "", (substr($str, strlen($url)))));
        }else{
                $name = $str;
        }

        return str_replace("@", "", $name);

    }
    
    public static function validatingLink($str)
    {
        if(strpos($str, "instagram.com")){
                $name = $str;
        }else{
                $name = "https://www.instagram.com/".$str;
        }
        return $name;
    }

    /**
     * Get total charge
     *
     */
    public function getCharge()
    {
        $result 	= 0;
        $submit 	= Input::get('submit');
        $id 		= Input::get('id');
        $type 		= Input::get('type');
        $qty 		= Input::get('qty');
        $service 	= Services::find($id);
        $user 		= Sentry::getUser();

        if ($submit) {

            $qty_order = $type == 'quantity' ? $qty : count(explode("\n", trim($qty)));
            $rate = Rate::where('user_id', '=', $user->id)->where('service_id', '=', $id)->first();
            $price = is_null($rate) ? $service->price : $rate->rates;
            $order = 1000 / $qty_order;
            $result = $price / $order;

        }

        echo round($result, 2);

    }
	
	/**
     * Get price Per 1000
     *
     */
    public function getPrice()
    {
        $result 	= 0;
        $submit 	= Input::get('submit');
        $id 		= Input::get('id');
        $service 	= Services::find($id);
        $user 		= Sentry::getUser();

        if ($submit) {

            $rate = Rate::where('user_id', '=', $user->id)->where('service_id', '=', $id)->first();
            $price = is_null($rate) ? $service->price : $rate->rates;
            $result = $price;

        }

        echo round($result, 2);

    }

}
