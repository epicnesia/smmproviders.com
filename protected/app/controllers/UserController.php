<?php

use Authority\Repo\User\UserInterface;
use Authority\Repo\Group\GroupInterface;
use Authority\Service\Form\Register\RegisterForm;
use Authority\Service\Form\User\UserForm;
use Authority\Service\Form\ResendActivation\ResendActivationForm;
use Authority\Service\Form\ForgotPassword\ForgotPasswordForm;
use Authority\Service\Form\ChangePassword\ChangePasswordForm;
use Authority\Service\Form\SuspendUser\SuspendUserForm;

class UserController extends BaseController
{

    protected $user;
    protected $group;
    protected $registerForm;
    protected $userForm;
    protected $resendActivationForm;
    protected $forgotPasswordForm;
    protected $changePasswordForm;
    protected $suspendUserForm;

    /**
     * Instantiate a new UserController
     */
    public function __construct(UserInterface $user, GroupInterface $group, RegisterForm $registerForm,
                                UserForm $userForm, ResendActivationForm $resendActivationForm,
                                ForgotPasswordForm $forgotPasswordForm, ChangePasswordForm $changePasswordForm,
                                SuspendUserForm $suspendUserForm)
    {
        $this->user                 = $user;
        $this->group                = $group;
        $this->registerForm         = $registerForm;
        $this->userForm             = $userForm;
        $this->resendActivationForm = $resendActivationForm;
        $this->forgotPasswordForm   = $forgotPasswordForm;
        $this->changePasswordForm   = $changePasswordForm;
        $this->suspendUserForm      = $suspendUserForm;

        $this->beforeFilter('Sentry', array(
            'only' => array(
                'edit',
                'update'
            )
        ));
        $this->beforeFilter('inGroup:Admins-Super Admins', array(
            'only' => array(
                'show',
                'index',
                'destroy',
                'suspend',
                'unsuspend',
                'ban',
                'unban'
            )
        ));
    }

    public function getApiKey($user_id)
    {

        $apis = array();
        $apis = Config::get('app.api_access');

        $user_key = '';
        foreach ($apis as $key => $value) {
            if ($user_id == $value) {
                $user_key = $key;
                break;
            }
        }

        return $user_key;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }
        if (!Sentry::getUser()->hasAccess('admin')) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        $users      = User::where('activated', '=', '1')->get();
        $status_btn = $this->getStatusBtn();

        foreach ($users as $key) {
            $api_key[$key->id]    = $this->getApiKey($key->id);
            $api_enable[$key->id] = 1;
            if ($api_key[$key->id] == '') {
                if ($api = ApiKeyGenerator::where('user_id', '=', $key->id)->first()) {
                    $api_key[$key->id]    = $api->api_key;
                    $api_enable[$key->id] = $api->api_enable;
                }
            }
        }


        return View::make('views.admin.users')->with('users', $users)->with('status_btn', $status_btn)
                   ->with('api_key', $api_key)->with('api_enable', $api_enable);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function balance()
    {
        if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }
        $user    = Sentry::getUser();
        $balance = 0;
        $userku  = User::find($user->id);
        if (!is_null($userku)) {
            $balance = $userku->balance;
        }

        return View::make('views.user.balance')->with('balance', $balance);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function setting()
    {
        if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        $user   = Sentry::getUser();
        $userku = User::find($user->id);
        if ($user == null || !is_numeric($user->id)) {
            return \App::abort(404);
        }

        return View::make('views.admin.setting')->with('user', $userku);;
    }


    /**
     * Show the form for creating a new user.
     *
     * @return Response
     */
    public function create()
    {
        if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }
        if (!Sentry::getUser()->hasAccess('admin')) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        $allGroups = $this->group->all();

        return View::make('admin_v.user_v.create')->with('allGroups', $allGroups);
    }

    /**
     * Store a newly created user.
     *
     * @return Response
     */
    public function store()
    {
        if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }
        if (!Sentry::getUser()->hasAccess('admin')) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        $validator = Validator::make(Input::all(), array('username' => 'required|unique:users|min:4'));
        if ($validator->fails()) {
            Session::flash('error', 'Username must be required, unique and min 4 character!');

            return Redirect::to('/admin/users')->withInput()->withErrors($validator->messages());
        }
        $data                          = Input::all();
        $data['email']                 = $data['username'] . '@' . $data['username'] . '.com';
        $data['password']              = '123456';
        $data['password_confirmation'] = '123456';
        // Form Processing
        $result = $this->registerForm->save($data);

        if ($result['success']) {

            $this->user->activate($result['mailData']['userId'], $result['mailData']['activationCode']);

            $userku = User::find($result['mailData']['userId']);

            $userku->username        = $data['username'];
            $userku->password_nohash = $data['password'];
			if(Input::has('skype_username'))
				$userku->skype_username = Input::get('skype_username');
            $userku->save();

            // Success!
            Session::flash('success', $result['success']);

            return Redirect::to('/admin/users');

        }
        else {
            Session::flash('error', $result['success']);

            return Redirect::to('/admin/users')->withInput()->withErrors($this->registerForm->errors());
        }
    }

    /**
     * Store a newly created user.
     *
     * @return Response
     */
    public function register()
    {

        $data                          = Input::all();
        $data['password_confirmation'] = $data['password'];
        // Form Processing
        $result = $this->registerForm->save($data);

        if ($result['success']) {
            $this->user->activate($result['mailData']['userId'], $result['mailData']['activationCode']);

            $userku = User::find($result['mailData']['userId']);

            $userku->username        = $data['username'];
            $userku->password_nohash = $data['password'];
            $userku->country         = $data['country'];
            $userku->save();

            // Success!
            Session::flash('success', $result['message']);

            return Redirect::to('/');

        }
        else {
            Session::flash('error', $result['message']);

            return Redirect::to('/')->withInput()->withErrors($this->registerForm->errors());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }
        if (!Sentry::getUser()->hasAccess('admin')) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        if ($id == '1') {
            $users = User::where('activated', '=', '1')->get();
        }
        else {
            $users = User::where('activated', '=', '0')->get();
        }


        $status_btn = $this->getStatusBtn();
		
		$api_key = [];
		$api_enable = [];
        foreach ($users as $key) {
            $api_key[$key->id]    = $this->getApiKey($key->id);
            $api_enable[$key->id] = 1;
            if ($api_key[$key->id] == '') {
                if ($api = ApiKeyGenerator::where('user_id', '=', $key->id)->first()) {
                    $api_key[$key->id]    = $api->api_key;
                    $api_enable[$key->id] = $api->api_enable;
                }
            }
        }

        return View::make('views.admin.users')->with('users', $users)->with('status_btn', $status_btn)
                   ->with('api_key', $api_key)->with('api_enable', $api_enable);


        //                return View::make('views.admin.users') -> with('users', $users)->with('status_btn', $status_btn);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        if (!Sentry::getUser()->hasAccess('admin')) {
            if (Sentry::getUser()->id != $id) {
                return Redirect::to('/')->withErrors(trans('users.ploginf'));
            }
        }

        $user = User::find($id);

        if ($user == null || !is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }


        return View::make('views.admin.user.edit')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }


        Session::flash('success', 'Success');

        return Redirect::to('/');

    }

    public function perbaharui($id)
    {
        if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        $validator = Validator::make(Input::all(), array(
            'newUsername' => 'required|min:4',
            'newPassword' => 'required|min:4',
        ));

        if ($validator->fails()) {
            Session::flash('error', 'Username and Password must be required!');

            return Redirect::to('/admin/users/' . $id . '/edit')->withErrors('validation')->withInput();
        }

        $data   = Input::all();
        $userku = Sentry::getUserProvider()->findById($id);

        $user_o = User::find($id);

        if (is_null($user_o)) {
            Session::flash('error', 'User not found!');

            return Redirect::to('/admin/users/' . $id . '/edit')->withErrors('user gak ditemukan')->withInput();
        }

        $userku->password = e($data['newPassword']);
        $userku->username = e($data['newUsername']);
        $userku->skype_username = e($data['skype_username']);
        if ($userku->save()) {
            // User saved
            $result['success'] = true;
            $result['message'] = trans('users.passwordchg');
        }
        else {
            // User not saved
            $result['success'] = false;
            $result['message'] = trans('users.passwordprob');
        }

        $user_o->password_nohash = $data['newPassword'];

        if ($user_o->save()) {
            // User saved
            $result['success'] = true;
            $result['message'] = trans('users.passwordchg');
        }
        else {
            // User not saved
            $result['success'] = false;
            $result['message'] = trans('users.passwordprob');
        }


        if ($result['success']) {
            // Success!
            Session::flash('success', 'User saved successfully!');

            return Redirect::to('/admin/users');
        }
        else {
            Session::flash('error', 'Cannot save!');

            return Redirect::to('/admin/users/' . $id . '/edit')->withErrors('tidak dapat disimpan')->withInput();
        }


    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }
        if (is_numeric($id)) {
            $user    = User::find($id);
            $order_c = Order::where('seller_id', '=', $id)->count();
            $bal     = '0';
            if (trim($user->balance) != '') {
                $bal = trim($user->balance);
            }
            if ($bal != '0' OR $order_c != 0) {
                Session::flash('error', 'Unable to Delete User');

                return Redirect::to('/admin/users');
            }
            else {
                $rates = Rate::where('user_id', '=', $id)->get();
                foreach ($rates as $rate) {
                    $rate->delete();
                }
            }
        }


        if ($this->user->destroy($id)) {
            Session::flash('success', 'User Deleted');

            return Redirect::to('/admin/users');
        }
        else {
            Session::flash('error', 'Unable to Delete User');

            return Redirect::to('/admin/users');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param
     * @return Response
     */
    public function deleteChecked()
    {
        if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }
        if (!Sentry::getUser()->hasAccess('admin')) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        $data = Input::all();
        // Update group memberships
        $allUsers = User::all();

        $is_success = false;
        foreach ($allUsers as $userku) {
            if (isset($data['users'][$userku->id])) {
                //The user should be added to this group
                if ($this->user->destroy($userku->id)) {
                    $is_success = true;
                }
                else {
                    $is_success = false;
                }

            }
        }

        if ($is_success) {
            // Success!
            $result['message'] = 'User Checked Deleted';
            Session::flash('success', $result['message']);

            return Redirect::to('/users');

        }
        else {
            $result['message'] = 'User Checked UnDeleted';
            Session::flash('error', $result['message']);

            return Redirect::to('/users');
        }

    }

    /**
     * Activate a new user
     * @param  int $id
     * @param  string $code
     * @return Response
     */
    public function activate($id, $code)
    {
        if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        if (!is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

        $result = $this->user->activate($id, $code);

        if ($result['success']) {
            // Success!
            Session::flash('success', $result['message']);

            return Redirect::route('/main');

        }
        else {
            Session::flash('error', $result['message']);

            return Redirect::route('/main');
        }
    }

    /**
     * Process resend activation request
     * @return Response
     */
    public function resend()
    {
        // Form Processing
        $result = $this->resendActivationForm->resend(Input::all());

        if ($result['success']) {
            Event::fire('user.resend', array(
                    'email'          => $result['mailData']['email'],
                    'userId'         => $result['mailData']['userId'],
                    'activationCode' => $result['mailData']['activationCode']
                ));

            // Success!
            Session::flash('success', $result['message']);

            return Redirect::route('/main');
        }
        else {
            Session::flash('error', $result['message']);

            return Redirect::route('profile')->withInput()->withErrors($this->resendActivationForm->errors());
        }
    }

    /**
     * Process Forgot Password request
     * @return Response
     */
    public function forgot()
    {
        // Form Processing
        $result = $this->forgotPasswordForm->forgot(Input::all());

        if ($result['success']) {
            Event::fire('user.forgot', array(
                    'email'     => $result['mailData']['email'],
                    'userId'    => $result['mailData']['userId'],
                    'resetCode' => $result['mailData']['resetCode']
                ));

            // Success!
            Session::flash('success', $result['message']);

            return Redirect::route('/main');
        }
        else {
            Session::flash('error', $result['message']);

            return Redirect::route('forgotPasswordForm')->withInput()->withErrors($this->forgotPasswordForm->errors());
        }
    }

    /**
     * Process a password reset request link
     * @param  [type] $id   [description]
     * @param  [type] $code [description]
     * @return [type]       [description]
     */
    public function reset($id, $code)
    {
        if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        if (!is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

        $result = $this->user->resetPassword($id, $code);

        if ($result['success']) {
            Event::fire('user.newpassword', array(
                    'email'       => $result['mailData']['email'],
                    'newPassword' => $result['mailData']['newPassword']
                ));

            // Success!
            Session::flash('success', $result['message']);

            return Redirect::route('/main');

        }
        else {
            Session::flash('error', $result['message']);

            return Redirect::route('/main');
        }

    }

    public function ubahPaypalDetail($id)
    {
        $data = Input::all();

        $validator = Validator::make(Input::all(), array(
            'paypall_email' => 'required|email|min:5',
        ));

        if ($validator->fails()) {
            Session::flash('error', 'Paypal email must be required!');

            return Redirect::to('/admin/setting')->withErrors($validator->messages())->withInput();
        }

        $user   = Sentry::getUser();
        $user_o = User::find($user->id);

        if (is_null($user_o)) {
            Session::flash('error', 'User not found!');

            return Redirect::to('/admin/setting');
        }

        $user_o->paypall_email = $data['paypall_email'];

        if ($user_o->save()) {
            // User saved
            $result['success'] = true;
            $result['message'] = trans('users.paypalchg');
        }
        else {
            // User not saved
            $result['success'] = false;
            $result['message'] = trans('users.paypalnochg');
        }

        if ($result['success']) {
            // Success!
            Session::flash('success', $result['message']);

            return Redirect::to('/');
        }
        else {
            Session::flash('error', $result['message']);

            return Redirect::to('/admin/setting')->withErrors($result['message'])->withInput();
        }

    }

    public function ubahApi($id)
    {
        $data = Input::all();

        $validator = Validator::make(Input::all(), array(
            'social_api'  => 'required|min:4',
            'youtube_api' => 'required|min:4',
        ));

        if ($validator->fails()) {
            Session::flash('error', 'Social and Youtube API must be required!');

            return Redirect::to('/admin/setting')->withErrors($validator->messages())->withInput();
        }

        $user   = Sentry::getUser();
        $user_o = User::find($user->id);

        if (is_null($user_o)) {
            Session::flash('error', 'User not found!');

            return Redirect::to('/admin/setting');
        }

        $user_o->social_bulk_api_key    = $data['social_api'];
        $user_o->youTube_sevice_api_key = $data['youtube_api'];

        if ($user_o->save()) {
            // User saved
            $result['success'] = true;
            $result['message'] = trans('users.apichg');
        }
        else {
            // User not saved
            $result['success'] = false;
            $result['message'] = trans('users.apinochg');
        }

        if ($result['success']) {
            // Success!
            Session::flash('success', $result['message']);

            return Redirect::to('/');
        }
        else {
            Session::flash('error', $result['message']);

            return Redirect::to('/admin/setting')->withErrors($result['message'])->withInput();
        }

    }

    /**
     * Change password form
     *
     */
    public function changePassword()
    {
        if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        $user = Sentry::getUser();

        return View::make('views.user.change_password')->with('user', $user);

    }

    /**
     * Process a password change request
     * @param  int $id
     * @return redirect
     */
    public function ubah($id)
    {

        $data = Input::all();

        $validator = Validator::make(Input::all(), array(
            'oldPassword'              => 'required|min:4',
            'newPassword'              => 'required|min:4|confirmed',
            'newPassword_confirmation' => 'required|min:4',
        ));

        if ($validator->fails()) {
            Session::flash('error', 'All input must be required & New Password must be confirmed!');

            return Redirect::to('/user/changePassword')->withErrors($validator->messages())->withInput();
        }

        $user   = Sentry::getUser();
        $userku = Sentry::getUserProvider()->findById($user->id);

        $user_o = User::where('password_nohash', '=', $data['oldPassword'])->where('id', '=', $user->id)->first();

        if (is_null($user_o)) {
            Session::flash('error', 'Your Old password wrong!' . $user->id . ';' . $data['oldPassword']);

            return Redirect::to('/user/changePassword');
        }

        $userku->password = e($data['newPassword']);

        if ($userku->save()) {
            // User saved
            $result['success'] = true;
            $result['message'] = trans('users.passwordchg');
        }
        else {
            // User not saved
            $result['success'] = false;
            $result['message'] = trans('users.passwordprob');
        }

        $user_o->password_nohash = $data['newPassword'];

        if ($user_o->save()) {
            // User saved
            $result['success'] = true;
            $result['message'] = trans('users.passwordchg');
        }
        else {
            // User not saved
            $result['success'] = false;
            $result['message'] = trans('users.passwordprob');
        }


        if ($result['success']) {
            // Success!
            Session::flash('success', 'Password changed successfully!');

            return Redirect::to('/logout');
        }
        else {
            Session::flash('error', 'Password cannot be saved!');

            return Redirect::to('/user/changePassword');
        }

    }

    /**
     * Process a suspend user request
     * @param  int $id
     * @return Redirect
     */
    public function suspend($id)
    {
        if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        if (!is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

        // Form Processing
        $result = $this->suspendUserForm->suspend(Input::all());

        if ($result['success']) {
            // Success!
            Session::flash('success', $result['message']);

            return Redirect::to('/users');

        }
        else {
            Session::flash('error', $result['message']);

            return Redirect::action('UserController@suspend', array($id))->withInput()
                           ->withErrors($this->suspendUserForm->errors());
        }
    }

    /**
     * Unsuspend user
     * @param  int $id
     * @return Redirect
     */
    public function unsuspend($id)
    {
        if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        if (!is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

        $result = $this->user->unSuspend($id);

        if ($result['success']) {
            // Success!
            Session::flash('success', $result['message']);

            return Redirect::to('/users');

        }
        else {
            Session::flash('error', $result['message']);

            return Redirect::to('/users');
        }
    }

    /**
     * Ban a user
     * @param  int $id
     * @return Redirect
     */
    public function ban($id)
    {
        if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        if (!is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

        $result = $this->user->ban($id);

        if ($result['success']) {
            // Success!
            Session::flash('success', $result['message']);

            return Redirect::to('/users');

        }
        else {
            Session::flash('error', $result['message']);

            return Redirect::to('/users');
        }
    }

    public function unban($id)
    {
        if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        if (!is_numeric($id)) {
            // @codeCoverageIgnoreStart
            return \App::abort(404);
            // @codeCoverageIgnoreEnd
        }

        $result = $this->user->unBan($id);

        if ($result['success']) {
            // Success!
            Session::flash('success', $result['message']);

            return Redirect::to('/users');

        }
        else {
            Session::flash('error', $result['message']);

            return Redirect::to('/users');
        }
    }

    protected function getStatusBtn()
    {

        $result = array();

        if (!Sentry::check()) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        if (!Sentry::getUser()->hasAccess('admin')) {
            return Redirect::to('/')->withErrors(trans('users.ploginf'));
        }

        $status = array(
            '1' => 'Active',
            '2' => 'Banned'
        );
        foreach ($status as $key => $value) {
            $result[$key]['id']   = $key;
            $result[$key]['name'] = $value;
            if ($key == 1) {
                $result[$key]['count'] = User::where('activated', '=', '1')->count();
            }
            else {
                $result[$key]['count'] = User::where('activated', '=', '0')->count();
            }
        }

        return $result;
    }

    public function changeStatusUser($id, $status)
    {
        if ($status == '1') {
            $user            = User::find($id);
            $user->activated = 1;
            $user->save();
        }
        else {
            $user            = User::find($id);
            $user->activated = 0;
            $user->save();
        }

        return Redirect::to('admin/users');

    }

}
