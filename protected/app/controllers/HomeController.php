<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome() {
		if (!Sentry::check()) {
			Session::put('menus', null);		
			Session::put('menu_on', 'null');
			$email 		= Session::get('email');
			$password 	= Session::get('password');		
			$rememberMe = is_null(Session::get('rememberMe'))?false:true;
			return View::make('views/login')
						->with('email_old', $email)
						->with('password_old', $password)
						->with('rememberMe_old', $rememberMe);
		}
		
		$menus = array();
		$menus['users'] 			= array('name' => trans("pages.users") , 'route' => '/admin/users', 'icon' => 'fa-group', 'status' => 1);
		$menus['orders'] 			= array('name' => trans("pages.orders") , 'route' => '/order', 'icon' => 'fa-fax', 'status' => 1);
		$menus['services'] 			= array('name' => trans("pages.services"), 'route' => '/admin/services', 'icon' => 'fa-puzzle-piece', 'status' => 1);
		$menus['new_order'] 		= array('name' => trans("orders.neworder"), 'route' => '/order/create', 'icon' => 'fa-new', 'status' => 1);					
		$menus['orders_history'] 	= array('name' => trans("pages.orders") . ' ' .trans("pages.history") , 'route' => '/user/order/history', 'icon' => 'fa-fax', 'status' => 1);
		$menus['auto_likes'] 		= array('name' => trans("pages.Auto Likes") , 'route' => '/auto_likes', 'icon' => 'fa-fax', 'status' => 1);
		$menus['auto_views'] 		= array('name' => trans("pages.Auto Views") , 'route' => '/auto_views', 'icon' => 'fa-fax', 'status' => 1);
		$menus['service_list'] 		= array('name' => trans("pages.Service List") , 'route' => '/user/service_list', 'icon' => 'fa-fax', 'status' => 1);
		
		$menus['setting'] 			= array('name' => trans("pages.setting") , 'route' => '/admin/setting', 'icon' => 'fa-wrench', 'status' => 1);
        $menus['payments'] 			= array('name' => trans("pages.payments") , 'route' => '/admin/payments', 'icon' => 'fa-money', 'status' => 1);
        $menus['balanceAdder'] 		= array('name' => trans("pages.balanceAdder") , 'route' => '/admin/balanceAdder', 'icon' => 'fa-money', 'status' => 1);
		$menus['stat'] 				= array('name' => trans("pages.stat") , 'route' => '/admin/stat', 'icon' => 'fa-stat', 'status' => 1);
		
		$menus['balance'] 			= array('name' => trans("pages.balance") , 'route' => '/user/balance', 'icon' => 'fa-balance', 'status' => 1);
		$menus['faq'] 				= array('name' => trans("pages.faq") , 'route' => '/page/1', 'icon' => 'fa-faq', 'status' => 1);
				
		$menus['ticket'] 			= array('name' => trans("pages.ticket") , 'route' => '/ticket', 'icon' => 'fa-ticketing', 'status' => 1);			
		
		$menus['page'] 				= array('name' => trans("pages.page") , 'route' => '/page', 'icon' => 'fa-faq', 'status' => 1);
		$menus['apikey'] 			= array('name' => trans("pages.apikey") , 'route' => '/api/key', 'icon' => 'fa-faq', 'status' => 1);
		$menus['changePassword'] 	= array('name' => trans("pages.changePassword") , 'route' => '/user/changePassword', 'icon' => 'fa-faq', 'status' => 1);	
		
		if (Sentry::getUser() -> hasAccess('admin')) {
			$menus['new_order']['status'] = 0;
			$menus['orders_history']['status'] = 0;
			$menus['service_list']['status'] = 0;
			$menus['balance']['status'] = 0;
			$menus['faq']['status'] = 0;
			$menus['changePassword']['status'] = 0;            
			
			Session::put('menus', $menus);		
			Session::put('menu_on', 'null');
			
            return Redirect::to('/admin');
		}else{
			$menus['users']['status'] = 0;
			$menus['services']['status'] = 0;
			$menus['orders']['status'] = 0;
			
			$menus['setting']['status'] = 0;                     
            $menus['payments']['status'] = 0;                       
            $menus['balanceAdder']['status'] = 0;
			$menus['stat']['status'] = 0;                                
            
            $menus['page']['status'] = 0;
						
			Session::put('menus', $menus);		
			Session::put('menu_on', 'null');            
			
			return Redirect::to('/user');
		}
	}

}
